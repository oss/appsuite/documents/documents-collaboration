#!/bin/bash
#
#   @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
#   @license AGPL-3.0
#
#   This code is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
#
#   Any use of the work other than as authorized under this license or copyright law is prohibited.
#

# some defaults
DCS_PROPERTIES="/etc/documents-collaboration/dcs.properties"

DCSDB_SEARCHKEY_HOST=db.host
DCSDB_SEARCHKEY_PORT=db.port
DCSDB_SEARCHKEY_SCHEMA=db.schema
DCSDB_SEARCHKEY_USERNAME=db.username
DCSDB_SEARCHKEY_PASSWORD=db.password
DCSDB_SEARCHKEY_CONNECTION_URL=

# Options
DCS_PROPERTIES_LONG=properties
DCS_PROPERTIES_SHORT=p

# Actions
DCS_ACTION_LIST='false'

LONGOPTS='$DCS_PROPERTIES_LONG:'
NEEDED_EXECUTABLES="mysql sed"

die() {
    test -n "$1" && echo 1>&2 "$1" || echo 1>&2 "ERROR"
    exit 1
}

# help/usage text
usage() {
    local LOPTS=$(echo ${LONGOPTS} | sed -e 's/[:,]/ /g')

    echo
    echo "$0 currently knows the following parameters:"
    echo

    printf '%-30s | %s\n' "Parameter" "Default value"
    echo "------------------------------------------------------------"

    for CUROPT in $LOPTS; do
        local RVAR=${CUROPT%_LONG}
        local DEFAULT=$(eval echo ${RVAR})
        local SHORTOPT=$(eval echo "${RVAR}_SHORT")
        local LONGOPT=$(eval echo ${CUROPT})
        printf '%-30s | %s\n' "-${SHORTOPT} | --${LONGOPT}" ${DEFAULT}
    done
    echo
    echo
    echo 'NOTE: use "-l" to list all documents-collaboration servers, currently registered.'
    echo 'NOTE: use "-h" to show this help text.'
    echo
    echo
cat<<EOF
Example:

  $0 -l
EOF
    echo
    exit 0
}

# read and validate properties function
read_properties() {
    if [[ -n ${DCS_PROPERTIES} ]] && [[ -f ${DCS_PROPERTIES} ]]; then
        local REGEX_PREFFIX='^[[:space:]]*'
        local REGEX_POSTFIX='[[:space:]]*=[[:space:]]*(.+)[[:space:]]*$'

        while read CUR_LINE; do
            if [[ $CUR_LINE =~ ${REGEX_PREFFIX}${DCSDB_SEARCHKEY_HOST}${REGEX_POSTFIX} ]]; then
                DCSDB_HOST=${BASH_REMATCH[1]}
            elif [[ $CUR_LINE =~ ${REGEX_PREFFIX}${DCSDB_SEARCHKEY_PORT}${REGEX_POSTFIX} ]]; then
                DCSDB_PORT=${BASH_REMATCH[1]}
            elif [[ $CUR_LINE =~ ${REGEX_PREFFIX}${DCSDB_SEARCHKEY_SCHEMA}${REGEX_POSTFIX} ]]; then
                DCSDB_SCHEMA=${BASH_REMATCH[1]}
            elif [[ $CUR_LINE =~ ${REGEX_PREFFIX}${DCSDB_SEARCHKEY_USERNAME}${REGEX_POSTFIX} ]]; then
                DCSDB_USERNAME=${BASH_REMATCH[1]}
            elif [[ $CUR_LINE =~ ${REGEX_PREFFIX}${DCSDB_SEARCHKEY_PASSWORD}${REGEX_POSTFIX} ]]; then
                DCSDB_PASSWORD=${BASH_REMATCH[1]}
            elif [[ $CUR_LINE =~ ${REGEX_PREFFIX}${DCSDB_SEARCHKEY_CONNECTION_URL}${REGEX_POSTFIX} ]]; then
                DCSDB_URL=${BASH_REMATCH[1]}
               
                if [[ $DCSDB_URL =~ jdbc:mysql://([^:^/]+)[:]?([0-9]*)/(.*) ]]; then
                    DCSDB_HOST=${BASH_REMATCH[1]}
                    DCSDB_PORT=${BASH_REMATCH[2]}
                    DCSDB_SCHEMA=${BASH_REMATCH[3]}
                fi
            fi
        done < "${DCS_PROPERTIES}"
        
        if [[ -z $DCSDB_HOST ]]; then
            DCSDB_HOST=localhost
        fi

        if [[ -z $DCSDB_PORT ]]; then
            DCSDB_PORT=3306
        fi
        
        if [[ -z $DCSDB_SCHEMA ]]; then
            DCSDB_SCHEMA=dcsdb
        fi
    else
        echo "Invalid properties file given: ${DCS_PROPERTIES}"
        exit 3
    fi
}

# inappropriate parameters
if [ $# -le 0 ]; then
    usage
    exit 1
fi

# check prerequisites
for CUR_EXECUTABLE in ${NEEDED_EXECUTABLES}; do
    if ! hash ${CUR_EXECUTABLE} 2>/dev/null; then
        echo "Needed binary cannot be found: ${CUR_EXECUTABLE}. Aborting!"
        exit 2
    fi
done

# read and evaluate command line params
TEMP=$(POSIXLY_CORRECT=true getopt -o p:hl --long "$(eval echo ${LONGOPTS}),help" -- "$@")
eval set -- "$TEMP"

while true; do
    case "$1" in
    -p|--${DCS_PROPERTIES_LONG})
        DCS_PROPERTIES=${2}
        shift 2
        ;;
    -l)
        DCS_ACTION_LIST='true'
        shift
        ;;
    -h|--help)
        usage
        exit 1
        shift
        ;;
    --)
        shift
        break
        ;;
    *)
        die "Internal error!"
        exit 1
        ;;
    esac
done

# read properties
read_properties

# list action
if [ 'true' = ${DCS_ACTION_LIST} ]; then
    mysql -u ${DCSDB_USERNAME} -h ${DCSDB_HOST} -P ${DCSDB_PORT} -p${DCSDB_PASSWORD} -e "SELECT * FROM \`${DCSDB_SCHEMA}\`.DCS"

    LIST_ERROR=$?

    if [[ ${LIST_ERROR} -ne 0 ]]; then
        exit ${LIST_ERROR}
    fi
fi

# all good
exit 0
