#!/bin/bash
#
#   @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
#   @license AGPL-3.0
#
#   This code is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
#
#   Any use of the work other than as authorized under this license or copyright law is prohibited.
#

# some database defaults
DCSDB_USERNAME=openexchange
DCSDB_USERNAME_LONG=dcsdb-user
DCSDB_PASSWORD=
DCSDB_PASSWORD_LONG=dcsdb-pass
DCSDB_HOST=localhost
DCSDB_HOST_LONG=dcsdb-host
DCSDB_PORT=3306
DCSDB_PORT_LONG=dcsdb-port
DCSDB_SCHEMA=dcsdb
DCSDB_SCHEMA_LONG=dcsdb-dbname
DCSDB_DBCREATE_OPTIONS="DEFAULT CHARACTER SET utf8mb4 DEFAULT COLLATE utf8mb4_unicode_ci"

MYSQL_ROOT_USER=root
MYSQL_ROOT_USER_LONG=mysql-root-user
MYSQL_ROOT_PASSWD=
MYSQL_ROOT_PASSWD_LONG=mysql-root-passwd

# executables to check for existence
NEEDED_EXECUTABLES="mysql mysqladmin"

# reading DCS default properties
DCS_PROPERTIES_FILE="/etc/documents-collaboration/dcs.properties"

DCSDB_SEARCHKEY_HOST=db.host
DCSDB_SEARCHKEY_PORT=db.port
DCSDB_SEARCHKEY_SCHEMA=db.schema
DCSDB_SEARCHKEY_USERNAME=db.username

# default is NOT to create sql admin user
CREATEADMIN=
DELETETEDCSDB=

MUSTOPTS="DCSDB_PASSWORD"
LONGOPTS='$DCSDB_USERNAME_LONG:,$DCSDB_PASSWORD_LONG:,$DCSDB_HOST_LONG:,$DCSDB_PORT_LONG:,$DCSDB_SCHEMA_LONG:,$MYSQL_ROOT_USER_LONG:,$MYSQL_ROOT_PASSWD_LONG:'

die() {
    test -n "$1" && echo 1>&2 "$1" || echo 1>&2 "ERROR"
    exit 1
}

usage() {
    echo
    echo "$0 currently knows the following parameters:"
    echo
    local lopts=$(echo $LONGOPTS | sed -e 's/[:,]/ /g')
    printf '%-30s | %s\n' "Parameter" "Default value"
    echo "------------------------------------------------------------"
    for opt in $lopts; do
    local rvar=${opt%_LONG}
    local default=$(eval echo $rvar)
    local lopt=$(eval echo $opt)
    #echo $opt $rvar $default $lopt
    printf '%-30s | %s\n' "--$lopt" $default
    done
    echo
    echo
    echo 'NOTE: use "-a" to create SQL admin user using GRANT command'
    echo '      use "-i" to automatically delete the dcsdb if exists'
    echo
    echo
cat<<EOF
Example:

  $0 --dcsdb-pass=secret
EOF
    echo
    exit 0
}

readproperties() {
    if [[ -r "${1}" ]]; then
        local REGEX_PREFFIX='^[[:space:]]*'
        local REGEX_POSTFIX='[[:space:]]*=[[:space:]]*(.+)[[:space:]]*$'

        echo  "Reading default values from properties file: '${1}'"

        while read CUR_LINE; do
            local CUR_VALUE=

            if [[ $CUR_LINE =~ ${REGEX_PREFFIX}${DCSDB_SEARCHKEY_HOST}${REGEX_POSTFIX} ]]; then
                CUR_VALUE=${BASH_REMATCH[1]}

                if [[ -n ${CUR_VALUE} ]]; then
                    DCSDB_HOST=${CUR_VALUE}
                fi
            elif [[ $CUR_LINE =~ ${REGEX_PREFFIX}${DCSDB_SEARCHKEY_PORT}${REGEX_POSTFIX} ]]; then
                CUR_VALUE=${BASH_REMATCH[1]}

                if [[ -n ${CUR_VALUE} ]]; then
                    DCSDB_PORT=${CUR_VALUE}
                fi
            elif [[ $CUR_LINE =~ ${REGEX_PREFFIX}${DCSDB_SEARCHKEY_SCHEMA}${REGEX_POSTFIX} ]]; then
                CUR_VALUE=${BASH_REMATCH[1]}

                if [[ -n ${CUR_VALUE} ]]; then
                    DCSDB_SCHEMA=${CUR_VALUE}
                fi
            elif [[ $CUR_LINE =~ ${REGEX_PREFFIX}${DCSDB_SEARCHKEY_USERNAME}${REGEX_POSTFIX} ]]; then
                CUR_VALUE=${BASH_REMATCH[1]}

                if [[ -n ${CUR_VALUE} ]]; then
                    DCSDB_USERNAME=${CUR_VALUE}
                fi
            fi
        done < "${1}"
    else
        echo  "Cannot read default properties file, using internal defaults."
    fi
}

TEMP=$(POSIXLY_CORRECT=true getopt -o iap:h --long "$(eval echo $LONGOPTS),help" -- "$@")
eval set -- "$TEMP"

# checking prerequisites
for CUR_EXECUTABLE in ${NEEDED_EXECUTABLES}; do
    if ! hash ${CUR_EXECUTABLE} 2>/dev/null; then
        echo "Needed executable cannot be found: ${CUR_EXECUTABLE}. Please install before running this script. Aborting!"
        exit 2
    fi
done

# reading and overriding the locally set DCSDB properties
# with set entries from the $DCS_PROPERTIES_FILE
readproperties "${DCS_PROPERTIES_FILE}"

# reading cmd line properties
while true; do
    case "$1" in
    --$DCSDB_USERNAME_LONG)
        DCSDB_USERNAME=$2
        shift 2
        ;;
    --$DCSDB_PASSWORD_LONG)
        DCSDB_PASSWORD=$2
        shift 2
        ;;
    --$DCSDB_HOST_LONG)
        DCSDB_HOST=$2
        shift 2
        ;;
    -p|--$DCSDB_PORT_LONG)
        DCSDB_PORT=$2
        shift 2
        ;;
    --$DCSDB_SCHEMA_LONG)
        DCSDB_SCHEMA=$2
        shift 2
        ;;
    --$MYSQL_ROOT_USER_LONG)
        MYSQL_ROOT_USER=$2
        shift 2
        ;;
    --$MYSQL_ROOT_PASSWD_LONG)
        MYSQL_ROOT_PASSWD=$2
        shift 2
        ;;
    -a)
        CREATEADMIN=true
        shift
        ;;
    -i)
        DELETETEDCSDB=true
        shift
        ;;
    -h|--help)
        usage
        exit 1
        shift
        ;;
    --)
        shift
        break
        ;;
    *)
        die "Internal error!"
        exit 1
        ;;
    esac
done

# generic parameter checking
for opt in $MUSTOPTS; do
    opt_var=$(eval echo \$$opt)
    opt_var_long=$(eval echo \$${opt}_LONG)
    opt_var_values=$(eval echo \$${opt}_VALUES)

    if [ -z "$opt_var" ]; then
        usage
        die "missing required option --$opt_var_long"
    fi

    if [ -n "$opt_var_values" ]; then
        found=

        for val in $opt_var_values; do
            if [ "$val" == "$opt_var" ]; then
            found=$val
            fi
        done

        if [ -z "$found" ]; then
            die "\"$opt_var\" is not a valid option to --$opt_var_long"
        fi
    fi
done

if [ -n "$CREATEADMIN" ]; then
    if [ $(id -u) -ne 0 ]; then
       die "need to be root in order to setup the system"
    fi
fi

if [ "$(mysql -h $DCSDB_HOST -P $DCSDB_PORT -u $DCSDB_USERNAME -p${DCSDB_PASSWORD} -e "show databases;" -B 2>/dev/null | grep ${DCSDB_SCHEMA})" == ${DCSDB_SCHEMA} ] && [ "$DELETETEDCSDB" != "true" ]; then
    die "the database $DCSDB_SCHEMA exists, use the -i switch if you want to automatically delete it"
fi

echo "initializing dcsdb from scratch..."

if [ -n "$CREATEADMIN" ]; then
    ROOTAUTH=

    if [ -n "$MYSQL_ROOT_PASSWD" ]; then
        ROOTAUTH="-u $MYSQL_ROOT_USER -p${MYSQL_ROOT_PASSWD}"
    fi

    cat<<EOF | mysql -h $DCSDB_HOST -P $DCSDB_PORT $(eval echo $ROOTAUTH) || die "unable to drop database (wrong or missing password for user root?)"
drop database if exists \`${DCSDB_SCHEMA}\`;
EOF

    cat<<EOF | mysql -h $DCSDB_HOST -P $DCSDB_PORT $(eval echo $ROOTAUTH) || die "unable to GRANT privileges"
GRANT CREATE, LOCK TABLES, REFERENCES, INDEX, DROP, DELETE, ALTER, SELECT, UPDATE, INSERT, CREATE TEMPORARY TABLES, SHOW VIEW, SHOW DATABASES ON *.* TO '$DCSDB_USERNAME'@'%' IDENTIFIED BY '$DCSDB_PASSWORD' WITH GRANT OPTION;
GRANT CREATE, LOCK TABLES, REFERENCES, INDEX, DROP, DELETE, ALTER, SELECT, UPDATE, INSERT, CREATE TEMPORARY TABLES, SHOW VIEW, SHOW DATABASES ON *.* TO '$DCSDB_USERNAME'@'localhost' IDENTIFIED BY '$DCSDB_PASSWORD' WITH GRANT OPTION;
create database \`${DCSDB_SCHEMA}\` ${DCSDB_DBCREATE_OPTIONS};
EOF

    mysqladmin -h $DCSDB_HOST -P $DCSDB_PORT $(eval echo $ROOTAUTH) flush-privileges || die

else

    cat<<EOF | mysql -h $DCSDB_HOST -P $DCSDB_PORT -u $DCSDB_USERNAME -p${DCSDB_PASSWORD} || die
drop database if exists \`${DCSDB_SCHEMA}\`;
create database \`${DCSDB_SCHEMA}\` ${DCSDB_DBCREATE_OPTIONS};
EOF

fi
