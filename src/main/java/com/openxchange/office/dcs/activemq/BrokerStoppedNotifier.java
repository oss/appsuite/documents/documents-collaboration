/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.activemq;

import org.apache.activemq.command.ActiveMQTextMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import com.openxchange.office.dcs.config.JmsDestination;

@Component
public class BrokerStoppedNotifier implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(BrokerStoppedNotifier.class);

	private final JmsTemplate jmsTemplate;

	private final String brokerUrl;

	@Autowired
	public BrokerStoppedNotifier(JmsTemplate jmsTemplate, @Value("${spring.activemq.broker-url}") String brokerUrl) {
		this.jmsTemplate = jmsTemplate;
		this.brokerUrl = brokerUrl;
	}

	@Override
	public void run() {
		log.info("Shutdown of broker invoked!");
		jmsTemplate.send(JmsDestination.BROKER_STOPPED_TOPIC, s -> {
			ActiveMQTextMessage msg = new ActiveMQTextMessage();
			msg.setText(brokerUrl);
			return msg;
		});
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			log.warn(e.getMessage(), e);
		}
	}
}