/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.activemq;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQSslConnectionFactory;
import org.apache.activemq.management.JMSStatsImpl;
import org.apache.activemq.transport.Transport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

public class EnhActiveMQSslConnectionFactory extends ActiveMQSslConnectionFactory implements ConnectionsAwareActiveMqConnectionFactory {

    private static final Logger log = LoggerFactory.getLogger(EnhActiveMQSslConnectionFactory.class);

    private Set<ActiveMQConnection> connections = new HashSet<>();

    @Override
    protected ActiveMQConnection createActiveMQConnection(Transport transport, JMSStatsImpl stats) throws Exception {
        log.info("CreateActiveMQConnection to {}", transport);
        ActiveMQConnection res = super.createActiveMQConnection(transport, stats);
        synchronized (connections) {
            connections.add(res);
        }
        return res;
    }

	@Scheduled(fixedDelay=30000, initialDelay=30000)
    public Set<ActiveMQConnection> getConnections() {
        Set<ActiveMQConnection> res = new HashSet<>();
        synchronized (connections) {
            Iterator<ActiveMQConnection> ite = connections.iterator();
            while (ite.hasNext()) {
                ActiveMQConnection conn = ite.next();
                if (conn.isStarted()) {
                    res.add(conn);
                } else {
                    ite.remove();
                }
            }
        }
        return res;
    }
}
