/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.activemq;

import java.io.EOFException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import javax.annotation.PostConstruct;

import org.apache.activemq.network.NetworkBridge;
import org.apache.activemq.network.NetworkConnector;
import org.apache.activemq.transport.TransportDisposedIOException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.openxchange.office.dcs.config.DcsAddress;
import com.openxchange.office.dcs.service.BrokerAccessorService;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.turbo.TurboFilter;
import ch.qos.logback.core.spi.FilterReply;

@Component
public class LoggerConfig {

	public static final String INACTIVITY_MONITOR_THREAD_NAME = "ActiveMQ InactivityMonitor Worker";

	final BrokerAccessorService brokerAccessorService;

	@Autowired
	public LoggerConfig(BrokerAccessorService brokerAccessorService) {
		this.brokerAccessorService = brokerAccessorService;
	}

	@PostConstruct
	public void init() {
	    final LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
	    loggerContext.addTurboFilter(new TurboFilter() {

			@Override
			public FilterReply decide(Marker marker, ch.qos.logback.classic.Logger logger, Level level, String format, Object[] params, Throwable t) {
				if (t instanceof EOFException) {
					return FilterReply.DENY;
				}
				if (format != null) {
					if (format.startsWith("Network connection between ")) {
                        boolean found = false;
                        try {
    						DcsAddress toRemoteBrokerAddress = extractAndNormalizeRemoteBrokerAddress(params[1].toString());
    						for (NetworkConnector nc : brokerAccessorService.getBrokerService().getNetworkConnectors()) {
    							for (NetworkBridge nb : nc.activeBridges()) {
    							    try {
    							        // IMPORTANT:
    							        // Check DcsAddress instances as the NetworkBridge instance does not
    							        // provide the correct scheme, but just use a hard-coded "tcp" regardless
    							        // of the real connection (e.g. ssl etc.)
    							        // ATTENTION:
    							        // This code is relying on implementation details which can be changed
    							        // without any hint.
    	                                final DcsAddress remoteBrokerAddress = new DcsAddress(nb);
    	                                found = remoteBrokerAddress.equals(toRemoteBrokerAddress);
    							    } catch (URISyntaxException | UnknownHostException e) {
    							        // ignore exception - this is just a logging filter
    							    }
    								if (found) {
    									break;
    								}
    							}
    						}
                        } catch (URISyntaxException | UnknownHostException e) {
                            // ignore exception - this is just a logging filter
                        }
						if (!found) {
							return FilterReply.DENY;
						}
					}
				}
				if (level.equals(Level.WARN) && (t != null)) {
					for (StackTraceElement st : t.getStackTrace()) {
						if (st.getClassName().contains("AbstractInactivityMonitor")) {
							return FilterReply.DENY;
						}
					}
				}
				if ((format != null) && format.contains("Connection.start")) {
					return FilterReply.DENY;
				}
				return FilterReply.NEUTRAL;
			}

		    private DcsAddress extractAndNormalizeRemoteBrokerAddress(String toRemoteBrokerAddrString) throws UnknownHostException, URISyntaxException {
		        if (StringUtils.isEmpty(toRemoteBrokerAddrString))
		            throw new URISyntaxException("", "Empty string is not a valid uri string");

		        // ATTENTION:
		        // This is magic code which heavily relies on implementation specific details of ActiveMQ which
		        // can be changed by the developers without any hint/information. You have to check this
		        // code if you updated the ActiveMQ version.
		        int atIndex = toRemoteBrokerAddrString.indexOf("@");
		        String to = toRemoteBrokerAddrString.substring(0, ((atIndex >= 0) ? atIndex : toRemoteBrokerAddrString.length()));

		        //remove strange third slash in logging message to ensure equality between
		        // remoteBrokerAddresses
		        if (to.indexOf(":///") > 0) {
		        	to = to.replace(":///", "://");
		        }

		        DcsAddress remoteBrokerAddress = DcsAddress.createDcsAddressFromUriString(to);

		        return remoteBrokerAddress;
		    }
		});

	    loggerContext.addTurboFilter(new TurboFilter() {

			@Override
			public FilterReply decide(Marker marker, Logger logger, Level level, String format, Object[] params, Throwable t) {
				if ((brokerAccessorService.getBrokerService() != null) && (brokerAccessorService.getBrokerService().isStopping())) {
					if (t instanceof TransportDisposedIOException) {
						return FilterReply.DENY;
					}
				}
				return FilterReply.NEUTRAL;
			}
		});
	}

}
