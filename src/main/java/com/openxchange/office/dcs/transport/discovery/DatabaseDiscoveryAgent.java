/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.transport.discovery;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.activemq.command.DiscoveryEvent;
import org.apache.activemq.transport.discovery.DiscoveryAgent;
import org.apache.activemq.transport.discovery.DiscoveryListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DatabaseDiscoveryAgent implements DiscoveryAgent {

    private static final Logger log = LoggerFactory.getLogger(DatabaseDiscoveryAgent.class);
    
    private static final int DEFAULT_IDLE_TIME = 500;

    private static DatabaseDiscoveryAgent instance;
    private static List<DatabaseDiscoveryAgent> instances = new ArrayList<>();
    
    private long initialReconnectDelay = 1000 * 5;
    private long maxReconnectDelay = 1000 * 30;
    private long backOffMultiplier = 2;
    private boolean useExponentialBackOff;
    private int maxReconnectAttempts;

    private DiscoveryListener discoveryListener;
    private URI discoveryURI;
    private String selfService;
    private long keepAliveInterval = DEFAULT_IDLE_TIME;
    private AtomicBoolean started = new AtomicBoolean(false);

    class RemoteBrokerData extends DiscoveryEvent {
        long lastHeartBeat;
        long recoveryTime;
        int failureCount;
        boolean failed;
        String abc;

        public RemoteBrokerData(String brokerName, String service) {
            super(service);
            setBrokerName(brokerName);
            this.lastHeartBeat = System.currentTimeMillis();
        }

        public synchronized void updateHeartBeat() {
            lastHeartBeat = System.currentTimeMillis();

            // Consider that the broker recovery has succeeded if it has not
            // failed in 60 seconds.
            if (!failed && failureCount > 0 && (lastHeartBeat - recoveryTime) > 1000 * 60) {
                if (log.isInfoEnabled()) {
                    log.info("I now think that the " + serviceName + " service has recovered.");
                }
                failureCount = 0;
                recoveryTime = 0;
            }
        }

        public synchronized long getLastHeartBeat() {
            return lastHeartBeat;
        }

        public synchronized boolean markFailed() {
            if (!failed) {
                failed = true;
                failureCount++;

                long reconnectDelay;
                if (!useExponentialBackOff) {
                    reconnectDelay = initialReconnectDelay;
                } else {
                    reconnectDelay = (long)Math.pow(backOffMultiplier, failureCount);
                    if (reconnectDelay > maxReconnectDelay) {
                        reconnectDelay = maxReconnectDelay;
                    }
                }

                if (log.isDebugEnabled()) {
                    log.debug("Remote failure of " + serviceName + " while still receiving multicast advertisements.  Advertising events will be suppressed for " + reconnectDelay
                              + " ms, the current failure count is: " + failureCount);
                }

                recoveryTime = System.currentTimeMillis() + reconnectDelay;
                return true;
            }
            return false;
        }

        /**
         * @return true if this broker is marked failed and it is now the right
         *         time to start recovery.
         */
        public synchronized boolean doRecovery() {
            if (!failed) {
                return false;
            }

            // Are we done trying to recover this guy?
            if (maxReconnectAttempts > 0 && failureCount > maxReconnectAttempts) {
                if (log.isDebugEnabled()) {
                    log.debug("Max reconnect attempts of the " + serviceName + " service has been reached.");
                }
                return false;
            }

            // Is it not yet time?
            if (System.currentTimeMillis() < recoveryTime) {
                return false;
            }

            if (log.isDebugEnabled()) {
                log.debug("Resuming event advertisement of the " + serviceName + " service.");
            }
            failed = false;
            return true;
        }

        public boolean isFailed() {
            return failed;            
        }

		@Override
		public String toString() {
			return "RemoteBrokerData [lastHeartBeat=" + lastHeartBeat + ", recoveryTime=" + recoveryTime
					+ ", failureCount=" + failureCount + ", failed=" + failed + "]";
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((getServiceName() == null) ? 0 : getServiceName().hashCode());
			result = prime * result + ((getBrokerName() == null) ? 0 : getBrokerName().hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			RemoteBrokerData other = (RemoteBrokerData) obj;
			if (!getServiceName().equals(other.getServiceName()))
				return false;
			if (!getBrokerName().equals(other.getBrokerName()))
				return false;
			return true;
		}
    }

    public static DatabaseDiscoveryAgent getInstance() {
    	if ((instance == null) || (instance.getDiscoveryListener() == null)) {
	    	for (DatabaseDiscoveryAgent agent : instances) {
	    		if (agent.getDiscoveryListener() != null) {
	    			instance = agent;
	    			break;
	    		}
	    	}
    	}
    	if (instance == null) {
    		throw new NullPointerException("DatabaseDiscoveryAgent has not been initialized yet!");
    	}
    	return instance;
    }
    
    public static void resetDatabaseDiscoveryAgent() {
    	instance = null;
    	instances.clear();
    }
    
    public DatabaseDiscoveryAgent() {
    	instance = this;
    	instances.add(this);
    }
    
    /**
     * Set the discovery listener
     * 
     * @param listener
     */
    public void setDiscoveryListener(DiscoveryListener listener) {
        this.discoveryListener = listener;
    }

    public DiscoveryListener getDiscoveryListener() {
		return discoveryListener;
	}

	/**
     * @return the discoveryURI
     */
    public URI getDiscoveryURI() {
        return discoveryURI;
    }

    /**
     * Set the discoveryURI
     * 
     * @param discoveryURI
     */
    public void setDiscoveryURI(URI discoveryURI) {
        this.discoveryURI = discoveryURI;
    }    
    
    /**
     * register a service
     */
    public void registerService(String name) {
        this.selfService = name;
    }

    public long getKeepAliveInterval() {
        return keepAliveInterval;
    }

    public void setKeepAliveInterval(long keepAliveInterval) {
        this.keepAliveInterval = keepAliveInterval;
    }
    
    /**
     * start the discovery agent
     * 
     * @throws Exception
     */
    public void start() throws Exception {
    	
        started.compareAndSet(false, true);        	
    }
    
    /**
     * stop the channel
     * 
     * @throws Exception
     */
    public void stop() throws Exception {
        started.compareAndSet(true, false);        
    }

    public void processAlive(String brokerName, String service) {
    	log.info("processAlive with brokerName {} and service {}. selfService: {}", brokerName, service, selfService);
        if (!service.equals(selfService)) {
            RemoteBrokerData data = new RemoteBrokerData(brokerName, service);
            fireServiceAddEvent(data);
        } else {
        	log.warn("Service to add is equal to selfservice");
        }
    }

    public void processDead(String service) {
    	log.info("processDead for service {}. selfService: {}", service, selfService);
        if (!service.equals(selfService)) {
            RemoteBrokerData data = new RemoteBrokerData(service, service);
            fireServiceRemovedEvent(data);
        } else {
        	log.warn("Service to remove is equal to selfservice");
        }
    }

    public void serviceFailed(DiscoveryEvent event) throws IOException {
    	log.info("serviceFailed for broker {}", event.getServiceName());
        RemoteBrokerData data = new RemoteBrokerData(event.getBrokerName(), event.getServiceName());
        fireServiceRemovedEvent(data);
    }

    private void fireServiceRemovedEvent(final RemoteBrokerData data) {
    	log.info("fireServiceRemovedEvent with RemoteBrokerData {}. started: {}, discoveryListener is null: {}" , data, started.get(), discoveryListener == null);
        if (discoveryListener != null && started.get()) {
        	discoveryListener.onServiceRemove(data);
        } else {
        	if (!started.get()) {
        		log.warn("Service {} is stopped!", selfService);
        	} else {
        		log.error("No discovery listener registered!");
        	}
        }
    }

    private void fireServiceAddEvent(final RemoteBrokerData data) {
    	log.info("fireServiceAddEvent with RemoteBrokerData {}. started: {}, discoveryListener is null: {}" , data, started.get(), discoveryListener == null);
        if (discoveryListener != null && started.get()) {
        	discoveryListener.onServiceAdd(data);
        } else {
        	if (!started.get()) {
        		log.warn("Service {} is stopped!", selfService);
        	} else {
        		log.error("No discovery listener registered!");
        	}
        }
    }

    public long getBackOffMultiplier() {
        return backOffMultiplier;
    }

    public void setBackOffMultiplier(long backOffMultiplier) {
        this.backOffMultiplier = backOffMultiplier;
    }

    public long getInitialReconnectDelay() {
        return initialReconnectDelay;
    }

    public void setInitialReconnectDelay(long initialReconnectDelay) {
        this.initialReconnectDelay = initialReconnectDelay;
    }

    public int getMaxReconnectAttempts() {
        return maxReconnectAttempts;
    }

    public void setMaxReconnectAttempts(int maxReconnectAttempts) {
        this.maxReconnectAttempts = maxReconnectAttempts;
    }

    public long getMaxReconnectDelay() {
        return maxReconnectDelay;
    }

    public void setMaxReconnectDelay(long maxReconnectDelay) {
        this.maxReconnectDelay = maxReconnectDelay;
    }

    public boolean isUseExponentialBackOff() {
        return useExponentialBackOff;
    }

    public void setUseExponentialBackOff(boolean useExponentialBackOff) {
        this.useExponentialBackOff = useExponentialBackOff;
    }

    @Override
    public String toString() {
        return  "DatabaseDiscoveryAgent-"
            + (selfService != null ? "advertise:" + selfService : "listener:" + this.discoveryListener);
    }
}
