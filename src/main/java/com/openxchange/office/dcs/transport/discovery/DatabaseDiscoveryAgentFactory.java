/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.transport.discovery;

import java.io.IOException;
import java.net.URI;
import java.util.Map;

import org.apache.activemq.transport.discovery.DiscoveryAgent;
import org.apache.activemq.transport.discovery.DiscoveryAgentFactory;
import org.apache.activemq.util.IOExceptionSupport;
import org.apache.activemq.util.IntrospectionSupport;
import org.apache.activemq.util.URISupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DatabaseDiscoveryAgentFactory extends DiscoveryAgentFactory {

    private static final Logger log = LoggerFactory.getLogger(DatabaseDiscoveryAgentFactory.class); 


    protected DiscoveryAgent doCreateDiscoveryAgent(URI uri) throws IOException {
        try {
            if (log.isTraceEnabled()) {
               log.trace("doCreateDiscoveryAgent: uri = " + uri.toString());
            }

            DatabaseDiscoveryAgent mda = new DatabaseDiscoveryAgent();

            mda.setDiscoveryURI(uri);

            // allow MDA's params to be set via query arguments  
            // (e.g., multicast://default?group=foo
            @SuppressWarnings("rawtypes")
            Map options = URISupport.parseParameters(uri);
            IntrospectionSupport.setProperties(mda, options);

            return mda;
        } catch (Throwable e) {
            throw IOExceptionSupport.create("Could not create discovery agent: " + uri, e);
        }
    }
}
