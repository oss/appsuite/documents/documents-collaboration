/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.metrics;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.IntrospectionException;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JmxHelper {
    private static final Logger log = LoggerFactory.getLogger(JmxHelper.class);

    private JmxHelper() {
        // nothing to do - helper class
    }

    public static MBeanInfo getMBeanInfo(MBeanServer mbeanServer, String objName) {
        MBeanInfo result = null;

        try {
            Object value = mbeanServer.getMBeanInfo(new ObjectName(objName));
            if (value instanceof MBeanInfo) {
                result = (MBeanInfo) value;
            }
        } catch (MalformedObjectNameException e) {
            log.warn("ObjectName malformed: " + objName + ". Cannot retrieve MBeanInfo instance.", e);
        } catch (ReflectionException e) {
            log.warn("ReflectionException caught! Cannot retrieve MBeanInfo instance for " + objName + ".", e);
        } catch (IntrospectionException e) {
            log.warn("IntrospectionException caught! Cannot retrieve MBeanInfo instance for " + objName + ".", e);
        } catch (InstanceNotFoundException e) {
            // nothing to do - may be jmx attributes are not yet added by ActiveMQ
        }

        return result;
    }

    public static Object getAttributeValue(MBeanServer mbeanServer, String viewRootName, String objName) {
        Object result = null;

        try {
            result = mbeanServer.getAttribute(new ObjectName(viewRootName), objName);
        } catch (MalformedObjectNameException e) {
            log.warn("ObjectName malformed: " + viewRootName, e);
        } catch (AttributeNotFoundException e) {
            // nothing to do - may be jmx attributes are not added by ActiveMQ
        } catch (ReflectionException e) {
            log.warn("ReflectionException caught! Cannot provide value for " + objName, e);
        } catch (MBeanException e) {
            log.warn("MBeanException caught! Cannot provide value for " + objName, e);
        } catch (InstanceNotFoundException e) {
            // nothing to do - may be jmx attributes are not yet added by ActiveMQ
        }

        return result;
    }

    public static Long mapObjectToLong(Object value) {
        long result = 0;
        if (value != null) {
            if (value instanceof Integer) {
                result = (int)value;
            } else if (value instanceof Long) {
                result = (Long)value;
            } else if (value instanceof String) {
                result = Long.valueOf((String)value);
            }
        }
        return result;
    }

    public static Number createNumberType(Class<?> numType) throws IllegalArgumentException {
        if (numType.equals(Long.class))
            return Long.valueOf(0);
        else if (numType.equals(Integer.class))
            return Integer.valueOf(0);
        else if (numType.equals(Double.class))
            return Double.valueOf(0);
        else if (numType.equals(Float.class))
            return (Float.valueOf(0));

        throw new IllegalArgumentException("Factory cannot create instance for type " + numType);
    }

}
