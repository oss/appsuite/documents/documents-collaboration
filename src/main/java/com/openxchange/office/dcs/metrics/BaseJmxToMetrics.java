/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.metrics;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanInfo;
import javax.management.MBeanServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.micrometer.core.instrument.MeterRegistry;

public abstract class BaseJmxToMetrics {

    private static final Logger log = LoggerFactory.getLogger(BrokerViewMetrics.class);

    private String                  jmxRootObjName;
    private AtomicBoolean           initialized = new AtomicBoolean(false);

    public static class MetricAttrInfo {
        public String metricName;
        public String description = null;
        public String baseUnit = null;
        public Class<?> attrTypeExpected;

        public MetricAttrInfo(String metricName, Class<?> attrTypeExpected) {
            this.metricName = metricName;
            this.attrTypeExpected = attrTypeExpected;
        }

        public MetricAttrInfo(String metricName, String description, String baseUnit, Class<?> attrTypeExcepted) {
            this(metricName, attrTypeExcepted);
            this.description = description;
            this.baseUnit = baseUnit;
        }
    }

    public BaseJmxToMetrics() {
        // nothing to do
    }

    public boolean isInitialized() {
        return initialized.get();
    }

    public String getJmxRootObjectName() {
        return jmxRootObjName;
    }

    public void setJmxRTootObjectName(String jmxRootObjName) {
        this.jmxRootObjName = jmxRootObjName;
    }

    public final void initMetrics(MeterRegistry meterRegistry, String brokerName) {
        if (initialized.compareAndSet(false, true)) {
            doInitMetrics(meterRegistry, brokerName);
        }
    }

    public final void updateMetrics(MBeanServer mbeanServer, boolean ignoreErrorIfNotExists) {
        if (initialized.get()) {
            doUpdateMetrics(mbeanServer, ignoreErrorIfNotExists);
        }
    }

    protected void doUpdateMetrics(MBeanServer mbeanServer, boolean ignoreErrorIfNotExists) {
        MBeanInfo brokerViewMBeanInfo = JmxHelper.getMBeanInfo(mbeanServer, this.getJmxRootObjectName());
        if (brokerViewMBeanInfo != null) {
            MBeanAttributeInfo[] attrInfo = brokerViewMBeanInfo.getAttributes();
            if (attrInfo != null) {
              Collection<MBeanAttributeInfo> attrs = Arrays.asList(attrInfo);
              attrs.stream().forEach(attr -> {
                  updateMetric(mbeanServer, attr, ignoreErrorIfNotExists);
              });
            }
        }
    }

    protected void updateMetric(MBeanServer mbeanServer, MBeanAttributeInfo mbeanAttrInfo, boolean ignoreErrorIfNotExists) {
        String attrName = mbeanAttrInfo.getName();
        MetricAttrInfo metricAttrInfo = getAttributesToMetricsMap().get(attrName);
        if (metricAttrInfo != null) {
            String metricName = metricAttrInfo.metricName;
            Object value = JmxHelper.getAttributeValue(mbeanServer, this.getJmxRootObjectName(), attrName);
            if (value != null) {
                if (value.getClass() != metricAttrInfo.attrTypeExpected) {
                    log.info("Unexpected type {} for jmx attribute {}, expected type {} ", value.getClass(), mbeanAttrInfo.getName(), metricAttrInfo.attrTypeExpected);
                    return;
                }

                final AtomicNumberWrapper metricValueInstance = getMetricsToInstanceMap().get(metricName);
                if (metricValueInstance != null) {
                    metricValueInstance.set((Number)value);
                } else if (!ignoreErrorIfNotExists) {
                    log.warn("BrokerViewMetrics no mapping to instance value for {} metric name found. Check the mappings!, metricName");
                }
            }
        }
    }

    protected abstract void doInitMetrics(MeterRegistry meterRegistry, String brokerName);

    protected abstract Map<String, MetricAttrInfo> getAttributesToMetricsMap();

    protected abstract Map<String, AtomicNumberWrapper> getMetricsToInstanceMap();

    public static String createMetricsFullName(String metricName) {
        StringBuilder tmp = new StringBuilder(MetricConstants.GROUP_BASE);
        tmp.append(".");
        tmp.append(metricName);
        return tmp.toString();
    }
}
