/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.metrics;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import java.lang.management.ManagementFactory;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import javax.annotation.PostConstruct;
import javax.management.MBeanServer;
import javax.management.ObjectInstance;
import javax.management.ObjectName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ActiveMqMetrics {

    private static final Logger log = LoggerFactory.getLogger(ActiveMqMetrics.class);

    private static final String PREFIX_OBJNAME_ACTIVEMQ_HEALTH  = "org.apache.activemq:type=Broker,brokerName=";
    private static final String POSTFIX_OBJNAME_ACTIVEMQ_HEALTH = ",service=Health";
    private static final String PREFIX_OBJNAME_ACTIVEMQ_NC      = "org.apache.activemq:type=Broker,brokerName=";
    private static final String POSTFIX_OBJNAME_ACTIVEMQ_NC     = ",connector=networkConnectors,networkConnectorName=NC,networkBridge=*";
    private static final String METRIC_PREFIX                   = "org_apache_activemq_Broker";
    private static final String METRIC_POSTFIX_1                = "{brokerName=\"";
    private static final String METRIC_POSTFIX_2                = "\"},";
    private static final String METRIC_HEALTH                   = "health";
    private static final String METRIC_BROKERS_NC_COUNT         = "brokers.count";
    private static final String ACTIVEMQ_HEALTH_GOOD            = "good";

    private MeterRegistry              meterRegistry;
    private String                     brokerName;
    private AtomicLong                 healthState                = new AtomicLong(100);
    private AtomicLong                 brokerNCCouunt             = new AtomicLong(0);
    private BrokerViewMetrics          brokerViewMetrics          = new BrokerViewMetrics();
    private ClientResponseTopicMetrics clientResponseTopicMetrics = new ClientResponseTopicMetrics();

    @Autowired
    public ActiveMqMetrics(MeterRegistry meterRegistry, @Value("${dcs.name:dcs_localhost_61616}") String brokerName) {
        this.meterRegistry = meterRegistry;
        this.brokerName = brokerName;
    }

/*
 *     public static <T> Gauge registerOrUpdateGauge(MeterRegistry registry, String name, Tags tags, @Nullable String description, @Nullable String baseUnit, @Nullable T obj, ToDoubleFunction<T> f) {
        Gauge oldGauge = registry.find(name).tags(tags).gauge();
        if (oldGauge != null) {
            registry.remove(oldGauge);
        }
        return Gauge.builder(name, obj, f)
            .description(description)
            .tags(tags)
            .baseUnit(baseUnit)
            .register(registry);
 */

    @PostConstruct
    public void initMetrics() {
        Gauge.builder(MetricConstants.GROUP_BASE + "." + METRIC_HEALTH, healthState, (m) -> m.get()).description("The health state of the DCS ActiveMQ broker.").tags(MetricConstants.TAG_BROKER, brokerName).baseUnit(MetricConstants.UNIT_PERCENT).register(meterRegistry);
        Gauge.builder(MetricConstants.GROUP_BASE + "." + METRIC_BROKERS_NC_COUNT, brokerNCCouunt, (m) -> m.get()).description("The number of connected DCS ActiveMQ brokers.").baseUnit(null).tags(MetricConstants.TAG_BROKER, brokerName).register(meterRegistry);
        brokerViewMetrics.initMetrics(meterRegistry, brokerName);
        clientResponseTopicMetrics.initMetrics(meterRegistry, brokerName);
    }

    @Scheduled(initialDelay = 3000, fixedDelay = 10000)
    private void pollJMXValueFromActiveMq() {
        MBeanServer mbeanServer = ManagementFactory.getPlatformMBeanServer();
        if (mbeanServer != null) {
            updateActiveMqHealthFromJMX(mbeanServer);
            updateActiveMqNCBrokerCount(mbeanServer);
            brokerViewMetrics.updateMetrics(mbeanServer, false);
            clientResponseTopicMetrics.updateMetrics(mbeanServer, true);
        }
    }

    private void updateActiveMqHealthFromJMX(MBeanServer mbeanServer) {
        String objName = getCompletObjNameForActiveMqHealth();
        Object value = JmxHelper.getAttributeValue(mbeanServer, objName, "CurrentStatus");
        if (value instanceof String) {
            healthState.set(ACTIVEMQ_HEALTH_GOOD.equalsIgnoreCase((String) value) ? 100 : 0);
        } else {
            log.error("Answer from MBeanServer malformed. Cannot provide any information about the health state!");
            healthState.set(0);
        }
    }

    private void updateActiveMqNCBrokerCount(MBeanServer mbeanServer) {
        try {
            String objNamePattern = getCompleteObjNamePatternForActiveMqNCEntries();
            Set<ObjectInstance> set = mbeanServer.queryMBeans(new ObjectName(objNamePattern), null);
            brokerNCCouunt.set(1 + set.size());
        } catch (Exception e) {
            log.error("Exception caught trying to determine number of network brokers", e);
        }
    }

    private String getCompletObjNameForActiveMqHealth() {
        StringBuilder tmp = new StringBuilder(PREFIX_OBJNAME_ACTIVEMQ_HEALTH);
        tmp.append(brokerName);
        tmp.append(POSTFIX_OBJNAME_ACTIVEMQ_HEALTH);
        return tmp.toString();
    }

    private String getCompleteObjNamePatternForActiveMqNCEntries() {
        StringBuilder tmp = new StringBuilder(PREFIX_OBJNAME_ACTIVEMQ_NC);
        tmp.append(brokerName);
        tmp.append(POSTFIX_OBJNAME_ACTIVEMQ_NC);
        return tmp.toString();
    }

    private String getFullMetricName(String brokerName, String metricBaseName) {
        StringBuilder tmp = new StringBuilder(METRIC_PREFIX);
        tmp.append(metricBaseName);
        tmp.append(METRIC_POSTFIX_1);
        tmp.append(brokerName);
        tmp.append(METRIC_POSTFIX_2);
        return tmp.toString();
    }

}
