/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.metrics;

import java.util.HashMap;
import java.util.Map;
import javax.management.MBeanServer;
import io.micrometer.core.instrument.Gauge;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import io.micrometer.core.instrument.MeterRegistry;

public class ClientResponseTopicMetrics extends BaseJmxToMetrics {

    //private static final Logger log = LoggerFactory.getLogger(ClientResponseTopicMetrics.class);

    private static final String OBJNAME_CLIENT_RESPONSE_TOPIC_MBEANINFO_PREFIX  = "org.apache.activemq:type=Broker,brokerName=";
    private static final String OBJNAME_CLIENT_RESPONSE_TOPIC_MBEANINFO_POSTFIX = ",destinationType=Topic,destinationName=ActiveMQ.Advisory.Consumer.Topic.clientResponse-Topic";

    private static final String ATTR_NAME_AVERAGE_BLOCKED_TIME                  = "AverageBlockedTime";
    private static final String ATTR_NAME_AVERAGE_ENQUEUE_TIME                  = "AverageEnqueueTime";
    private static final String ATTR_NAME_AVERAGE_MESSAGE_SIZE                  = "AverageMessageSize";
    private static final String ATTR_NAME_CONSUMER_COUNT                        = "ConsumerCount";
    private static final String ATTR_NAME_DEQUEUE_COUNT                         = "DequeueCount";
    private static final String ATTR_NAME_ENQUEUE_COUNT                         = "EnqueueCount";
    private static final String ATTR_NAME_IN_FLIGHT_COUNT                       = "InFlightCount";
    private static final String ATTR_NAME_MAX_ENQUEUE_TIME                      = "MaxEnqueueTime";
    private static final String ATTR_NAME_MAX_MESSAGE_SIZE                      = "MaxMessageSize";
    private static final String ATTR_NAME_MEMORY_PERCENTAGE_USAGE               = "MemoryPercentUsage";
    private static final String ATTR_NAME_QUEUE_SIZE                            = "QueueSize";

    private static final String METRIC_AVERAGE_BLOCKED_TIME                     = "blocked.average.time";
    private static final String METRIC_AVERAGE_ENQUEUE_TIME                     = "enqueue.average.time";
    private static final String METRIC_AVERAGE_MSG_SIZE                         = "message.average.size";
    private static final String METRIC_CONSUMER_COUNT                           = "consumer.count";
    private static final String METRIC_DEQUEUE_COUNT                            = "dequeue.count";
    private static final String METRIC_ENQUEUE_COUNT                            = "enqueue.count";
    private static final String METRIC_IN_FLIGHT_COUNT                          = "in.flight.count";
    private static final String METRIC_MAX_ENQUEUE_TIME                         = "enqueue.max.time";
    private static final String METRIC_MAX_MSG_SIZE                             = "message.max.size";
    private static final String METRIC_MEMORY_PERCENTAGE_USAGE                  = "message.usage";
    private static final String METRIC_QUEUE_SIZE                               =" queue.size";

    private static final String TOPIC_NAME                                      = "clientResponse-Topic";

    private Map<String, AtomicNumberWrapper> metricNameToInstanceMap     = new HashMap<>();
    private Map<String, MetricAttrInfo>      beanAttrNameToMetricInfoMap = new HashMap<>();

    public void doInitMetrics(MeterRegistry meterRegistry, String brokerName) {
        StringBuilder tmp = new StringBuilder(OBJNAME_CLIENT_RESPONSE_TOPIC_MBEANINFO_PREFIX);
        tmp.append(brokerName);
        tmp.append(OBJNAME_CLIENT_RESPONSE_TOPIC_MBEANINFO_POSTFIX);

        String jmxRootObjectName = tmp.toString();
        setJmxRTootObjectName(jmxRootObjectName);

        addMappingEntries(ATTR_NAME_AVERAGE_BLOCKED_TIME, METRIC_AVERAGE_BLOCKED_TIME, brokerName, "Average time (ms) messages have been blocked by flow control.", MetricConstants.UNIT_MS, Double.class);
        addMappingEntries(ATTR_NAME_AVERAGE_ENQUEUE_TIME, METRIC_AVERAGE_ENQUEUE_TIME, brokerName, "Average time a message was held on this destination.", MetricConstants.UNIT_MS, Double.class);
        addMappingEntries(ATTR_NAME_AVERAGE_MESSAGE_SIZE, METRIC_AVERAGE_MSG_SIZE, brokerName, "Average message size on this destination.", MetricConstants.UNIT_NO, Long.class);
        addMappingEntries(ATTR_NAME_CONSUMER_COUNT, METRIC_CONSUMER_COUNT, brokerName, "Number of consumers subscribed to this destination.", MetricConstants.UNIT_NO, Long.class);
        addMappingEntries(ATTR_NAME_DEQUEUE_COUNT, METRIC_DEQUEUE_COUNT, brokerName, "Number of messages that has been acknowledged (and removed) from the destination.", MetricConstants.UNIT_NO, Long.class);
        addMappingEntries(ATTR_NAME_ENQUEUE_COUNT, METRIC_ENQUEUE_COUNT, brokerName, "Number of messages that have been sent to the destination.", MetricConstants.UNIT_NO, Long.class);
        addMappingEntries(ATTR_NAME_IN_FLIGHT_COUNT, METRIC_IN_FLIGHT_COUNT, brokerName, "Number of messages that have been dispatched to, but not acknowledged by, consumers.", MetricConstants.UNIT_NO, Long.class);
        addMappingEntries(ATTR_NAME_MAX_ENQUEUE_TIME, METRIC_MAX_ENQUEUE_TIME, brokerName, "The longest time a message was held on this destination", MetricConstants.UNIT_MS, Long.class);
        addMappingEntries(ATTR_NAME_MAX_MESSAGE_SIZE, METRIC_MAX_MSG_SIZE, brokerName, "Max message size on this destination.", MetricConstants.UNIT_NO, Long.class);
        addMappingEntries(ATTR_NAME_MEMORY_PERCENTAGE_USAGE, METRIC_MEMORY_PERCENTAGE_USAGE, brokerName, "The percentage of the memory limit used.", MetricConstants.UNIT_PERCENT, Integer.class);
        addMappingEntries(ATTR_NAME_QUEUE_SIZE, METRIC_QUEUE_SIZE, brokerName, "Number of messages on this destination, including any that have been dispatched but not acknowledged.", MetricConstants.UNIT_NO, Long.class);

        beanAttrNameToMetricInfoMap.values().stream().forEach(v -> {
            Gauge.builder(v.metricName, metricNameToInstanceMap.get(v.metricName), m -> m.doubleValue())
                 .tags(MetricConstants.TAG_BROKER, brokerName, MetricConstants.TAG_DESTINATION_TYPE, "topic", MetricConstants.TAG_TOPC, TOPIC_NAME)
                 .description(v.description)
                 .baseUnit(v.baseUnit)
                 .register(meterRegistry);
        });
    }

    @Override
    protected void doUpdateMetrics(MBeanServer mbeanServer, boolean ignoreErrorIfNotExists) {
        super.doUpdateMetrics(mbeanServer, ignoreErrorIfNotExists);
    }

    @Override
    protected Map<String, MetricAttrInfo> getAttributesToMetricsMap() {
        return beanAttrNameToMetricInfoMap;
    }

    @Override
    protected Map<String, AtomicNumberWrapper> getMetricsToInstanceMap() {
        return metricNameToInstanceMap;
    }

    private void addMappingEntries(String jmxAttrName, String metricName, String brokerName, String description, String baseUnits, Class<?> expectedAttrType) {
        String metricFullName = BaseJmxToMetrics.createMetricsFullName(metricName);
        metricNameToInstanceMap.put(metricFullName, new AtomicNumberWrapper(JmxHelper.createNumberType(expectedAttrType)));
        beanAttrNameToMetricInfoMap.put(jmxAttrName, new MetricAttrInfo(metricFullName, description, baseUnits, expectedAttrType));
    }

}
