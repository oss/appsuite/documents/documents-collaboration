/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.metrics;

import java.util.HashMap;
import java.util.Map;
import javax.management.MBeanServer;
import io.micrometer.core.instrument.Gauge;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import io.micrometer.core.instrument.MeterRegistry;

public class BrokerViewMetrics extends BaseJmxToMetrics {

    //private static final Logger log = LoggerFactory.getLogger(BrokerViewMetrics.class);

    private static final String OBJNAME_ACTIVEMQ_BROKER_MBEANINFO = "org.apache.activemq:type=Broker,brokerName=";

    private static final String ATTR_NAME_CURRENT_CONNECTIONS_COUNT = "CurrentConnectionsCount";
    private static final String ATTR_NAME_STORE_PERCENTAGE_USAGE    = "StorePercentUsage";
    private static final String ATTR_NAME_TEMP_PERCENTAGE_USAGE     = "TempPercentUsage";
    private static final String ATTR_NAME_TOTAL_CONNECTIONS_COUNT   = "TotalConnectionsCount";
    private static final String ATTR_NAME_TOTAL_CONSUMER_COUNT      = "TotalConsumerCount";
    private static final String ATTR_NAME_TOTAL_DEQUEUE_COUNT       = "TotalDequeueCount";
    private static final String ATTR_NAME_TOTAL_ENQUEUE_COUNT       = "TotalEnqueueCount";
    private static final String ATTR_NAME_UPTIME_MILLIS             = "UptimeMillis";
    private static final String ATTR_NAME_MEMORY_PERCENTAGE_USAGE   = "MemoryPercentUsage";
    private static final String ATTR_NAME_TOTAL_MESSAGE_COUNT       = "TotalMessageCount";

    private static final String METRIC_CURRENT_CONNECTIONS_COUNT = "current.connections.count";
    private static final String METRIC_STORE_PERCENTAGE_USAGE    = "store.usage";
    private static final String METRIC_TEMP_PERCENTAGE_USAGE     = "store.temp.usage";
    private static final String METRIC_TOTAL_CONNECTIONS_COUNT   = "connections.count.total";
    private static final String METRIC_TOTAL_CONSUMER_COUNT      = "consumer.count.total";
    private static final String METRIC_TOTAL_DEQUEUE_COUNT       = "dequeue.count.total";
    private static final String METRIC_TOTAL_ENQUEUE_COUNT       = "enqueue.count.total";
    private static final String METRIC_UPTIME_MILLIS             = "up.time";
    private static final String METRIC_MEMORY_PERCENTAGE_USAGE   = "memory.usage";
    private static final String METRIC_TOTAL_MSG_COUNT           = "message.count.total";

    private final Map<String, AtomicNumberWrapper> metricNameToInstanceMap  = new HashMap<>();
    private final Map<String, MetricAttrInfo>      beanAttrNameToMetricName = new HashMap<>();

    public BrokerViewMetrics() {
        // nothing to do
    }

    protected void doInitMetrics(MeterRegistry meterRegistry, String brokerName) {
        StringBuilder tmp = new StringBuilder(OBJNAME_ACTIVEMQ_BROKER_MBEANINFO);
        tmp.append(brokerName);
        setJmxRTootObjectName(tmp.toString());

        addMappingEntries(ATTR_NAME_CURRENT_CONNECTIONS_COUNT, METRIC_CURRENT_CONNECTIONS_COUNT, brokerName, "Current number of connections.", MetricConstants.UNIT_NO, Integer.class);
        addMappingEntries(ATTR_NAME_STORE_PERCENTAGE_USAGE, METRIC_STORE_PERCENTAGE_USAGE, brokerName, "Percent of store limit used.", MetricConstants.UNIT_PERCENT, Integer.class);
        addMappingEntries(ATTR_NAME_TEMP_PERCENTAGE_USAGE, METRIC_TEMP_PERCENTAGE_USAGE, brokerName, "Percent of temp limit used.", MetricConstants.UNIT_PERCENT, Integer.class);
        addMappingEntries(ATTR_NAME_TOTAL_CONNECTIONS_COUNT, METRIC_TOTAL_CONNECTIONS_COUNT, brokerName, "Total number of connections.", MetricConstants.UNIT_NO, Long.class);
        addMappingEntries(ATTR_NAME_TOTAL_CONSUMER_COUNT, METRIC_TOTAL_CONSUMER_COUNT, brokerName, "Number of message consumers subscribed to destinations on the broker.", MetricConstants.UNIT_NO,  Long.class);
        addMappingEntries(ATTR_NAME_TOTAL_DEQUEUE_COUNT, METRIC_TOTAL_DEQUEUE_COUNT, brokerName, "Number of messages that have been acknowledged on the broker.", MetricConstants.UNIT_NO, Long.class);
        addMappingEntries(ATTR_NAME_TOTAL_ENQUEUE_COUNT, METRIC_TOTAL_ENQUEUE_COUNT, brokerName, "Number of messages that have been sent to the broker.", MetricConstants.UNIT_NO, Long.class);
        addMappingEntries(ATTR_NAME_TOTAL_MESSAGE_COUNT, METRIC_TOTAL_MSG_COUNT, brokerName, "Number of unacknowledged messages on the broker.", MetricConstants.UNIT_NO, Long.class);
        addMappingEntries(ATTR_NAME_UPTIME_MILLIS, METRIC_UPTIME_MILLIS, brokerName, "Uptime of the broker in milliseconds.", MetricConstants.UNIT_MS, Long.class);
        addMappingEntries(ATTR_NAME_MEMORY_PERCENTAGE_USAGE, METRIC_MEMORY_PERCENTAGE_USAGE, brokerName, "Percent of memory limit used.", MetricConstants.UNIT_PERCENT, Integer.class);

        beanAttrNameToMetricName.values().stream().forEach(v -> {
            Gauge.builder(v.metricName, metricNameToInstanceMap.get(v.metricName), m -> m.doubleValue())
                 .tag(MetricConstants.TAG_BROKER, brokerName)
                 .description(v.description)
                 .baseUnit(v.baseUnit)
                 .register(meterRegistry);
        });
    }

    public void doUpdateMetrics(MBeanServer mbeanServer, boolean ignoreErrorIfNotExists) {
        super.doUpdateMetrics(mbeanServer, ignoreErrorIfNotExists);
    }

    @Override
    protected Map<String, MetricAttrInfo> getAttributesToMetricsMap() {
        return beanAttrNameToMetricName;
    }

    @Override
    protected Map<String, AtomicNumberWrapper> getMetricsToInstanceMap() {
        return metricNameToInstanceMap;
    }

    private void addMappingEntries(String jmxAttrName, String metricName, String brokerName, String description, String baseUnits, Class<?> expectedAttrType) {
        String metricFullName = BaseJmxToMetrics.createMetricsFullName(metricName);
        metricNameToInstanceMap.put(metricFullName, new AtomicNumberWrapper(JmxHelper.createNumberType(expectedAttrType)));
        beanAttrNameToMetricName.put(jmxAttrName, new MetricAttrInfo(metricFullName, description, baseUnits, expectedAttrType));
    }

}
