/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.metrics;

import java.util.HashSet;
import java.util.Set;

import javax.jms.JMSException;

import org.apache.activemq.jms.pool.PooledConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

import com.openxchange.office.dcs.activemq.ConnectionsAwareActiveMqConnectionFactory;

@Component
@ManagedResource
public class ActiveMqConnectionFactoryMBean {
	
	private static final Logger log = LoggerFactory.getLogger(ActiveMqConnectionFactoryMBean.class);
	
	private final PooledConnectionFactory connFactory;

	@Autowired
	public ActiveMqConnectionFactoryMBean(PooledConnectionFactory connFactory) {
		this.connFactory = connFactory;
	}
	
	@ManagedAttribute
	public Set<String> getCurrentJmsConnections() throws Exception {
		ConnectionsAwareActiveMqConnectionFactory amqConnFact = (ConnectionsAwareActiveMqConnectionFactory) this.connFactory.getConnectionFactory();
		Set<String> res = new HashSet<>();		
		amqConnFact.getConnections().forEach(c -> {
			try {
				res.add("ClientId: " + c.getClientID().toString() + ", BrokerId: " + c.getBrokerInfo().getBrokerId() + ", BrokerUrl: " + c.getBrokerInfo().getBrokerURL() + ", BrokerName: " + c.getBrokerInfo().getBrokerName());
			} catch (JMSException e) {
				log.info(e.getMessage());
			}
		});
		return res;
		
	}
}
