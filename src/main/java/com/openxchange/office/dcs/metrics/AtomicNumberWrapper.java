/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.metrics;

import java.util.concurrent.atomic.AtomicReference;

public class AtomicNumberWrapper extends Number {

    private static final long serialVersionUID = -1501751898866992482L;
    private AtomicReference<Number> value = new AtomicReference<>();

    public AtomicNumberWrapper(Number value) {
        this.value.set(value);
    }

    public void set(Number value) {
        this.value.set(value);
    }

    @Override
    public int intValue() {
        return value.get().intValue();
    }

    @Override
    public long longValue() {
        return value.get().longValue();
    }

    @Override
    public float floatValue() {
        return value.get().floatValue();
    }

    @Override
    public double doubleValue() {
        return value.get().doubleValue();
    }

}
