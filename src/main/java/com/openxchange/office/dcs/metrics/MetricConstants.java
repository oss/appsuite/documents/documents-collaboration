package com.openxchange.office.dcs.metrics;

public class MetricConstants {

    private MetricConstants() {
        // nothing to do
    }

    public static final String GROUP_BASE = "dcs.activemq";

    public static final String TAG_BROKER = "broker";
    public static final String TAG_TOPC = "topic";
    public static final String TAG_DESTINATION_TYPE = "destinationType";

    public static final String UNIT_NO = null;
    public static final String UNIT_PERCENT = "percent";
    public static final String UNIT_MS = "ms";

}
