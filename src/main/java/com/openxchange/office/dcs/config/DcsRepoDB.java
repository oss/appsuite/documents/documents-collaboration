/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.config;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import com.google.common.base.Throwables;

@Component
@Profile("db")
public class DcsRepoDB extends DcsRepoBase implements InitializingBean {

    private static final Logger log = LoggerFactory.getLogger(DcsRepoDB.class);

    private static final String INSERT_STMT = "INSERT INTO DCS (ID, Host, Interface, JMSPort, UseSSL, TimestampUTC) VALUES (?, ?, ?, ?, ?, ?)";
    private static final String UPDATE_STMT = "UPDATE DCS SET Host = ?, Interface = ?, JMSPort = ?, UseSSL = ?, TimestampUTC = ? WHERE ID = ?";
    private static final String UPDATE_TIMESTAMP_STMT = "UPDATE DCS SET TimestampUTC = ? WHERE ID = ?";
    private static final String DELETE_STMT = "DELETE FROM DCS WHERE ID = ?";
    private static final String SELECT_ALL_STMT = "SELECT ID, Host, Interface, JMSPort, UseSSL FROM DCS";
    private static final String SELECT_ALL_TIMESTAMPS_STMT = "SELECT ID, TimestampUTC FROM DCS";
    private static final String SELECT_BY_ID = "SELECT ID, Host, Interface, JMSPort, UseSSL FROM DCS WHERE ID = ?";
    private static final String SELECT_BY_HOST_AND_PORT = "SELECT ID, Host, Interface, JMSPort, UseSSL FROM DCS where Host = ? and JMSPort = ?";
    private static final String SELECT_BY_INTERFACE_AND_PORT = "SELECT ID, Host, Interface, JMSPort, UseSSL FROM DCS where Interface = ? and JMSPort = ?";
    private static final int MAX_RETRY_COUNT = 3;

    private static final ZoneId ZONEID_UTC = ZoneId.of("UTC");

    private final JdbcTemplate jdbcTemplate;

    private final Map<String, DBTimestampEntry> dbTimestampEntryMap = new HashMap<>();

    private final Random random = new Random();

    /**
     * {@link DBTimestampEntry}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.10.6
     */
    private static class DBTimestampEntry {

        private Timestamp dbTimestamp;
        private long updateTimeMillis;

        /**
         * Initializes a new {@link DBTimestampEntry}.
         * @param dbTimestamp
         */
        DBTimestampEntry(final Timestamp dbTimestamp) {
            super();
            implUpdateTimestamp(dbTimestamp);
        }

        void update(final Timestamp dbTimestamp) {
            if (dbTimestamp.after(this.dbTimestamp)) {
                implUpdateTimestamp(dbTimestamp);
            }
        }

        long getUpdateTimeMillis() {
            return this.updateTimeMillis;
        }

        private void implUpdateTimestamp(final Timestamp dbTimestamp) {
            this.dbTimestamp = dbTimestamp;
            this.updateTimeMillis = System.currentTimeMillis();

        }
    }

    @Value("${spring.datasource.url}")
    private String dataSourceUrl;

    @Autowired
    public DcsRepoDB(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void afterPropertiesSet() {
        log.info("Server is using the database connection {} for service discovery", removeQueryPart(dataSourceUrl));
    }

    @Override
    public Set<String> keysMonitored() {
        Set<String> keys = new HashSet<>();
        synchronized (dbTimestampEntryMap) {
            keys.addAll(dbTimestampEntryMap.keySet());
        }
        return keys;
    }

    @Override
    public void register(DcsEntity dcsEntity) throws DuplicateKeyException {
        final String key = dcsEntity.getKey();
        final String host = dcsEntity.getHost();
        final String interfce = dcsEntity.getInterfce();
        final int jmsPort = dcsEntity.getJmsPort();
        final boolean useSSL = dcsEntity.isUseSSL();
        final DcsEntity dbDCSEntity = get(key);
        final Timestamp timestampUTC = implGetCurrentTimestampUTC();

        if (null == dbDCSEntity) {
            log.info("Registering server with key {} in database", key);

            jdbcTemplate.update(INSERT_STMT, ps -> {
                ps.setString(1, key);
                ps.setString(2, host);
                ps.setString(3, interfce);
                ps.setInt(4, jmsPort);
                ps.setBoolean(5, useSSL);
                ps.setTimestamp(6, timestampUTC);
            });
        } else if (!host.equals(dbDCSEntity.getHost()) ||
                   !interfce.equals(dbDCSEntity.getInterfce()) ||
                   (jmsPort != dbDCSEntity.getJmsPort()) ||
                   (useSSL != dbDCSEntity.isUseSSL())) {

            log.info("Updating server with key {} in database", key);

            jdbcTemplate.update(UPDATE_STMT, ps -> {
                ps.setString(1, host);
                ps.setString(2, interfce);
                ps.setInt(3, jmsPort);
                ps.setBoolean(4, useSSL);
                ps.setTimestamp(5, timestampUTC);
                ps.setString(6, key);
            });
        } else {
            log.info("Server with key {} already registered in database", key);

            implUpdateTimestamp(key, timestampUTC);
        }
    }

    @Override
    public void updateAndValidateTimestamps(String key, long maxTimestampAgeMillis) {
        if (null != key) {
           implUpdateTimestamp(key, implGetCurrentTimestampUTC());
        }

        // get timestamp map of all currently existing DB entries
        final Map<String, Timestamp> dbTimestampMap = new HashMap<>();

        jdbcTemplate.query(SELECT_ALL_TIMESTAMPS_STMT, (rs) -> {
            dbTimestampMap.put(rs.getString(1), rs.getTimestamp(2));
        });

        // Keep local map in sync with DB and validate all entries timestamps
        synchronized (this.dbTimestampEntryMap) {
            // remove entries from local map first, if they are not contained in DB anymore
            dbTimestampEntryMap.keySet().retainAll(dbTimestampMap.keySet());

            // update/create timestamps of local map entries with DB timestamps and validate each
            final long curTimeMillis = System.currentTimeMillis();

            dbTimestampMap.keySet().stream().forEach((curKey) -> {
                final Timestamp dbTimestamp = dbTimestampMap.get(curKey);
                final DBTimestampEntry dbTimestampEntry = this.dbTimestampEntryMap.get(curKey);

                if (null != dbTimestampEntry) {
                    // update timestamp of existing entry
                    dbTimestampEntry.update(dbTimestamp);

                    // remove entry from local map and DB if timeout has been reached
                    if (((curTimeMillis - dbTimestampEntry.getUpdateTimeMillis()) > maxTimestampAgeMillis) && (delete(curKey) == 1)) {
                        this.dbTimestampEntryMap.remove(curKey);
                        log.warn("Key validation detected not updated timestamp for key {} => removed row", curKey);
                    }
                } else {
                    // create new timestamp entry
                    this.dbTimestampEntryMap.put(curKey, new DBTimestampEntry(dbTimestamp));
                }
            });
        }
    }


    @Override
    public int delete(String key) {
        log.debug("Calling delete for key {}", key);
        final int deleted = jdbcTemplate.update(DELETE_STMT, ps -> {
            ps.setString(1, key);
        });
        log.debug("Called delete for key {}, delete {} rows.", key, deleted);

        return deleted;
    }

    @Override
    public Collection<DcsEntity> getAll() {
        Collection<DcsEntity> res = jdbcTemplate.query(SELECT_ALL_STMT, (rs, rowNum) -> {
            return new DcsEntity(rs.getString(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getBoolean(5));
        });
        return res;
    }

    @Override
    public void deleteAll() {
        jdbcTemplate.execute("delete from DCS");
    }

    private DcsEntity get(String key) {
        Object [] args = {key};
        DcsEntity res = null;
        try {
            res = jdbcTemplate.queryForObject(SELECT_BY_ID, args, (rs, rowNum) -> implCreateDCSEntity(rs, rowNum));
        } catch (EmptyResultDataAccessException ex) {}
        return res;
    }

    @Override
    public DcsEntity get(String hostOrInterface, int port) {
        Object [] args = {hostOrInterface, port};
        DcsEntity res = null;
        try {
            res = jdbcTemplate.queryForObject(SELECT_BY_HOST_AND_PORT, args, (rs, rowNum) -> implCreateDCSEntity(rs, rowNum));
        } catch (EmptyResultDataAccessException ex) {}
        if (res == null) {
            try {
                res = jdbcTemplate.queryForObject(SELECT_BY_INTERFACE_AND_PORT, args, (rs, rowNum) -> implCreateDCSEntity(rs, rowNum));
            } catch (EmptyResultDataAccessException ex) {}
        }
        if ((res == null) && (hostOrInterface.equals("127.0.0.1"))) {
            Object [] args2 = {"localhost", port};
            try {
                res = jdbcTemplate.queryForObject(SELECT_BY_INTERFACE_AND_PORT, args2, (rs, rowNum) -> implCreateDCSEntity(rs, rowNum));
            } catch (EmptyResultDataAccessException ex) {}
        }
        return res;
    }

    private DcsEntity implCreateDCSEntity(final ResultSet rs, final int nRow) {
        try {
            return new DcsEntity(rs.getString(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getBoolean(5));
        } catch (SQLException e) {
            log.error(Throwables.getRootCause(e).getMessage());
        }

        return null;
    }

    private String removeQueryPart(String dsUrl) {
        String res = "<unknown>";
        if (StringUtils.isNotEmpty(dsUrl)) {
            int queryIdx = dsUrl.indexOf('?');
            res = (queryIdx >= 0) ? dsUrl.substring(0, queryIdx) : dsUrl;
        }
        return res;
    }


    private Timestamp implGetCurrentTimestampUTC() {
        return Timestamp.valueOf(LocalDateTime.now(ZONEID_UTC));
    }

    private void implUpdateTimestamp(final String key, final Timestamp timestampUTC) {
        // update timestamp for key
        log.debug("Updating server timestamp with key {} in database, UTC timestamp: {}", key, timestampUTC.toString());

        int retryCount = 0;
        boolean success = false;
        DataAccessException lastEx = null;
        while (!success && (retryCount <= MAX_RETRY_COUNT)) {
            try {
                jdbcTemplate.update(UPDATE_TIMESTAMP_STMT, ps -> {
                    ps.setTimestamp(1, timestampUTC);
                    ps.setString(2, key);
                });
                success = true;
                lastEx = null;
            } catch (DataAccessException e) {
                lastEx = e;

                try {
                    long delayUntilNextTry = 500 + getRandomTimeSpanInMS(1000);
                    log.debug("BackOff for {} ms for next try", delayUntilNextTry);
                    Thread.sleep(delayUntilNextTry);
                } catch (InterruptedException ie) {
                    Thread.currentThread().interrupt();
                }
            }

            if (!success) {
                ++retryCount;
                log.debug("Retrying: Updating server timestamp with key {} in database, UTC timestamp: {}", key, timestampUTC.toString(), retryCount);
            }
        }

        if (lastEx != null) {
            log.error("Exception caught trying to update time stamp for DCS entry, even after " + MAX_RETRY_COUNT + " retries.", lastEx);
        }

    }

    private int getRandomTimeSpanInMS(int span) {
        if (span <= 0) throw new IllegalArgumentException("span must be positive");

        return random.nextInt(span + 1);
    }
}
