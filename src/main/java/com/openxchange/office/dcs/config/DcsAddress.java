/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.config;

import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import org.apache.activemq.network.NetworkBridge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

public class DcsAddress {
    private static final Logger log = LoggerFactory.getLogger(DcsAddress.class);

    private final String scheme;
    private final String address;
    private final int jmsPort;

    /**
     * For testing purposes only. Don't use it in the normal
     * DCS code.
     *
     * @param scheme
     * @param host
     * @param port
     */
    public DcsAddress(String scheme, String address, int port) {
        this.scheme = scheme;
        this.address = address;
        this.jmsPort = port;
    }

    public DcsAddress(DcsEntity dcsEntity) throws UnknownHostException {
        final URI uri = dcsEntity.toUri();

        InetAddress remoteAddress = null;
        try {
            remoteAddress = InetAddress.getByName(uri.getHost());
        } catch (Exception e) {
            log.warn("Exception caught trying to determine address of DCSEntity!", e);
            throw e;
        }

        this.scheme = uri.getScheme();
        this.address = remoteAddress.getHostAddress();
        this.jmsPort = uri.getPort();
    }

    public DcsAddress(NetworkBridge nb) throws URISyntaxException, UnknownHostException {
        String brokerRemoteAddress = nb.getRemoteAddress();

        URI remoteUri = null;
        InetAddress remoteAddress = null;
        try {
            remoteUri = new URI(brokerRemoteAddress);
            remoteAddress = InetAddress.getByName(remoteUri.getHost());
        } catch (Exception e) {
            log.warn("Exception caught trying to determine remote broker address!", e);
            throw e;
        }

        this.scheme = remoteUri.getScheme();
        this.address = remoteAddress.getHostAddress();
        this.jmsPort = remoteUri.getPort();
    }

    public String getScheme() {
        return this.scheme;
    }

    public String getAddress() {
        return address;
    }

    public int getJmsPort() {
        return jmsPort;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + jmsPort;
		return result;
	}

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DcsAddress other = (DcsAddress) obj;
        if (address == null) {
            if (other.address != null)
                return false;
        } else if (!address.equals(other.address))
            return false;
        return jmsPort == other.jmsPort;
    }

    public URI toUri() {
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme(scheme).host(address).port(jmsPort).build();
        return uriComponents.toUri();
    }

    @Override
    public String toString() {
        return new StringBuilder(address).append(":").append(jmsPort).toString();
    }

    static public DcsAddress createDcsAddressFromUriString(String uriString) throws URISyntaxException, UnknownHostException {
        final URI uri = new URI(uriString);

        InetAddress remoteAddress = null;
        try {
            remoteAddress = InetAddress.getByName(uri.getHost());
        } catch (Exception e) {
            log.warn("Exception caught trying to determine address of provided uri string!", e);
            throw e;
        }

        return new DcsAddress(uri.getScheme(), remoteAddress.getHostAddress(), uri.getPort());
    }

}
