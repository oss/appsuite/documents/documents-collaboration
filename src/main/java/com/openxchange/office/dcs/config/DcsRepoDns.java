/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.config;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;

@Component
@Profile("dns")
public class DcsRepoDns extends DcsRepoBase implements InitializingBean {

	private static final Logger log = LoggerFactory.getLogger(DcsRepoDns.class);

	@Value("${dcs.serviceName}")
	private String dcsServiceName;

	@Value("${spring.activemq.transportConnector.uri.protocol}")
	private String activeMqTransConnProt;

	public DcsRepoDns() {
		// nothing to do
	}

	@Override
	public void afterPropertiesSet() {
    	log.info("Server is using DNS for service discovery, serviceName={}", dcsServiceName);
	}

    @Override
    public Set<String> keysMonitored() {
        return new HashSet<>();
    }

	@Override
	public void register(DcsEntity dcsEntity) throws DuplicateKeyException {
		// nothing to do
	}

	@Override
	public int delete(String key) {
		return 0;
	}

	@Override
	public Collection<DcsEntity> getAll() {
		Collection<String> entries = getDnsEntriesForServiceName();
		return entries.stream()
				      .map(e -> new DcsEntity(e))
				      .collect(Collectors.toSet());
	}

	@Override
	public void deleteAll() {
		// nothing to do
	}

	@Override
	public DcsEntity get(String hostOrInterface, int port) {
		if (StringUtils.isEmpty(hostOrInterface))
			throw new IllegalArgumentException("Host of interface must not be empty!");

		if (port < 0)
			port = DcsEntity.DCS_PORT_DEFAULT;
		else if (port != DcsEntity.DCS_PORT_DEFAULT)
			throw new IllegalArgumentException("Only default port " + DcsEntity.DCS_PORT_DEFAULT + " is allowed!");

		Collection<DcsEntity> all = getAll();
		Optional<DcsEntity> entry = all.stream()
				                       .filter(e -> (StringUtils.compare(hostOrInterface, e.getInterfce()) == 0))
				                       .findFirst();
		
		return entry.isPresent() ? entry.get() : null;
	}

	@Override
	public void updateAndValidateTimestamps(String key, long maxTimestampAgeMillis) {
		// nothing to do
	}

	private Collection<String> getDnsEntriesForServiceName() {
		Set<String> result = new HashSet<>();

		try {
			InetAddress[] addresses = InetAddress.getAllByName(dcsServiceName);
			if ((addresses != null) && (addresses.length > 0)) {
				for (InetAddress address : addresses) {
					result.add(createtUrl(address.getHostAddress()));
				}
			}
		} catch (UnknownHostException e) {
			log.info("UnknownHostException caught trying to determine ip addresses for service name " + dcsServiceName, e);
		}

		return result;
	}

	/**
	 * Create a url
	 * @param hostAddress
	 * @return
	 */
	private String createtUrl(String hostAddress) {
		StringBuilder dcsEntryStr = new StringBuilder(isSSLModeActive() ? DcsEntity.SCHEMA_SSL : DcsEntity.SCHEMA_TCP);
		dcsEntryStr.append("://");
		dcsEntryStr.append(hostAddress);
		return dcsEntryStr.toString();
	}

	private boolean isSSLModeActive() {
		return (activeMqTransConnProt.indexOf(DcsEntity.SCHEMA_SSL) == 0);
	}

}
