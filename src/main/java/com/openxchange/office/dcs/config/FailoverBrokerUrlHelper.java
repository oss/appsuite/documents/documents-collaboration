/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.config;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;

public class FailoverBrokerUrlHelper {
    private static final String ACTIVEMQ_FAILOVER_SCHEME = "failover";
    private static final char CONNECT_PART_START = '(';
    private static final char CONNECT_PART_END = ')';
    private static final char QUERY_PART_START = '?';
    private static final char QUERY_EQUAL = '=';
    private static final char QUERY_NEXT_PARAM = '&';

    private static class QueryPartData {
        public String queryPart = "";
        public boolean hasQueryChar = false;

        public QueryPartData(String queryPart, boolean hasQueryChar) {
            this.queryPart = queryPart;
            this.hasQueryChar = hasQueryChar;
        }
    }

    private static class QueryParamData {
        public String value;
        public int index;
        public int length;

        public QueryParamData(String value,  int index, int length) {
            this.value = value;
            this.index = index;
            this.length = length;
        }
    }

    private FailoverBrokerUrlHelper() {
        // nothing to do
    }

    public static String ensureFailoverBrokerUrlContainsQueryParamWithValue(String failoverBrokerUrl, String queryParam, String value) {
        if (StringUtils.isEmpty(queryParam) || StringUtils.isEmpty(value))
            throw new IllegalArgumentException("queryParam and value must not be null or empty");

        String result = failoverBrokerUrl;

        try {
            URI uri = new URI(failoverBrokerUrl);
            if (ACTIVEMQ_FAILOVER_SCHEME.equals(uri.getScheme())) {
                String schemeSpecificPart = uri.getRawSchemeSpecificPart();
                if (StringUtils.isNotEmpty(schemeSpecificPart)) {
                    QueryPartData queryPartData = extractRootQueryPart(schemeSpecificPart);
                    if (queryPartData.queryPart.indexOf(queryParam) == -1) {
                        StringBuilder tmp = new StringBuilder(schemeSpecificPart);
                        if (StringUtils.isNotEmpty(queryPartData.queryPart)) {
                            tmp.append("&").append(queryParam).append("=").append(value);
                        } else {
                            if (!queryPartData.hasQueryChar) {
                                tmp.append("?");
                            }
                            tmp.append(queryParam).append("=").append(value);
                        }
                        schemeSpecificPart = tmp.toString();
                        result = uri.getScheme() + ":" + schemeSpecificPart;
                    }
                }
            } else {
                throw new IllegalArgumentException("Provided URL " + failoverBrokerUrl + " is not a failover URL");
            }
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Exception caught on parsing failover broker URI " + failoverBrokerUrl, e);
        }
        return result;
    }

    public static String ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(String failoverBrokerUrl, String queryParam, String value) {
        if (StringUtils.isEmpty(queryParam) || StringUtils.isEmpty(value))
            throw new IllegalArgumentException("queryParam and value must not be null or empty");

        String result = failoverBrokerUrl;

        try {
            URI failoverBrokerURI = new URI(failoverBrokerUrl);
            if (ACTIVEMQ_FAILOVER_SCHEME.equals(failoverBrokerURI.getScheme())) {
                String schemeSpecificPart = failoverBrokerURI.getRawSchemeSpecificPart();
                QueryPartData rootQueryData = extractRootQueryPart(schemeSpecificPart);
                if (StringUtils.isNotEmpty(schemeSpecificPart)) {
                    List<URI> connectionURIs = extractConnectionURIs(schemeSpecificPart);
                    StringBuilder buildFailoverBrokerUrl = new StringBuilder(ACTIVEMQ_FAILOVER_SCHEME);
                    if (!connectionURIs.isEmpty()) {
                        List<URI> fixedURIs = connectionURIs.stream()
                                                            .map(uri -> ensureConnectionURIContainsQueryParamWithValue(uri, queryParam, value))
                                                            .collect(Collectors.toList());
                        buildFailoverBrokerUrl.append(":").append(CONNECT_PART_START);
                        Iterator<URI> iter = fixedURIs.iterator();
                        while (iter.hasNext()) {
                            buildFailoverBrokerUrl.append(iter.next().toString());
                            if (iter.hasNext()) {
                                buildFailoverBrokerUrl.append(",");
                            }
                        }
                        buildFailoverBrokerUrl.append(CONNECT_PART_END);
                        if (rootQueryData.hasQueryChar) {
                            buildFailoverBrokerUrl.append(QUERY_PART_START).append(rootQueryData.queryPart);
                        }
                        result = buildFailoverBrokerUrl.toString();
                    } else {
                        throw new IllegalArgumentException("Illegal state detected - no connection uris found in failover broker url " + failoverBrokerUrl);
                    }
                }
            } else {
                throw new IllegalArgumentException("Provided URL " + failoverBrokerUrl + " is not a failover URL");
            }
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("URISyntaxException caught on parsing failover broker URL " + failoverBrokerUrl, e);
        }
        return result;
    }

    private static QueryPartData extractRootQueryPart(String failoverSchemeSpecificPart) throws IllegalArgumentException {
        boolean hasQueryChar = false;
        String result = "";

        if (StringUtils.isNotEmpty(failoverSchemeSpecificPart)) {
            int startIndex = failoverSchemeSpecificPart.indexOf(CONNECT_PART_START);
            int endIndex = failoverSchemeSpecificPart.lastIndexOf(CONNECT_PART_END);
            if ((startIndex == 0) && (endIndex > startIndex)) {
                int queryIndex = failoverSchemeSpecificPart.lastIndexOf(QUERY_PART_START);
                if ((queryIndex != -1) && (queryIndex > endIndex) && (queryIndex + 1 < failoverSchemeSpecificPart.length())) {
                    result = failoverSchemeSpecificPart.substring(queryIndex + 1);
                }
                hasQueryChar = ((queryIndex != -1) && (queryIndex > endIndex)) ? true : false;
            } else {
                throw new IllegalArgumentException("Failover scheme specific part not as expected: " + failoverSchemeSpecificPart);
            }
        }

        return new QueryPartData(result, hasQueryChar);
    }

    private static List<URI> extractConnectionURIs(String failoverSchemeSpecificPart) throws IllegalArgumentException {
        List<URI> result = new ArrayList<>();

        if (StringUtils.isNotEmpty(failoverSchemeSpecificPart)) {
            int startIndex = failoverSchemeSpecificPart.indexOf(CONNECT_PART_START);
            int endIndex = failoverSchemeSpecificPart.lastIndexOf(CONNECT_PART_END);
            if ((startIndex == 0) && (endIndex > (startIndex + 1))) {
                String connURISString = failoverSchemeSpecificPart.substring(startIndex + 1, endIndex);
                final String [] connectionUris = connURISString.split(",");
                if ((connectionUris != null) && (connectionUris.length > 0)) {
                    for (int i=0; i < connectionUris.length; i++ ) {
                        try {
                            result.add(new URI(connectionUris[i]));
                        } catch (URISyntaxException e) {
                            throw new IllegalArgumentException("Syntax exception caught for provided failover connection part", e);
                        }
                    }
                }
            } else {
                throw new IllegalArgumentException("Failover scheme specific part not as expected: " + failoverSchemeSpecificPart);
            }
        }

        return result;
    }

    private static URI ensureConnectionURIContainsQueryParamWithValue(URI connURI, String queryParam, String value) throws IllegalArgumentException {
        URI result = connURI;

        String rawQueryPart = ensureNotNull(connURI.getRawQuery());

        QueryParamData queryParamData = determineQueryParamData(rawQueryPart, queryParam, value);
        if (queryParamData == null) {
            // parameter not found
            rawQueryPart += (StringUtils.isEmpty(rawQueryPart)) ? "" : "&";
            rawQueryPart += queryParam + "=" + value;
        } else if ((queryParamData.value != null) && (queryParamData.value.equals(value))) {
            // value exists and has the same value - nothing to do
            return connURI;
        } else {
            // replace query parameter string and value with correct value
            String targetString = rawQueryPart.substring(queryParamData.index, queryParamData.index + queryParamData.length);
            rawQueryPart = rawQueryPart.replace(targetString, queryParam + "=" + value);
        }

        try {
            result = new URI(connURI.getScheme(), connURI.getUserInfo(), connURI.getHost(),
                connURI.getPort(), connURI.getPath(), rawQueryPart, connURI.getFragment()); 
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("URISyntaxException caught trying to create needed connection uri for failover broker URL " + connURI, e);
        }

        return result;
    }

    private static QueryParamData determineQueryParamData(String rawQueryPart, String queryParam, String value) {
        QueryParamData result = null;

        if (!StringUtils.isEmpty(rawQueryPart)) {
            result = determineQueryParamData(rawQueryPart,0, queryParam, value);
        }

        return result;
    }

    private static QueryParamData determineQueryParamData(String rawQueryPart, int index, String queryParam, String value) {
        QueryParamData result = null;

        if (!StringUtils.isEmpty(rawQueryPart)) {
            final int queryParmIndex = rawQueryPart.indexOf(queryParam, index);
            if (queryParmIndex != -1) {
                if (queryParmIndex > 0) {
                    // Check for special case: abc<substring>=abc would
                    // match although this is not the query parameter we
                    // are looking for. Ensure that's not the case.
                    final char prevChar = rawQueryPart.charAt(queryParmIndex-1);
                    if (!((prevChar == QUERY_PART_START) || (prevChar == QUERY_NEXT_PARAM))) {
                        // parameter is not the same -> start search again
                        return determineQueryParamData(rawQueryPart, queryParmIndex + queryParam.length(), queryParam, value);
                    }
                }

                // check for the correct value
                String queryParamRestPart = rawQueryPart.substring(queryParmIndex);
                if (queryParamRestPart.length() > queryParam.length()) {
                    int nextCharIndex = queryParam.length();
                    char nextChar = queryParamRestPart.charAt(nextCharIndex);
                    switch (nextChar) {
                        case QUERY_EQUAL: {
                            final int valueEndIdx = queryParamRestPart.indexOf(QUERY_NEXT_PARAM);
                            if (valueEndIdx == -1) {
                                // no next param
                                if (queryParamRestPart.length() > (nextCharIndex + 1)) {
                                    // we have a value part
                                    String valueInQuery = queryParamRestPart.substring(nextCharIndex + 1);
                                    result = new QueryParamData(valueInQuery, queryParmIndex, queryParam.length() + 1 + valueInQuery.length());
                                } else {
                                    // equals char without value part
                                    result = new QueryParamData(null, queryParmIndex, queryParam.length() + 1);
                                }
                            } else {
                                // there is a next param
                                if (valueEndIdx > (queryParam.length() + 1)) {
                                    // we have a value part
                                    String valueInQuery = queryParamRestPart.substring(queryParam.length() + 1, valueEndIdx);
                                    result = new QueryParamData(valueInQuery, queryParmIndex, queryParam.length() + 1 + valueInQuery.length());
                                } else {
                                    // no value "=&" found
                                    result = new QueryParamData(null, queryParmIndex, queryParam.length() + 1);
                                }
                            }
                            break;
                        }
                        case QUERY_NEXT_PARAM: {
                            // param without value, but with '&';
                            result = new QueryParamData(null, queryParmIndex, queryParam.length());
                            break;
                        }
                        default: {
                            // we found a longer string containing our queryParam - search further
                            result = determineQueryParamData(rawQueryPart, queryParmIndex + queryParam.length(), queryParam, value);
                            break;
                        }
                    }
                } else {
                    // param at the end of the query part - without value
                    result = new QueryParamData(null, queryParmIndex, queryParam.length());
                }
            }
        }

        return result;
    }

    private static String ensureNotNull(String str) {
        return (str == null) ? "" : str;
    }
}
