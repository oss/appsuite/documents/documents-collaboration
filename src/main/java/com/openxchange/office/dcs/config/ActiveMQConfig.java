/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.config;

import java.net.UnknownHostException;
import java.util.Properties;
import javax.sql.DataSource;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.PublishedAddressPolicy;
import org.apache.activemq.jms.pool.PooledConnectionFactory;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.jdbc.DatabaseDriver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.support.DatabaseStartupValidator;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;

import com.openxchange.office.dcs.activemq.AdvisoryMessageConverter;

@EnableJms
@ImportResource(value = "classpath:activemq.xml")
@Configuration
public class ActiveMQConfig {
    
	private static final Logger log = LoggerFactory.getLogger(ActiveMQConfig.class);
	
    @Value("${dcs.db.check.timeout:180}")
    private int dbCheckTimeout;

    @Value("${dcs.db.check.interval:5}")
    private int dbCheckInterval;

	@Value("${dcs.advertiseURL}") 
	private String advertiseUrl;

	@Value("${spring.activemq.broker-url}") 
	private String brokerUrl;

	@Autowired
    private ActiveMQConnectionFactory activeMQConnectionFactory;

	private static boolean logged = false;

    @Bean
    @Profile("db")
    public DatabaseStartupValidator databaseStartupValidator(DataSource dataSource) {
        DatabaseStartupValidator dsv = new OXDatabaseStartupValidator();
        dsv.setTimeout(dbCheckTimeout);
        dsv.setInterval(dbCheckInterval);
        dsv.setDataSource(dataSource);
        dsv.setValidationQuery(DatabaseDriver.MARIADB.getValidationQuery());
        return dsv;
    }

	@Bean
	public PublishedAddressPolicy publishedAddressPolicy() {
		DcsEntity advertiseEntity = new DcsEntity(advertiseUrl);
		DcsEntity brokerEntity = new DcsEntity(brokerUrl);
		if (StringUtils.isNotBlank(advertiseEntity.getHost()) && !advertiseEntity.getHost().equals(brokerEntity.getHost())) {
			return new PublishedAddressPolicy() {
				@Override
				protected String getPublishedHostValue(String uriHostEntry) throws UnknownHostException {
					if (!logged) {
						log.info("Published host ist always {}", advertiseEntity.getHost());
						logged = true;
					}
					return advertiseEntity.getHost();
				}

			};
		}
		if (!logged) {
			log.info("Published host is always {}", brokerEntity.getHost());
			logged = true;
		}
		return new PublishedAddressPolicy();
	}

    @Bean
    public PooledConnectionFactory pooledConnectionFactory() throws Exception {
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(activeMQConnectionFactory);
        return pooledConnectionFactory;
    }

    @Bean
    public JmsListenerContainerFactory<?> topicListenerFactory() throws Exception {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(pooledConnectionFactory());
        factory.setPubSubDomain(true);
        return factory;
    }

    @Bean
    public JmsListenerContainerFactory<?> advisoryListenerFactory() throws Exception {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(pooledConnectionFactory());
        factory.setPubSubDomain(true);
        factory.setMessageConverter(new AdvisoryMessageConverter());
        return factory;
    }

    @Bean
    public JmsTemplate jmsTemplate() throws Exception {
    	return new JmsTemplate(pooledConnectionFactory());
    }
    
    @Bean
    public BuildProperties buildProperties() {
        return new BuildProperties(new Properties());
    }
}
