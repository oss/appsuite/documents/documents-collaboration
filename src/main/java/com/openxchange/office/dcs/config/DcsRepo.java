/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.config;

import java.util.Collection;
import java.util.Set;
import org.springframework.dao.DuplicateKeyException;

public interface DcsRepo {

    public void register(DcsEntity dcsEntity) throws DuplicateKeyException;
    public int delete(String key);
    public Collection<DcsEntity> getAll();
    public void deleteAll();
    public DcsEntity get(String hostOrInterface, int port);
    public void updateAndValidateTimestamps(String key, long maxTimestampAgeMillis);
    public Set<String> keysMonitored();
}
