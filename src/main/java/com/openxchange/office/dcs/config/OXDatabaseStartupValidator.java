package com.openxchange.office.dcs.config;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLNonTransientConnectionException;
import java.sql.SQLTimeoutException;
import java.sql.Statement;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;

import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.jdbc.support.DatabaseStartupValidator;
import org.springframework.jdbc.support.JdbcUtils;

/**
 * Extended database startup validator to detect a permanent error
 * trying to connect to a possible not yet started database
 * (e.g user or password error). In that case the validator will
 * stop trying to connect and throws an exception.
 *
 */
public class OXDatabaseStartupValidator extends DatabaseStartupValidator {

	private DataSource myDataSource;
	private String myValidationQuery;
	private int myInterval = DatabaseStartupValidator.DEFAULT_INTERVAL;
	private int myTimeout = DatabaseStartupValidator.DEFAULT_TIMEOUT;

	/**
	 * Set the DataSource to validate.
	 */
	public void setDataSource(DataSource dataSource) {
		this.myDataSource = dataSource;
	}

	/**
	 * Set the SQL query string to use for validation.
	 */
	public void setValidationQuery(String validationQuery) {
		this.myValidationQuery = validationQuery;
	}

	/**
	 * Set the interval between validation runs (in seconds).
	 * Default is {@value #DEFAULT_INTERVAL}.
	 */
	public void setInterval(int interval) {
		this.myInterval = interval;
	}

	/**
	 * Set the timeout (in seconds) after which a fatal exception
	 * will be thrown. Default is {@value #DEFAULT_TIMEOUT}.
	 */
	public void setTimeout(int timeout) {
		this.myTimeout = timeout;
	}

	/**
	 * Check whether the validation query can be executed on a Connection
	 * from the specified DataSource, with the specified interval between
	 * checks, until the specified timeout.
	 */
	@Override
	public void afterPropertiesSet() {
		if (this.myDataSource == null) {
			throw new IllegalArgumentException("Property 'dataSource' is required");
		}
		if (this.myValidationQuery == null) {
			throw new IllegalArgumentException("Property 'validationQuery' is required");
		}

		try {
			boolean validated = false;
			long beginTime = System.currentTimeMillis();
			long deadLine = beginTime + TimeUnit.SECONDS.toMillis(this.myTimeout);
			SQLException latestEx = null;

			while (!validated && System.currentTimeMillis() < deadLine) {
				Connection con = null;
				Statement stmt = null;
				try {
					con = this.myDataSource.getConnection();
					if (con == null) {
						throw new CannotGetJdbcConnectionException("Failed to execute validation query: " +
								"DataSource returned null from getConnection(): " + this.myDataSource);
					}
					stmt = con.createStatement();
					stmt.execute(this.myValidationQuery);
					validated = true;
				}
				catch (SQLTimeoutException | SQLNonTransientConnectionException ex) {
					latestEx = ex;
					if (logger.isDebugEnabled()) {
						logger.debug("Validation query [" + this.myValidationQuery + "] threw exception", ex);
					}
					if (logger.isInfoEnabled()) {
						float rest = ((float) (deadLine - System.currentTimeMillis())) / 1000;
						if (rest > this.myInterval) {
							logger.info("Database has not started up yet - retrying in " + this.myInterval +
									" seconds (timeout in " + rest + " seconds)");
						}
					}
				}
				catch (SQLException e) {
					if (logger.isDebugEnabled()) {
						logger.debug("Validation query [" + this.myValidationQuery + "] threw exception", e);
					}
					throw new CannotGetJdbcConnectionException("Fatal exception detected and giving up, please " +
				              "check your configuration for: " + this.myDataSource, e);
				}
				finally {
					JdbcUtils.closeStatement(stmt);
					JdbcUtils.closeConnection(con);
				}

				if (!validated) {
					TimeUnit.SECONDS.sleep(this.myInterval);
				}
			}

			if (!validated) {
				throw new CannotGetJdbcConnectionException(
						"Database has not started up within " + this.myTimeout + " seconds", latestEx);
			}

			if (logger.isInfoEnabled()) {
				float duration = ((float) (System.currentTimeMillis() - beginTime)) / 1000;
				logger.info("Database startup detected after " + duration + " seconds");
			}
		}
		catch (InterruptedException ex) {
			// Re-interrupt current thread, to allow other threads to react.
			Thread.currentThread().interrupt();
		}
	}
}
