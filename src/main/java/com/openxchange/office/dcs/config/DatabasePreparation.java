/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLNonTransientConnectionException;
import java.sql.SQLTimeoutException;
import java.sql.Statement;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import liquibase.integration.spring.SpringLiquibase;

@Service
@Configuration
@Profile("db")
public class DatabasePreparation implements BeanPostProcessor {
    private static final Logger log = LoggerFactory.getLogger(DatabasePreparation.class);

    private static final String DCSDB_DBCREATE_OPTIONS = "DEFAULT CHARACTER SET utf8mb4 DEFAULT COLLATE utf8mb4_unicode_ci;";
    public static final String ID_OF_ADD_PRIMARY_KEY = "addPrimKeyToLiquibaseTable";

	private JdbcTemplate jdbcTemplate;

    @Value("${dcs.db.check.timeout:180}")
    private int dbCheckTimeout;

    @Value("${dcs.db.check.interval:10}")
    private int dbCheckInterval;

    @Value("${spring.datasource.prepareUrl}")
    private String prepareDcsDbUrl;

	@Value("${spring.datasource.db-schema}")
	private String dcsDbSchema;

	@Value("${spring.datasource.username}")
	private String dbUser;

	@Value("${spring.datasource.password}")
	private String dbPassword;

    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof SpringLiquibase) {
            String id = null;
            try {
                id = jdbcTemplate.queryForObject("select ID from DATABASECHANGELOG where ORDEREXECUTED = 1", String.class);
            } catch (BadSqlGrammarException | EmptyResultDataAccessException ex) {
                // Table DATABASECHANGELOG does not exist or is empty.
            }
            if ((id == null) || !ID_OF_ADD_PRIMARY_KEY.equals(id)) {
                jdbcTemplate.execute("DROP TABLE IF EXISTS DCS;");
                jdbcTemplate.execute("DROP TABLE IF EXISTS DATABASECHANGELOG;");
                jdbcTemplate.execute("DROP TABLE IF EXISTS DATABASECHANGELOGLOCK;");
            }

            try {
                for (int i = 0; i < 10; ++i) {
                    int count = jdbcTemplate.queryForObject("select count(*) from DATABASECHANGELOGLOCK", Integer.class);
                    if (count > 0) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            // Nothing todo
                        }
                    }
                }
                int count = jdbcTemplate.queryForObject("select count(*) from DATABASECHANGELOGLOCK", Integer.class);
                if (count > 0) {
                    jdbcTemplate.execute("TRUNCATE TABLE DATABASECHANGELOGLOCK;");
                }
            } catch (BadSqlGrammarException | EmptyResultDataAccessException ex) {
                // No entry in table DATABASECHANGELOGLOCK --> continue
            }
        } else if (bean instanceof DataSource) {
            // ensure schema creation before we continue
            checkDbConnectionAndPrepareGrantsAndSchemaCreationUntilTimeout();
        }
        return bean;
    }

    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
    	if (bean instanceof DataSource) {
    		DataSource dataSource = (DataSource) bean;
    		jdbcTemplate = new JdbcTemplate(dataSource);
    	}
    	return bean;
   }

    private void checkDbConnectionAndPrepareGrantsAndSchemaCreationUntilTimeout() {
    	log.debug("checkDbConnectionAndPrepareGrantsAndSchemaCreationUntilTimeout");

        Exception lastException = null;
        long started = System.currentTimeMillis();
        boolean tryAgain = true;
        boolean success = false;

        log.info("Trying to connect to database with db user");
        while (tryAgain && !success) {
        	Statement stm = null;
        	Connection conn = null;

            try {
                conn = DriverManager.getConnection(prepareDcsDbUrl, dbUser, dbPassword);

                // create database - jdbc conn works with auto-commit by default
                log.info("Create 'dcsdb' schema if not exists");
                stm = conn.createStatement();
                stm.executeUpdate("CREATE DATABASE IF NOT EXISTS `" + dcsDbSchema + "` " + DCSDB_DBCREATE_OPTIONS + ";");

                success = true;
            } catch (SQLTimeoutException | SQLNonTransientConnectionException e) {
                lastException = e;
                log.info("Waiting for availability of database, currently no connection to database possible ...");
            } catch (SQLException e) {
                log.info("Waiting for creation of database user to continue ...");
                lastException = e;
            } finally {
            	closeSqlResources(conn, stm);
            }

            if (!success) {
                tryAgain = tryAgainToConnect(started);
            }
        }

        if (!success) {
            throw new RuntimeException(lastException);
        }
    }

    private boolean tryAgainToConnect(long started) {
        boolean tryAgain = true;

        long now = System.currentTimeMillis();
        if ((now - started) > (dbCheckTimeout * 1000)) {
        	log.info("No connection to database possible, giving up. Please check your database server or user/password settings.");
            return false;
        }

        try {
            Thread.sleep(dbCheckInterval * 1000);
        } catch (InterruptedException i) {
            Thread.currentThread().interrupt();
            tryAgain = false;
        }
        return tryAgain;
    }

    private void closeSqlResources(Connection conn, Statement stm) {
    	if (stm != null) {
    		try {
    			stm.close();
    		} catch (SQLException e) {
    			log.error("Exception trying to close statement", e);
    		}
    	}
    	if (conn != null) {
    		try {
    			conn.close();
    		} catch (SQLException e) {
    			log.error("Exception trying to close connection", e);
    		}
    	}
    }

}
