/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openxchange.office.dcs.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.openxchange.office.dcs.activemq.EnhActiveMQConnectionFactory;
import com.openxchange.office.dcs.activemq.EnhActiveMQSslConnectionFactory;

@Configuration
public class ActiveMQConnectionFactoryConfig {

    @Value("${spring.activemq.broker-url}")
    private String brokerUrl;

    @Value("${dcs.ssl.client.truststore.path:#{${dcs.usessl:false} matches 'true' ? '/etc/documents-collaboration/security/dcs-client.ts' : null}}")
    private String clientTruststorePath;

    @Value("${dcs.ssl.client.truststore.password:password}")
    private String clientTrusstorePwd;

    @Value("${dcs.ssl.client.truststore.path:#{${dcs.usessl:false} matches 'true' ? '/etc/documents-collaboration/security/dcs-client.ks' : null}}")
    private String clientKeystorePath;

    @Value("${dcs.ssl.client.keystore.password:password}")
    private String clientKeystorePwd;

    @Value("${dcs.username:}")
    private String dcsUsername;

    @Value("${dcs.password:}")
    private String dcsPassword;

    @Bean
    @ConditionalOnProperty(matchIfMissing = true, prefix = "dcs", name = "usessl", havingValue = "false")
    public ActiveMQConnectionFactory connectionFactory() {
        ActiveMQConnectionFactory res = new EnhActiveMQConnectionFactory();
        if (StringUtils.isNotBlank(dcsUsername)) {
            res.setUserName(dcsUsername);
        }
        if (StringUtils.isNotBlank(dcsPassword)) {
            res.setPassword(dcsPassword);        	
        }
        res.setBrokerURL(brokerUrl);
        return res;
    }

    @Bean
    @ConditionalOnProperty(matchIfMissing = false, prefix = "dcs", name = "usessl", havingValue = "true")
    public ActiveMQConnectionFactory sslConnectionFactory() throws Exception {
        EnhActiveMQSslConnectionFactory res = new EnhActiveMQSslConnectionFactory();
        if (StringUtils.isNotBlank(dcsUsername)) {
            res.setUserName(dcsUsername);
        }
        if (StringUtils.isNotBlank(dcsPassword)) {
            res.setPassword(dcsPassword);        	
        }
        res.setKeyStore(clientKeystorePath);
        res.setKeyStorePassword(clientKeystorePwd);
        res.setTrustStore(clientTruststorePath);
        res.setTrustStorePassword(clientTrusstorePwd);
        res.setBrokerURL(brokerUrl);
        return res;
    }
}
