/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.config;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.yaml.snakeyaml.Yaml;

import com.google.common.net.InetAddresses;

public class ConfigReader {
    private static final Logger log = LoggerFactory.getLogger(ConfigReader.class);

    private static final String LOCALHOST_IPV4 = "127.0.0.1";
    private static final String LOCALHOST_IPV6 = "0:0:0:0:0:0:0:1";
    private static final String LOCALHOST = "localhost";
    // private static final String sslBrokerStoreDefaultDir = "/etc/documents-collaboration/security/";
    private static final String sslBrokerStoreDefaultDir = null;
    private static final String ENV_VAR_REF_PREFIX = "${";
    private static final String ENV_VAR_REF_POSTFIX = "}";
    private static final String ENV_VAR_DEFAULT_VAL_SEP = ":";

    public static final String UNSET = "<unset>";
	public static final String DEFAULT_DCS_HOST="0.0.0.0";

    public static final String SPRING_CONFIG_LOCATION = "--spring.config.location=";

    private Map<String, Object> configCache;

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void init(List<String> args) throws IOException {
        configCache = new HashMap<>();
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        Yaml yaml = new Yaml();
        for (String str : args) {
            if (str.startsWith(SPRING_CONFIG_LOCATION)) {
                String configFileName = str.substring(SPRING_CONFIG_LOCATION.length());
                StringTokenizer tokenizer = new StringTokenizer(configFileName, ",");
                while (tokenizer.hasMoreTokens()) {
                    String token = tokenizer.nextToken();
                    Resource resource = resourceLoader.getResource(token);
                    InputStream is = resource.getInputStream();
                    Map topLevelMap = null;
                    if (token.endsWith(".properties")) {
                        Properties props = new Properties();
                        props.load(is);
                        topLevelMap = createMapHierarchieFromProperties(props);
                    } else {
                        topLevelMap = (Map) yaml.load(is);
                    }
                    if (configCache.isEmpty()) {
                        configCache.putAll(topLevelMap);
                    } else {
                        merge_internal(configCache, topLevelMap);
                    }
                }
            }
        }
    }

    public String getDcsHostname(String defValue) {
        String res = System.getenv("DCS_NAME");
        if (StringUtils.isEmpty(res)) {
            Map<String, Object> dcsSectionInYaml = getConfigSection(configCache, "dcs", null);
            if (StringUtils.isBlank(defValue)) {
                defValue = getActiveMqHost() + "_" + getActiveMqPort();
            }
            res = getConfigEntryWithDefault(dcsSectionInYaml, "name", defValue);
        }
        return res;
    }

    public String getDcsAdvertiseUrl() {
        String res = System.getenv("DCS_ADVERTISEURL");
        if (StringUtils.isEmpty(res)) {
            Map<String, Object> dcsSectionInYaml = getConfigSection(configCache, "dcs", null);
            res = getConfigEntryWithDefault(dcsSectionInYaml, "advertiseURL", null);
            res = resolveEnvVariableRefs(res);
        }
        return res;
    }

    public String getDcsDiscoveryStrategy() {
        String res = System.getenv("DCS_DISCOVERYSTRATEGY");
        if (StringUtils.isEmpty(res)) {
	        Map<String, Object> dcsSectionInYaml = getConfigSection(configCache, "dcs", null);
            res = getConfigEntryWithDefault(dcsSectionInYaml, "discoveryStrategy", Profiles.PROFILE_DB);
            if ("<unset>".equals(res)) {
                return null;
            }
        }
        return res;
    }

    public String getActiveMqHost() {
        String res = System.getenv("DCS_HOST");
        if (StringUtils.isEmpty(res)) {
            Map<String, Object> dcsSectionInYaml = getConfigSection(configCache, "dcs", null);
            res = getConfigEntryWithDefault(dcsSectionInYaml, "host", "localhost");
        }
        return res;
    }

    public String getActiveMqPort() {
        String res = System.getenv("DCS_PORT");
        if (StringUtils.isEmpty(res)) {
            Map<String, Object> dcsSectionInYaml = getConfigSection(configCache, "dcs", null);
            res = getConfigEntryWithDefault(dcsSectionInYaml, "port", "61616");
        }
        return res;
    }

    public boolean isActiveMqUseSSL() {
        String res = System.getenv("DCS_USESSL");
        if (StringUtils.isEmpty(res)) {
            Map<String, Object> dcsSectionInYaml = getConfigSection(configCache, "dcs", null);
            res = getConfigEntryWithDefault(dcsSectionInYaml, "usessl", "false");
        }
        return "true".equalsIgnoreCase(res);
    }

    public String getActiveMqUsername() {
        String res = System.getenv("DCS_USERNAME");
        if (StringUtils.isEmpty(res)) {
            Map<String, Object> dcsSectionInYaml = getConfigSection(configCache, "dcs", null);
            res = getConfigEntryWithDefault(dcsSectionInYaml, "username", "");
        }
        return res;
    }

    /**
     * REMARK: Currently our code does not use host names to store/provide DCS instances,
     * therefore verifyHostName cannot be set to true as otherwise we would get an certificate
     * error.
     * TODO: must be changed if we decide to support host names for DCS instances.
     *
     * @return FALSE
     */
    public boolean isVerifyHostname() {
        String res = System.getenv("DCS_SSL_VERIFYHOSTNAME");
        if (StringUtils.isEmpty(res)) {
            Map<String, Object> dcsSSLSectionInYaml = getDCSSSLConfigSection();
            res = getConfigEntryWithDefault(dcsSSLSectionInYaml, "verifyHostname", "false");
        }
        return false;
        //return "true".equalsIgnoreCase(res);
    }

    public String getBrokerKeystorePath() {
        String res = System.getenv("DCS_SSL_SERVER_KEYSTORE_PATH");
        if (StringUtils.isEmpty(res)) {
            Map<String, Object> dcsSSLServerSectionInYaml = getDCSSSLServerConfigSection();
            res = getConfigEntryWithDefault(getConfigSection(dcsSSLServerSectionInYaml, "keystore", null), "path",
                (null != sslBrokerStoreDefaultDir) ? (sslBrokerStoreDefaultDir + "dcs-broker.ks") : null);
        }
        return res;
    }

    public String getBrokerKeystorePassword() {
        String res = System.getenv("DCS_SSL_SERVER_KEYSTORE_PASSWORD");
        if (StringUtils.isEmpty(res)) {
            Map<String, Object> dcsSSLServerSectionInYaml = getDCSSSLServerConfigSection();
            res = getConfigEntryWithDefault(getConfigSection(dcsSSLServerSectionInYaml, "keystore", null), "password", "password");
        }
        return res;
    }

    public String getBrokerTruststorePath() {
        String res = System.getenv("DCS_SSL_SERVER_TRUSTSTORE_PATH");
        if (StringUtils.isEmpty(res)) {
            Map<String, Object> dcsSSLServerSectionInYaml = getDCSSSLServerConfigSection();
            res = getConfigEntryWithDefault(getConfigSection(dcsSSLServerSectionInYaml, "truststore", null), "path",
                (null != sslBrokerStoreDefaultDir) ? (sslBrokerStoreDefaultDir + "dcs-broker.ts") : null);
        }
        return res;
    }

    public String getBrokerTruststorePassword() {
        String res = System.getenv("DCS_SSL_SERVER_TRUSTSTORE_PASSWORD");
        if (StringUtils.isEmpty(res)) {
            Map<String, Object> dcsSSLServerSectionInYaml = getDCSSSLServerConfigSection();
            res = getConfigEntryWithDefault(getConfigSection(dcsSSLServerSectionInYaml, "truststore", null), "password", "password");
        }
        return res;
    }

    public String getJmxUser() {
        String res = System.getenv("JMX_USERNAME");
        if (StringUtils.isEmpty(res)) {
            Map<String, Object> dcsSectionInYaml = getConfigSection(configCache, "jmx", null);
            res = getConfigEntryWithDefault(dcsSectionInYaml, "username", "");
        }
        return res;
    }

    public String getJmxPwd() {
        String res = System.getenv("JMX_PASSWORD");
        if (StringUtils.isEmpty(res)) {
            Map<String, Object> dcsSectionInYaml = getConfigSection(configCache, "jmx", null);
            res = getConfigEntryWithDefault(dcsSectionInYaml, "password", "");
        }
        return res;
    }

    public int getJmxPort() {
        String res = System.getenv("JMX_PORT");
        if (StringUtils.isEmpty(res)) {
            Map<String, Object> dcsSectionInYaml = getConfigSection(configCache, "jmx", null);
            res = getConfigEntryWithDefault(dcsSectionInYaml, "port", "9994");
        }
        return Integer.parseInt(res);
    }

    public int getJmxServerPort() {
        String res = System.getenv("JMX_SERVERPORT");
        if (StringUtils.isEmpty(res)) {
            Map<String, Object> dcsSectionInYaml = getConfigSection(configCache, "jmx", null);
            res = getConfigEntryWithDefault(dcsSectionInYaml, "serverport", "0");
        }
        return Integer.parseInt(res);
    }

    public String getJmxHost() {
        String res = System.getenv("JMX_HOST");
        if (StringUtils.isEmpty(res)) {
            Map<String, Object> dcsSectionInYaml = getConfigSection(configCache, "jmx", null);
            res = getConfigEntryWithDefault(dcsSectionInYaml, "host", determineHostAddress(true));
        }
        return res;
    }

    public String getJmxRmiServerHostName() {
        String res = System.getenv("JMX_HOSTNAME");
        if (StringUtils.isEmpty(res)) {
            Map<String, Object> dcsSectionInYaml = getConfigSection(configCache, "jmx", null);
            res = getConfigEntryWithDefault(dcsSectionInYaml, "hostname", "");
        }
        return res;
    }

    public boolean getJmxEnabled() {
        String res = System.getenv("JMX_ENABLED");
        if (StringUtils.isEmpty(res)) {
            Map<String, Object> dcsSectionInYaml = getConfigSection(configCache, "jmx", null);
            res = getConfigEntryWithDefault(dcsSectionInYaml, "enabled", "false");
        }
        return "true".equalsIgnoreCase(res);
    }

    public String getSpringSecurityUserName() {
        String res = System.getenv("SPRING_SECURITY_USER_PASSWORD");
        if (StringUtils.isEmpty(res)) {
	        Map<String, Object> springSectionInYaml = getConfigSection(configCache, "spring", null);
	        if (springSectionInYaml == null) {
	            return null;
	        }
	        Map<String, Object> securitySectionInYaml = getConfigSection(springSectionInYaml, "security", null);
	        if (securitySectionInYaml == null) {
	            return null;
	        }
	        Map<String, Object> userSectionInYaml = getConfigSection(securitySectionInYaml, "user", null);
	        if (userSectionInYaml == null) {
	            return null;
	        }

            res = getConfigEntryWithDefault(userSectionInYaml, "name", "<unset>");
            if ("<unset>".equals(res)) {
                return null;
            }
        }
        return res;
    }

    public String getSpringSecurityUserPassword() {
        String res = System.getenv("SPRING_SECURITY_USER_PASSWORD");
        if (StringUtils.isEmpty(res)) {
	        Map<String, Object> springSectionInYaml = getConfigSection(configCache, "spring", null);
	        if (springSectionInYaml == null) {
	            return null;
	        }
	        Map<String, Object> securitySectionInYaml = getConfigSection(springSectionInYaml, "security", null);
	        if (securitySectionInYaml == null) {
	            return null;
	        }
	        Map<String, Object> userSectionInYaml = getConfigSection(securitySectionInYaml, "user", null);
	        if (userSectionInYaml == null) {
	            return null;
	        }

            res = getConfigEntryWithDefault(userSectionInYaml, "password", "<unset>");
            if ("<unset>".equals(res)) {
                return null;
            }
        }
        return res;
    }

    private String determineHostAddress(boolean first) {
        try {
            Enumeration<NetworkInterface> e = NetworkInterface.getNetworkInterfaces();
            while (e.hasMoreElements()) {
                NetworkInterface n = e.nextElement();
                if (n.isUp()) {
                    Enumeration<InetAddress> ee = n.getInetAddresses();
                    while (ee.hasMoreElements()) {
                        InetAddress i = ee.nextElement();
                        String hostAddress = i.getHostAddress();
                        try {
                            String localHostAddress = "";
                            switch (InetAddresses.forString(hostAddress).getAddress().length) {
                                case 4:
                                    localHostAddress = LOCALHOST_IPV4;
                                    break;
                                case 16:
                                    localHostAddress = LOCALHOST_IPV6;
                                    break;
                                default:
                                    throw new IllegalArgumentException("Incorrect ip address " + hostAddress + " length " + InetAddresses.forString(hostAddress).getAddress().length);
                            }
                            if (!localHostAddress.equals(hostAddress)) {
                                if (first) {
                                    if (!i.getHostAddress().equalsIgnoreCase(i.getHostName())) {
                                        return i.getHostName();
                                    }
                                } else {
                                    return i.getHostAddress();
                                }
                            }
                        } catch (IllegalArgumentException ex) {
                            // SKIP
                        }
                    }
                }
            }
        } catch (SocketException ex) {
            return LOCALHOST;
        }
        if (first) {
            return determineHostAddress(false);
        }
        return LOCALHOST;
    }

    private Map<String, Object> getConfigSection(Map<String, Object> baseNode, String key, String errMsg) {
        @SuppressWarnings({ "unchecked", "rawtypes" })
        Map<String, Object> section = (Map) baseNode.get(key);
        if (section == null) {
            if (errMsg == null) {
                return null;
            }
            throw new InvalidConfigurationException(errMsg);
        }
        return section;
    }

    @SuppressWarnings("unused")
    private String getConfigEntry(Map<String, Object> baseNode, String key, String errMsg) {
        if (baseNode == null) {
            throw new InvalidConfigurationException(errMsg);
        }
        Object res = baseNode.get(key);
        if (res == null) {
            if (errMsg == null) {
                return null;
            }
            throw new InvalidConfigurationException(errMsg);
        }
        return res.toString();
    }

    private String getConfigEntryWithDefault(Map<String, Object> baseNode, String key, String defaultValue) {
        if (baseNode == null) {
            return defaultValue;
        }
        Object res = baseNode.get(key);
        if ((res == null) || (StringUtils.isBlank(res.toString()))){
            return defaultValue;
        }
        return res.toString();
    }

    @SuppressWarnings("unchecked")
    private Map<String, Object> createMapHierarchieFromProperties(Properties properties) {
        final Map<String, Object> root = new HashMap<>();

        for (String propName : properties.stringPropertyNames()) {
            final String[] path = propName.split("\\.");

            Map<String,Object> parent = root;
            final int n = path.length - 1;

            for (int i = 0; i < n; ++i) {
                final String pathPart = path[i];
                final Object child = parent.get(pathPart);

                if (null == child) {
                    Map<String,Object> map = new HashMap<>();
                    parent.put(pathPart, map);
                    parent = map;
                } else if (child instanceof Map) {
                    parent = (Map<String,Object>)child;
                } else {
                    throw new InvalidConfigurationException("Property entry cannot be map and value at same time!");
                }
            }

            parent.put(path[n], properties.getProperty(propName));
        }

        return root;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void merge_internal(Map<String, Object> mergedResult, Map<String, Object> yamlContents) {

        if (null == yamlContents) {
            return;
        }

        for (String key : yamlContents.keySet()) {

            Object yamlValue = yamlContents.get(key);
            if (yamlValue == null) {
                addToMergedResult(mergedResult, key, yamlValue);
                continue;
            }

            Object existingValue = mergedResult.get(key);
            if (existingValue != null) {
                if (yamlValue instanceof Map) {
                    if (existingValue instanceof Map) {
                        merge_internal((Map<String, Object>) existingValue, (Map<String, Object>) yamlValue);
                        continue;
                    }
                    if (existingValue instanceof String) {
                        throw new IllegalArgumentException("Cannot merge complex element into a simple element: " + key);
                    }
                    throw new ConfigMergeException(key, yamlValue);
                }
                if (yamlValue instanceof List) {
                    mergeLists(mergedResult, key, yamlValue);
                    continue;
                }
                if ((yamlValue instanceof String) || (yamlValue instanceof Boolean) || (yamlValue instanceof Double) || (yamlValue instanceof Integer)) {
                    if (!key.equals("password") && !key.equals("pwd")) {
                        log.info("overriding value of "+key+" with value "+yamlValue);
                    }
                    addToMergedResult(mergedResult, key, yamlValue);
                    continue;

                }
                throw new ConfigMergeException(key, yamlValue);
            } else {
                if ((yamlValue instanceof Map) || (yamlValue instanceof List) || (yamlValue instanceof String) ||
                    (yamlValue instanceof Boolean) || (yamlValue instanceof Integer) || (yamlValue instanceof Double)) {
                	if (yamlValue instanceof Map) {
                		Map<?, ?> m = (Map) yamlValue;
                        if (!m.containsKey("pwd") && !m.containsKey("password")) {
                        	log.info("adding new key '" + key + "' with value '" + yamlValue + "'");
                        }
                    }
                    addToMergedResult(mergedResult, key, yamlValue);
                }
            }
        }
    }

    private Object addToMergedResult(Map<String, Object> mergedResult, String key, Object yamlValue) {
        return mergedResult.put(key, yamlValue);
    }

    @SuppressWarnings("unchecked")
    private void mergeLists(Map<String, Object> mergedResult, String key, Object yamlValue) {
        if (! ((yamlValue instanceof List) && (mergedResult.get(key) instanceof List))) {
            throw new IllegalArgumentException("Cannot merge a list with a non-list: "+key);
        }

        List<Object> originalList = (List<Object>) mergedResult.get(key);
        originalList.addAll((List<Object>) yamlValue);
    }

    private Map<String, Object> getDCSSSLConfigSection() {
        return getConfigSection(getConfigSection(configCache, "dcs", null), "ssl", null);
    }

    private Map<String, Object> getDCSSSLServerConfigSection() {
        return getConfigSection(getConfigSection(getConfigSection(configCache, "dcs", null), "ssl", null), "server", null);
    }

    private String resolveEnvVariableRefs(String configValue) {
        int prefixIdx = configValue.indexOf(ENV_VAR_REF_PREFIX);
        int postfixIdx = configValue.indexOf(ENV_VAR_REF_POSTFIX);
        if ((prefixIdx >= 0) && (postfixIdx > prefixIdx)) {
            String envVarStr = configValue.substring(prefixIdx+ENV_VAR_REF_PREFIX.length(), postfixIdx);

            String defaultValue = null;
            int defValSepIdx = envVarStr.indexOf(ENV_VAR_DEFAULT_VAL_SEP);
            if ((defValSepIdx > 0) && (defValSepIdx < envVarStr.length())) {
                defaultValue = envVarStr.substring(defValSepIdx + 1);
                envVarStr = envVarStr.substring(0, defValSepIdx);
            }
            configValue = System.getenv(envVarStr);
            configValue = (configValue == null) ? defaultValue : configValue;
        }
        return configValue;
    }
}
