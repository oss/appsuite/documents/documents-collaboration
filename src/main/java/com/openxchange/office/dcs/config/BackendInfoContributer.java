/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.config;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.jms.BytesMessage;
import javax.jms.Message;

import org.apache.activemq.command.ActiveMQTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.info.Info.Builder;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.core.io.ClassRelativeResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.jms.JmsException;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.VersionMessageRequest;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.VersionMessageResponse;
import com.openxchange.office.dcs.service.BrokerAccessorService;

@Component
@ManagedResource
public class BackendInfoContributer implements InfoContributor {

	private static final Logger log = LoggerFactory.getLogger(BackendInfoContributer.class);

	public static final String ADMIN_TOPIC_STR = "admin-Topic";
	public static final ActiveMQTopic ADMIN_TOPIC = new ActiveMQTopic(ADMIN_TOPIC_STR);

	public static final String HEADER_VERSION_MSG_RCV  = "internal.version.rcv";
	public static final String HEADER_VERSION_MSG_SND  = "internal.version.snd";

	private final JmsTemplate jmsTemplate;

	private final BrokerAccessorService brokerAccessorService;

	private final int port;

	private Properties versionProperties = new Properties();

	private List<String> tmpFinalInfo = null;
	private List<String> finalInfo = null;

	private Integer countBrokers = null;

	private int currentCountBrokers = 0;

	private Integer countBackends = null;

	private int currentCountBackends = 0;

	private Object syncObj = new Object();

	private UUID selfId = UUID.randomUUID();

	@Autowired
	public BackendInfoContributer(BrokerAccessorService brokerAccessorService, JmsTemplate jmsTemplate,
			                      @Value("${dcs.port:61616}") int port) {
		this.brokerAccessorService = brokerAccessorService;
		this.jmsTemplate = jmsTemplate;
		this.port = port;
	}

	@PostConstruct
	public void readVersion() {
		ClassRelativeResourceLoader resourceLoader = new ClassRelativeResourceLoader(BackendInfoContributer.class);
		Resource resource = resourceLoader.getResource("classpath:version.properties");
		if (resource != null) {
			try {
				versionProperties.load(resource.getInputStream());
			} catch (FileNotFoundException ex) {
				log.info("No version information available because of missing version.properties in classpath");
				versionProperties = null;
			} catch (IOException ex) {
				log.warn(ex.getMessage(), ex);
				versionProperties = null;
			}
		} else {
			versionProperties = null;
		}
	}

	@Override
	public void contribute(Builder builder) {
		synchronized (syncObj) {
			finalInfo = null;
			tmpFinalInfo = new ArrayList<>();
			countBrokers = 1;
			if (brokerAccessorService.getBrokerService().getNetworkConnectors().size() >= 1) {
				countBrokers += brokerAccessorService.getBrokerService().getNetworkConnectors().get(0).activeBridges().size();
			}
			currentCountBrokers = 0;
			countBackends = null;
			currentCountBackends = 0;

	    	ByteArrayOutputStream baOut = new ByteArrayOutputStream();
	        try {
	        	VersionMessageRequest versMsg = VersionMessageRequest.newBuilder()
	        			.setOriginator(selfId.toString())
	        			.build();
	        	versMsg.writeTo(baOut);
	        	jmsTemplate.convertAndSend(ADMIN_TOPIC, baOut.toByteArray(), (msg) -> {
	        		msg.setBooleanProperty(HEADER_VERSION_MSG_SND, true);
	        		msg.setBooleanProperty(HEADER_VERSION_MSG_RCV, false);
	        		return msg;
	        	});
	        } catch (JmsException | IOException ex) {
	        	log.error("sending VersionMessageRequest", ex);
	        }
	        int count = 0;
	        while ((finalInfo == null) &&(count <= 50)) {
	        	++count;
	        	try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					count = Integer.MAX_VALUE;
				}
	        }
	        if (finalInfo != null) {
	        	builder.withDetail("memberinfo", finalInfo);
	        } else {
	        	builder.withDetail("partial memberinfo", tmpFinalInfo);
	        }
		}
	}

	@JmsListener(containerFactory="topicListenerFactory", destination=ADMIN_TOPIC_STR)
	public void receiveQueueWithNoConsumerEvent(Message msg) throws Exception {
		if (msg.getBooleanProperty(HEADER_VERSION_MSG_SND)) {
    		BytesMessage byteMsg = (BytesMessage) msg;
    		byte [] data = new byte[(int)byteMsg.getBodyLength()];
    		byteMsg.readBytes(data);
    		String originator = VersionMessageRequest.parseFrom(data).getOriginator();
    		String version = "Unknown";
    		if (versionProperties != null) {
    			version = versionProperties.getProperty("Version") + "(" + versionProperties.get("Buildtime") + ")";
    		}
			VersionMessageResponse versMsg = VersionMessageResponse.newBuilder()
				.setHost(InetAddress.getLocalHost().getHostName())
				.setVersion(version)
				.setOriginator(originator)
				.setHostId(selfId.toString())
				.setPort(port).build();
	    	ByteArrayOutputStream baOut = new ByteArrayOutputStream();
	        try {
	        	versMsg.writeTo(baOut);
	        	jmsTemplate.convertAndSend(ADMIN_TOPIC, baOut.toByteArray(), (jmsMsg) -> {
	        		jmsMsg.setBooleanProperty(HEADER_VERSION_MSG_SND, false);
	        		jmsMsg.setBooleanProperty(HEADER_VERSION_MSG_RCV, true);
	        		return jmsMsg;
	        	});
	        } catch (JmsException | IOException ex) {
	        	log.error("sending VersionMessageResponse", ex);
	        }
		}
		if (msg.getBooleanProperty(HEADER_VERSION_MSG_RCV)) {
    		BytesMessage byteMsg = (BytesMessage) msg;
    		byte [] data = new byte[(int)byteMsg.getBodyLength()];
    		byteMsg.readBytes(data);
    		StringBuilder strBuilder = new StringBuilder();
    		VersionMessageResponse versMsg = VersionMessageResponse.parseFrom(data);
    		if (versMsg.getOriginator().equals(selfId.toString())) {
    			strBuilder.append("ServerId: " + versMsg.getHostId() + ", ");
	    		strBuilder.append("Host: " + versMsg.getHost() + ", ");
	    		if (versMsg.getPort() != 0) {
	    			strBuilder.append("Port: " + versMsg.getPort() + ", ");
	    		}
	    		strBuilder.append("Version: " + versMsg.getVersion());
	    		tmpFinalInfo.add(strBuilder.toString());
	    		if (versMsg.getCountBackends() != 0) {
	    			countBackends = versMsg.getCountBackends();
	    			++currentCountBackends;
	    		} else {
	    			++currentCountBrokers;
	    		}
	    		if ((countBackends != null) && (currentCountBackends >= countBackends) && (currentCountBrokers >= countBrokers)) {
	    			finalInfo = tmpFinalInfo;
	    		}
    		}
		}
	}

	@ManagedAttribute
	public String getSelfId() {
		return selfId.toString();
	}
}
