/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.config;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DcsEntity {

	public static final int DCS_PORT_DEFAULT = 61616;
	public static final String SCHEMA_SSL = "ssl";
	public static final String SCHEMA_TCP = "tcp";

	private final String key;
	private final String host;
	private final String interfce;
	private final int jmsPort;
    private final boolean useSSL;

	public DcsEntity(String brokerUrl) {
		String schema;
		String urlStr;

        // detect, if SSL is to be used
		final int idxSSL = brokerUrl.indexOf(SCHEMA_SSL);
		final boolean useSSL = (idxSSL >= 0);

		if (useSSL)  {
		    urlStr = brokerUrl.substring(idxSSL);
		    schema = "https:";
		} else {
            urlStr = brokerUrl;
            schema = "http:";
		}

        // detect full schema
		final int idxSlash = urlStr.indexOf("/");

		if (idxSlash >= 0) {
			urlStr = schema + urlStr.substring(idxSlash);
		} else {
		    urlStr = schema + "//" + urlStr;
		}

		try {
			final URL url = new URL(urlStr);
			final String host = url.getHost();
			final int port = url.getPort();

			this.host = host.equals("localhost") ? "127.0.0.1" : host;
			this.interfce = host.equals("localhost") ? "127.0.0.1" : host;
			this.jmsPort = (useSSL || (port == -1)) ? DCS_PORT_DEFAULT :  port;
            this.useSSL = useSSL;

            if (this.useSSL) {
                this.key = new StringBuilder(256).append(SCHEMA_SSL).append(':').
                    append(this.host).append(':').append(this.jmsPort).toString();
            } else {
                this.key = new StringBuilder(256).append(SCHEMA_TCP).append(':').
                    append(this.host).append(':').append(this.jmsPort).toString();
            }
		} catch (MalformedURLException e) {
			// Exception will never be thrown because the validity of the url will be checked earlier.
			throw new RuntimeException(e);
		}
	}

	@JsonCreator
	public DcsEntity(@JsonProperty String key, @JsonProperty String host, @JsonProperty String interfce, @JsonProperty int jmsPort, @JsonProperty boolean useSSL) {
		this.key = key;
		this.host = host.equals("localhost") ? "127.0.0.1" : host;
		this.interfce = interfce;
		this.jmsPort = jmsPort;
        this.useSSL = useSSL;
	}

    public URI toUri() {
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme(useSSL ? "ssl" : "tcp").host(getInterfce()).port(getJmsPort()).build();
        return uriComponents.toUri();
    }

	public String getKey() {
		return key;
	}

	public String getHost() {
		return host;
	}

	public String getInterfce() {
		return interfce;
	}

	public int getJmsPort() {
		return jmsPort;
	}

	public boolean isUseSSL() {
	    return useSSL;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DcsEntity other = (DcsEntity) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return key;
	}
}
