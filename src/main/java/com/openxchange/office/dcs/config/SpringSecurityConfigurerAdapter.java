/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Configuration class to configure spring security
 * 
 * @author Matthias Wessel
 */
@Configuration
@EnableWebSecurity
public class SpringSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

    private static final String HEALTH_PATH = "/health/**";
    private static final String INFO_PATH = "/info";
    private static final String PROMETHEUS_PATH = "/metrics";

    private static final String[] DEFAULT_PATHS_FOR_NON_AUTH_ACCESS = { HEALTH_PATH, INFO_PATH };
    private static final String[] PATHS_FOR_NON_AUTH_ACCESS_INCL_PROMETHEUS = { HEALTH_PATH, INFO_PATH, PROMETHEUS_PATH };

    @Value("${dcs.secured.prometheus.access:true}")
    private String dcsSecuredPrometheusAccess;

    @Value("${spring.security.user.name}")
    private String userName;

    @Value("${spring.security.user.password}")
    private String userPassword;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        boolean protectedPrometheusEndPoint = Boolean.valueOf(dcsSecuredPrometheusAccess);
        final String[] nonAuthPaths = protectedPrometheusEndPoint ? DEFAULT_PATHS_FOR_NON_AUTH_ACCESS : PATHS_FOR_NON_AUTH_ACCESS_INCL_PROMETHEUS;
        http.authorizeRequests()
            .antMatchers(nonAuthPaths).permitAll()
            .anyRequest().authenticated()
            .and().formLogin()
            .and().httpBasic();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        auth.inMemoryAuthentication().withUser(userName).password(encoder.encode(userPassword)).roles("USER");
    }

}
