/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.jmx;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.NoSuchObjectException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.RMIServerSocketFactory;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

import javax.management.MBeanServer;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openxchange.office.dcs.config.ConfigReader;

public class JmxServer extends Thread {

	private static final Logger log = LoggerFactory.getLogger(JmxServer.class);
	
	private static final String RMI_SERVER_HOST_NAME_PROPERTY = "java.rmi.server.hostname";
	
	private RMIServerSocketFactory serverSocketFactory;
	private Registry rmiRegistry;
	private boolean serverHostNamePropertySet = true;
	private MBeanServer mbeanServer;
	private JMXConnectorServer connector;
	
	@Override
	public void run() {
		if (connector != null) {
			try {
				connector.stop();
			} catch (IOException e) {
				log.warn("Could not stop our Jmx connector server", e);
			} finally {
				connector = null;
			}
		}
		if (rmiRegistry != null) {
			try {
				UnicastRemoteObject.unexportObject(rmiRegistry, true);
			} catch (NoSuchObjectException e) {
				log.warn("Could not unexport our RMI registry", e);
			} finally {
				rmiRegistry = null;
			}
		}
		if (serverHostNamePropertySet) {
			System.clearProperty(RMI_SERVER_HOST_NAME_PROPERTY);
			serverHostNamePropertySet = false;
		}
		
	}
	
	public void start(ConfigReader configReader) {
		try {
			startRmiRegistry(configReader);
			startJmxService(configReader);
			Runtime.getRuntime().addShutdownHook(this);
		} catch (IOException ex) {
			System.err.println("Cannot start JMXServer because: " + ex.getMessage());
			throw new RuntimeException(ex);
		}
	}
	
	private void startRmiRegistry(ConfigReader configReader) throws IOException {		

		InetAddress inetAddress = InetAddress.getByName(configReader.getJmxHost());
		int registryPort = configReader.getJmxPort();
		serverSocketFactory = new LocalSocketFactory(inetAddress);
		/*
		 * We have to do this because JMX tries to connect back the server that we just set and it won't be
		 * able to locate it if we set our own address to anything but the InetAddress.getLocalHost()
		 * address.
		 */
		String rmiServerHostname = configReader.getJmxRmiServerHostName();
		if (StringUtils.isNotBlank(rmiServerHostname)) {
			System.setProperty(RMI_SERVER_HOST_NAME_PROPERTY, rmiServerHostname);
		}
		serverHostNamePropertySet = true;
		rmiRegistry = LocateRegistry.createRegistry(registryPort, null, serverSocketFactory);
	}

	private void startJmxService(ConfigReader configReader) throws IOException {
		JMXServiceURL url = jmxServiceUrlFor(configReader.getJmxHost(), configReader.getJmxServerPort(), configReader.getJmxPort());
		String logJmxUrl = "JMXUrl: " + url;
		System.out.println(logJmxUrl);
		Files.write(Paths.get("jmxurl.log"), logJmxUrl.getBytes());	
		Map<String, Object> envMap = new HashMap<String, Object>();
		if (StringUtils.isNotBlank(configReader.getJmxUser()) && StringUtils.isNotBlank(configReader.getJmxPwd())) {
			JmxUsernamePasswordAuthenticator usernamePasswordAuthenticator = new JmxUsernamePasswordAuthenticator();
			usernamePasswordAuthenticator.addUser(configReader.getJmxUser(), configReader.getJmxPwd());
			envMap.put(JMXConnectorServer.AUTHENTICATOR, usernamePasswordAuthenticator);
		}
		mbeanServer = ManagementFactory.getPlatformMBeanServer();
		connector = JMXConnectorServerFactory.newJMXConnectorServer(url, envMap, mbeanServer);
		connector.start();
	}	
	
    private JMXServiceURL jmxServiceUrlFor(final String hostName, final int jmxServerPort, final int jmxRmiPort) throws IOException {
        String host = hostName;
        if (null == host) {
            host = InetAddress.getLocalHost().getHostName();
        }
        if (jmxServerPort <= 0) {
            return new JMXServiceURL("service:jmx:rmi:///jndi/rmi://" + host + ":" + jmxRmiPort + "/jmxrmi");
        }
        return new JMXServiceURL("service:jmx:rmi://" + host + ":" + jmxServerPort + "/jndi/rmi://" + host + ":" + jmxRmiPort + "/jmxrmi");
    }
	
    
}
