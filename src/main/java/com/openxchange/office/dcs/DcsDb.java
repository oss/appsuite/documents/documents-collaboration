package com.openxchange.office.dcs;

import java.util.stream.Stream;

import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.openxchange.office.dcs.config.SpringSecurityConfigurerAdapter;
import com.openxchange.office.dcs.metrics.ActiveMqMetrics;
import com.openxchange.office.dcs.service.BrokerClusteringService;

import liquibase.integration.spring.SpringLiquibase;

@Configuration
@EnableScheduling
@Import({SpringSecurityConfigurerAdapter.class})
@SpringBootApplication()
@Profile("db")
public class DcsDb {

	/**
	 * Ensure that we set dependsOn dynamically as the annotation 
	 * provided by Spring-Boot is not flexible enough.
	 *
	 * @return configured BeanFactoryPostProcessor instance
	 */
    @Bean
    public static BeanFactoryPostProcessor dependsOnPostProcessor() {
        return bf -> {
            String[] flyway = bf.getBeanNamesForType(SpringLiquibase.class);
            Stream.of(flyway)
                  .map(bf::getBeanDefinition)
                  .forEach(it -> it.setDependsOn("databaseStartupValidator"));

            flyway = bf.getBeanNamesForType(BrokerClusteringService.class);
            Stream.of(flyway)
                  .map(bf::getBeanDefinition)
                  .forEach(it -> it.setDependsOn("liquibase", "brokerRegistrationService"));

            flyway = bf.getBeanNamesForType(ActiveMqMetrics.class);
            Stream.of(flyway)
            .map(bf::getBeanDefinition)
            .forEach(it -> it.setDependsOn("liquibase"));
        };
    }

}
