/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.listener;

import java.lang.management.ManagementFactory;
import java.util.HashSet;
import java.util.Set;
import javax.jms.Message;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import org.apache.activemq.advisory.AdvisorySupport;
import org.apache.activemq.command.ActiveMQMessage;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.DestinationInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

@Component
@ManagedResource
public class AdvisoryMessageListener {

	private static final Logger log = LoggerFactory.getLogger(AdvisoryMessageListener.class);

	public static final String QUEUE_PREFIX = "queue://";

	public static final String ACTIVE_MQ_CONSUMER_QUEUE_PREFIX = "topic://ActiveMQ.Advisory.Consumer.Queue.documents-queue";

	public static final String DOCUMENT_QUEUE_NAME = "documents-queue-";

	private String dcsName;

	private Set<String> createdClientQueueNames = new HashSet<>();

    @Autowired
    public AdvisoryMessageListener(@Value("${dcs.name:dcs_localhost_61616}") String dcsName) {
    	this.dcsName = dcsName;
    }

    @ManagedAttribute
    public Set<String> getCreatedClientQueueNames() {
    	return createdClientQueueNames;
    }

	@JmsListener(containerFactory="advisoryListenerFactory", destination=AdvisorySupport.QUEUE_CONSUMER_ADVISORY_TOPIC_PREFIX + ">")
	public void receiveQueueWithNoConsumerEvent(Message msg) throws Exception {
		if (msg.getIntProperty("consumerCount") == 0) {
			String queueId = msg.getJMSDestination().toString().substring(ACTIVE_MQ_CONSUMER_QUEUE_PREFIX.length() + 1);
			log.info("Queue {} has no consumers! Removing it.", DOCUMENT_QUEUE_NAME + queueId);
			ActiveMQQueue queue = new ActiveMQQueue(DOCUMENT_QUEUE_NAME + queueId);
			if (queue.isQueue()) {
				ObjectName activeMQ = new ObjectName("org.apache.activemq:type=Broker,brokerName=" + dcsName);
				MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
				String operationName = "removeQueue";
				Object [] params = {DOCUMENT_QUEUE_NAME + queueId};
				String [] sig = {"java.lang.String"};
				mBeanServer.invoke(activeMQ, operationName, params, sig);
			} else {
				log.error("Cannot remove queue with id {}", DOCUMENT_QUEUE_NAME + queueId);
			}
		}
	}

	@JmsListener(containerFactory="advisoryListenerFactory", destination=AdvisorySupport.ADVISORY_TOPIC_PREFIX + "Queue")
	public void receiveQueueAdvisoryEvent(Message msg) {
		ActiveMQMessage amqMsg = (ActiveMQMessage) msg;
		DestinationInfo destInfo = (DestinationInfo) amqMsg.getDataStructure();
		String queueName = destInfo.getDestination().getQualifiedName();
		if (queueName.startsWith(QUEUE_PREFIX + DOCUMENT_QUEUE_NAME)) {
			if (createdClientQueueNames.contains(queueName)) {
				createdClientQueueNames.remove(queueName);
			} else {
				createdClientQueueNames.add(queueName);
				log.info("Received new client with id {}. Monitoring queue with name {}.", queueName.substring(DOCUMENT_QUEUE_NAME.length()), queueName.substring(QUEUE_PREFIX.length()));
			}
		}
	}
}
