/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.listener;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.apache.activemq.jms.pool.PooledConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.openxchange.office.dcs.config.DcsEntity;
import com.openxchange.office.dcs.config.FailoverBrokerUrlHelper;
import com.openxchange.office.dcs.config.JmsDestination;
import com.openxchange.office.dcs.service.BrokerClusteringService;

@Component
public class ClusterUpdateListener {

	private static final Logger log = LoggerFactory.getLogger(ClusterUpdateListener.class);
	private static final String ACTIVEMQ_CONNECTION_QUERYPARAM_VERIFYHOSTNAME = "verifyHostName";

	private final PooledConnectionFactory pooledConnFact;

	private final BrokerClusteringService brokerClusteringService;

	private final JmsTemplate jmsTemplate;

	@Value("${dcs.ssl.verifyHostname:false}")
	private Boolean isVerifyHostName;

	@Value("${dcs.usessl:false}")
	private Boolean useSSL;

    @Autowired
    public ClusterUpdateListener(PooledConnectionFactory pooledConnFact, BrokerClusteringService brokerClusteringService, JmsTemplate jmsTemplate) {
    	this.pooledConnFact = pooledConnFact;
    	this.brokerClusteringService = brokerClusteringService;
    	this.jmsTemplate = jmsTemplate;
    }

	@JmsListener(containerFactory="topicListenerFactory", destination=JmsDestination.BROKER_CHANGED_TOPIC_STR)
	public void receiveBrokerChangeEvent(String newBrokerUrl) {
		log.info("Received from BROKER_CHANGED_TOPIC new brokerURL {}", newBrokerUrl);
		if (useSSL) {
			try {
				String adoptedBrokerUrl = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(newBrokerUrl, ACTIVEMQ_CONNECTION_QUERYPARAM_VERIFYHOSTNAME, isVerifyHostName.toString());
				log.info("Adopted broker url {} created from received broker url {}", adoptedBrokerUrl, newBrokerUrl);
				newBrokerUrl = adoptedBrokerUrl;
			} catch (IllegalArgumentException e) {
				log.warn("IllegalArgumentException caught trying to modify received failover brokerUrl " + newBrokerUrl);
			}
		}
		ActiveMQConnectionFactory amqConnFact = (ActiveMQConnectionFactory) pooledConnFact.getConnectionFactory();
		amqConnFact.setBrokerURL(newBrokerUrl);
		brokerClusteringService.lookupForClusterChanged(false, false);
	}

	@JmsListener(containerFactory="topicListenerFactory", destination=JmsDestination.BROKER_STOPPED_TOPIC_STR)
	public void receiveBrokerStoppedEvent(String stoppedBrokerUrl) {
		DcsEntity shutdownDcs = new DcsEntity(stoppedBrokerUrl);
		String dcsUrl = brokerClusteringService.getBrokerUrlUsedByClients(shutdownDcs);
		jmsTemplate.send(JmsDestination.BROKER_CHANGED_TOPIC, s -> {
			ActiveMQTextMessage txtMsg = new ActiveMQTextMessage();
			txtMsg.setText(dcsUrl);
			return txtMsg;
		});		
		String shutdownDcsIdent = shutdownDcs.getHost() + ":" + shutdownDcs.getJmsPort();
		log.info("Received shutdown broker: {}", shutdownDcsIdent);
	}
}
