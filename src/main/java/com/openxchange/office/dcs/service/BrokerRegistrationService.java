/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.service;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;

import com.openxchange.office.dcs.config.DcsEntity;
import com.openxchange.office.dcs.config.DcsRepo;

@Component
@DependsOn("liquibase")
@Profile("db")
public class BrokerRegistrationService {

	private static Logger log = LoggerFactory.getLogger(BrokerRegistrationService.class);

	private final DcsEntity dcsEntity;
	private final DcsRepo dcsRepo;
	private final String advertiseUrl;

	@Autowired
	public BrokerRegistrationService(DcsRepo dcsRepo, @Value("${spring.activemq.broker-url}") String brokerUrl, @Value("${dcs.advertiseURL}") String advertiseUrl) {
		this.dcsRepo = dcsRepo;
		this.dcsEntity = new DcsEntity(brokerUrl);
		this.advertiseUrl = advertiseUrl;
	}

	@PostConstruct
	public void doRegister() {
		DcsEntity tmpEntity = dcsEntity;
		if (StringUtils.isNotBlank(advertiseUrl)) {
			tmpEntity = new DcsEntity(advertiseUrl); 
		}
		try {
			dcsRepo.register(tmpEntity);
			log.info("DCS with key {} was registered in database.", tmpEntity.getKey());
		} catch (DuplicateKeyException ex) {
			log.error("A DCS with key '" + tmpEntity.getKey() + "' has already been registered!");
			System.exit(1);
		}
	}

}
