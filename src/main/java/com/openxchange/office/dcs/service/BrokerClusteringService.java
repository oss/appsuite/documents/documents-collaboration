/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.service;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PreDestroy;
import javax.jms.Message;

import org.apache.activemq.advisory.AdvisorySupport;
import org.apache.activemq.broker.Connection;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.apache.activemq.network.NetworkBridge;
import org.apache.activemq.network.NetworkConnector;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.openxchange.office.dcs.config.DcsAddress;
import com.openxchange.office.dcs.config.DcsEntity;
import com.openxchange.office.dcs.config.DcsRepo;
import com.openxchange.office.dcs.config.JmsDestination;
import com.openxchange.office.dcs.transport.discovery.DatabaseDiscoveryAgent;

@Component
@ManagedResource
public class BrokerClusteringService {

    private static final Logger log = LoggerFactory.getLogger(BrokerClusteringService.class);

    private static final long TIMESTAMP_UPDATE_PERIOD_MILLIS = 60000;

    private static final long TIMESTAMP_MAX_AGE_MILLIS = 600000;

    private final BrokerAccessorService brokerAccessorService;

    private final DcsRepo dcsRepo;

    private final JmsTemplate jmsTemplate;

    private final DcsEntity selfEntity;

	@Value("${dcs.ssl.verifyHostname:false}")
	private Boolean isVerifyHostName;

    @Autowired
    public BrokerClusteringService(BrokerAccessorService brokerAccessorService, DcsRepo dcsRepo,
            JmsTemplate jmsTemplate, @Value("${spring.activemq.broker-url}") String brokerUrl,
            @Value("${dcs.advertiseURL}") String advertiseUrl) {
        this.brokerAccessorService = brokerAccessorService;
        this.dcsRepo = dcsRepo;
        this.jmsTemplate = jmsTemplate;
        if (StringUtils.isNoneBlank(advertiseUrl)) {
            this.selfEntity = new DcsEntity(advertiseUrl);
        } else {
            this.selfEntity = new DcsEntity(brokerUrl);
        }
    }

	@PreDestroy
	public void shutdown() {
		if (selfEntity != null) {
			log.info("Shutdown BrokerClusteringService...");
			dcsRepo.delete(selfEntity.getKey());
		}
	}

	@JmsListener(containerFactory = "advisoryListenerFactory", destination = AdvisorySupport.NETWORK_BRIDGE_TOPIC_PREFIX)
	public void receiveNetworkbridgeAdvisoryEvent(Message msg) {
		log.info("receiveNetworkbridgeAdvisoryEvent {}", msg);
		lookupForClusterChanged(true, true);
	}

	@Scheduled(initialDelay = 3000, fixedDelay = 10000)
	public void lookupForClusterChanged() {
		lookupForClusterChanged(true, false);
	}

    @Scheduled(initialDelay = TIMESTAMP_UPDATE_PERIOD_MILLIS, fixedDelay = TIMESTAMP_UPDATE_PERIOD_MILLIS)
    public void updateAndValidateTimestamps() {
        this.dcsRepo.updateAndValidateTimestamps((null != this.selfEntity) ? this.selfEntity.getKey() : null, TIMESTAMP_MAX_AGE_MILLIS);
    }

    public void lookupForClusterChanged(boolean updateClients, boolean forceUpdateClients) {
		log.debug("Checking for dcs cluster update...");
		Set<DcsAddress> currentDcsList = getNetworkConnectorList();
		Set<DcsAddress> sollDcsList = generateSollDcsList();
		Set<DcsAddress> newDcsEntries = getNewDcsEntries(currentDcsList, sollDcsList);
		Set<DcsAddress> removedDcsEntries = getRemovedDcsEntries(currentDcsList, sollDcsList);
		if (!newDcsEntries.isEmpty() || !removedDcsEntries.isEmpty()) {
			log.info("Current dcs entries: {}, expected dcs entries: {}, new dcs entries: {}, removed dcs entries: {}, ",
					  currentDcsList, sollDcsList, newDcsEntries, removedDcsEntries);
		}
		boolean updated = false;
		for (DcsAddress a : newDcsEntries) {
			DcsEntity dcs = new DcsEntity(a.toUri().toString());
			if (!dcs.equals(selfEntity)) {
				log.info("Process alive for dcs {}", a);
				updated = true;
				getDatabaseDiscoveryAgent().registerService(selfEntity.toUri().toString());
				URI newDcsConnUri = createConnectionURI(dcs);
				getDatabaseDiscoveryAgent().processAlive(dcs.getKey(), newDcsConnUri.toString());
			} else {
				log.info("Dcs {} is equal to self entity {}", a, selfEntity.toUri());
			}
		}
		for (DcsAddress a : removedDcsEntries) {
			DcsEntity dcs = new DcsEntity(a.toUri().toString());
			if (!dcs.equals(selfEntity)) {
				log.info("Process dead for dcs {}", a);
				updated = true;
				getDatabaseDiscoveryAgent().processDead(dcs.toUri().toString());
			}
		}
		if (forceUpdateClients || (updateClients && updated)) {
			updateClients();
		}
	}

	@ManagedAttribute
	public Set<DcsAddress> getNetworkConnectorList() {
		Set<DcsAddress> res = new HashSet<>();
		for (NetworkConnector nc : brokerAccessorService.getBrokerService().getNetworkConnectors()) {
			for (NetworkBridge nb : nc.activeBridges()) {
				String remoteBrokerAddress = nb.getRemoteAddress();
				// DOCS-3630 ignore network bridges with empty remote address
				if (StringUtils.isNotEmpty(remoteBrokerAddress)) {
					try {
						res.add(new DcsAddress(nb));
					} catch (UnknownHostException | URISyntaxException e) {
						log.error(e.getMessage(), e);
					}
				}
			}
		}
		return res;
	}

	private Set<DcsAddress> generateSollDcsList() {
		Set<DcsAddress> sollDcsList = new HashSet<>();
		Collection<DcsEntity> dcsEntities = dcsRepo.getAll();
		dcsEntities.forEach(e -> {
			try {
				sollDcsList.add(new DcsAddress(e));
			} catch (UnknownHostException ex) {
				log.error(ex.getMessage(), ex);
			}
		});

		log.debug("List from db: " + Arrays.toString(sollDcsList.toArray()));
		try {
			sollDcsList.remove(new DcsAddress(selfEntity));
		} catch (UnknownHostException e) {
			log.error(e.getMessage(), e);
		}
		dcsEntities.remove(selfEntity);
		log.debug("List after removal of selfEntity: " + Arrays.toString(sollDcsList.toArray()));
		Set<DcsAddress> res = new HashSet<>();
		for (DcsEntity dcsEntity : dcsEntities) {
			try {
				DcsAddress address = new DcsAddress(dcsEntity);
				res.add(address);
				log.debug("Adding dcs with address {} to res list.", address.getAddress());
			} catch (UnknownHostException e) {
				log.error(e.getMessage(), e);
			}
		}
		log.debug("SollDcsList: {}", Arrays.toString(res.toArray()));
		return res;
	}

	Set<DcsAddress> getNewDcsEntries(Set<DcsAddress> currentDcsList, Set<DcsAddress> sollDcsList) {
		Set<DcsAddress> res = new HashSet<>(sollDcsList);
		res.removeAll(currentDcsList);
		return res;
	}

	Set<DcsAddress> getRemovedDcsEntries(Set<DcsAddress> currentDcsList, Set<DcsAddress> sollDcsList) {
		Set<DcsAddress> res = new HashSet<>(currentDcsList);
		res.removeAll(sollDcsList);
		return res;
	}

	@ManagedOperation
	public void updateClients() {
		final String url = getBrokerUrlUsedByClients();
		jmsTemplate.send(JmsDestination.BROKER_CHANGED_TOPIC, s -> {
			ActiveMQTextMessage txtMsg = new ActiveMQTextMessage();
			txtMsg.setText(url);
			return txtMsg;
		});
	}

	@ManagedAttribute
	public Set<String> getConnectedClients() throws Exception {
		Set<String> res = new HashSet<>();
		for (Connection conn : brokerAccessorService.getBrokerService().getBroker().getClients()) {
			res.add(conn.getConnectionId() + "[" + conn.getRemoteAddress() + "]");
		}
		return res;
	}

	@ManagedAttribute
	public String getBrokerUrlUsedByClients() {
		String res = getNetworkUrl(null);
		res += "?maxReconnectAttempts=3";
		return res;
	}

	public String getBrokerUrlUsedByClients(DcsEntity excludedDcsEntity) {
		String res = getNetworkUrl(excludedDcsEntity);
		res += "?maxReconnectAttempts=3";
		return res;
	}

	public DcsEntity getSelfEntity() {
		return selfEntity;
	}

	private String getNetworkUrl(DcsEntity excludedDcsEntity) {
		final Set<DcsEntity> res = new HashSet<>(dcsRepo.getAll());
		if (!res.isEmpty()) {
			final StringBuilder strBuilder = new StringBuilder("failover");
			strBuilder.append(":(");
			res.forEach(dcs -> {
				if ((excludedDcsEntity == null) || (!dcs.equals(excludedDcsEntity))) {
					strBuilder.append(dcs.isUseSSL() ? "ssl://" : "tcp://");
					strBuilder.append(dcs.getHost());
					strBuilder.append(":");
					strBuilder.append(dcs.getJmsPort());
					strBuilder.append(",");
				}
			});
			strBuilder.deleteCharAt(strBuilder.lastIndexOf(","));
			strBuilder.append(")");
			return strBuilder.toString();
		}
		return null;
	}

	private DatabaseDiscoveryAgent getDatabaseDiscoveryAgent() {
		return DatabaseDiscoveryAgent.getInstance();
	}

	private URI createConnectionURI(DcsEntity newDcsBroker) {
		URI uri = newDcsBroker.toUri();

		String scheme = uri.getScheme();
		if (DcsEntity.SCHEMA_SSL.equalsIgnoreCase(scheme)) {
			String query = (uri.getRawQuery() == null) ? StringUtils.EMPTY : uri.getRawQuery();

			StringBuilder queryPart = new StringBuilder(query);
			queryPart.append(StringUtils.isEmpty(query) ? StringUtils.EMPTY : "&");
			queryPart.append("socket.verifyHostName=");
			queryPart.append(isVerifyHostName);

			UriComponents uriComponents = UriComponentsBuilder.newInstance()
					.scheme(uri.getScheme())
					.host(uri.getHost()).port(uri.getPort())
					.query(queryPart.toString()).build();
			uri = uriComponents.toUri();
		}
		return uri;
	}
}
