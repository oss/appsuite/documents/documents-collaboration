/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.service;

import java.util.Map;
import org.apache.activemq.broker.BrokerRegistry;
import org.apache.activemq.broker.BrokerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import com.openxchange.office.dcs.activemq.BrokerStoppedNotifier;

@Component
public class BrokerAccessorService {

	private final BrokerStoppedNotifier brokerStoppedNotifier;

	private final String brokerName;

	private BrokerService brokerService;

	@Autowired
	public BrokerAccessorService(BrokerStoppedNotifier brokerStoppedNotifier, @Value("${dcs.name:dcs_localhost_61616}") String brokerName) {
		this.brokerStoppedNotifier = brokerStoppedNotifier;
		this.brokerName = brokerName;
	}

	public BrokerService getBrokerService() {
		if (brokerService == null) {
			Map<String, BrokerService> brokers = BrokerRegistry.getInstance().getBrokers();
			for (Map.Entry<String, BrokerService> entry : brokers.entrySet()) {
				if (entry.getKey().equals(brokerName)) {
					brokerService = entry.getValue();
					brokerService.addPreShutdownHook(brokerStoppedNotifier);
					break;
				}
			}
		}
		return brokerService;
	}
}
