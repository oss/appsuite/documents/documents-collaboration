/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.SpringApplication;
import com.openxchange.office.dcs.config.ConfigReader;
import com.openxchange.office.dcs.config.InvalidConfigurationException;
import com.openxchange.office.dcs.config.Profiles;
import com.openxchange.office.dcs.jmx.JmxServer;

public class Dcs {

    public static final String SPRING_CONFIG_LOCATION = "--spring.config.location=";
    public static final String DEFAULT_SPRING_SECURITY_USER = "user";

    public static final class AdvertiseUrlData {
        public String advertiseUrl;
        int port = -1;
        public boolean rewritten;
        public AdvertiseUrlData(String advertiseUrl, int port, boolean rewritten) {
            this.advertiseUrl = advertiseUrl;
            this.port = port;
            this.rewritten = rewritten;
        }
    }

    public static void main(String[] args) throws Exception {
        List<String> arglist = addBasicYamlToArgs(args);
        ConfigReader configReader = new ConfigReader();
        configReader.init(arglist);
        readConfigForActiveMqXml(arglist, configReader);
        args = arglist.toArray(args);
        if (configReader.getJmxEnabled()) {
            JmxServer jmxServer = new JmxServer();
            jmxServer.start(configReader);
        }
        //Files.write(Paths.get("cmdline.log"), Arrays.toString(args).getBytes());
        String springSecurityUserPassword = configReader.getSpringSecurityUserPassword();
        if (StringUtils.isEmpty(springSecurityUserPassword)) {
            arglist.add("--spring.security.user.password=" + UUID.randomUUID().toString());
            args = arglist.toArray(args);
        }
        String springSecurityUserName = configReader.getSpringSecurityUserName();
        if (StringUtils.isEmpty(springSecurityUserName)) {
            arglist.add("--spring.security.user.name=" + DEFAULT_SPRING_SECURITY_USER);
            args = arglist.toArray(args);
        }
        Class<?> springAppClass = null;
        String discovery = configReader.getDcsDiscoveryStrategy();
        switch (discovery) {
        case Profiles.PROFILE_DNS: {
            springAppClass = DcsDns.class;
            break;
        }
        case Profiles.PROFILE_DB: {
            springAppClass = DcsDb.class;
            break;
        }
        default: throw new IllegalArgumentException("Discovery strategy " + discovery + " not supported");
        }

        arglist.add("--spring.profiles.active=" + discovery);
        args = arglist.toArray(args);
        SpringApplication.run(springAppClass, args);
    }

    private static void readConfigForActiveMqXml(List<String> arglist, ConfigReader configReader) throws IOException {
        final StringBuilder argListBuilder = new StringBuilder(256);
        final String host = configReader.getActiveMqHost();
        final String port = configReader.getActiveMqPort();
        final boolean useSSL = configReader.isActiveMqUseSSL();

        // create and add protocol argument
        argListBuilder.append("--spring.activemq.transportConnector.uri.protocol=").append(useSSL ? "ssl" : "nio");
        arglist.add(argListBuilder.toString());
        
        final StringBuilder isAmqSecuredBuilder = new StringBuilder(256);
        isAmqSecuredBuilder.append("--dcs.adminGroup=").append(configReader.getActiveMqUsername().isEmpty() ? "admins" : "secured");
        arglist.add(isAmqSecuredBuilder.toString());

        // set name
        boolean advertiseUrlRewritten = false;
        String advertiseUrl = configReader.getDcsAdvertiseUrl();
        String dcsName = configReader.getDcsHostname(ConfigReader.UNSET);
        if (ConfigReader.UNSET.equals(dcsName)) {
            // fallback for default dcs host which cannot be a valid, unique dcs name
            String activeMqHost = configReader.getActiveMqHost();
            AdvertiseUrlData advertiseUrlData = null;
            if (ConfigReader.DEFAULT_DCS_HOST.equalsIgnoreCase(activeMqHost)) {
                if (StringUtils.isNotEmpty(advertiseUrl)) {
                    advertiseUrlData = rewriteAdvertiseUrlIfNeeded(advertiseUrl, useSSL);
                    activeMqHost = advertiseUrlData.advertiseUrl;
                    advertiseUrl = advertiseUrlData.advertiseUrl;
                    advertiseUrlRewritten = advertiseUrlData.rewritten;
                } else {
                    activeMqHost = advertiseUrl;
                }
            }
            if (StringUtils.isEmpty(activeMqHost)) {
                throw new InvalidConfigurationException("The DCS_ADERTISEURL property must be set in case the DCS_HOST property remains on default. Please check your configuration.");
            }
            boolean portDefinedByAdvertiseUrl = (advertiseUrlData != null) ? (advertiseUrlData.port != -1) : false;
            dcsName = "dcs_" + activeMqHost.replace(".", "_").replace(":",  "_").replace("/", "_") + (portDefinedByAdvertiseUrl ? "" : ("_" + configReader.getActiveMqPort()));
        }
        arglist.add("--dcs.name=" + dcsName);
        if (advertiseUrlRewritten) {
            arglist.add("--dcs.advertiseURL=" + advertiseUrl);
        }

        // create and add URL argument
        argListBuilder.setLength(0);
        argListBuilder.        	
            append("--spring.activemq.broker-url=").
            append("${spring.activemq.transportConnector.uri.protocol:nio}").append("://").
            append(host).append(':').append(port).
            append("?closeAsync=false&dynamicManagement=true");

        if (useSSL) {
            boolean isVerfiyHostname = configReader.isVerifyHostname();
            argListBuilder.
                append("&socket.verifyHostName=").
                append(isVerfiyHostname ? "true" : "false");
            arglist.add("--dcs.ssl.verifyHostname=" + isVerfiyHostname);
        }

        arglist.add(argListBuilder.toString());
    }

    private static List<String> addBasicYamlToArgs(String [] args) {
        List<String> arglist = new ArrayList<>(Arrays.asList(args));
        boolean found = false;
        for (int i=0;i<arglist.size(); ++i) {
            if (arglist.get(i).startsWith(SPRING_CONFIG_LOCATION)) {
                found = true;
                String springConfigLoc = arglist.get(i);
                String springConfigLocWithoutKey = springConfigLoc.substring(SPRING_CONFIG_LOCATION.length());
                String newSpringConfigLoc =  SPRING_CONFIG_LOCATION + "classpath:activemq.basic.yaml," + springConfigLocWithoutKey;
                arglist.remove(i);
                arglist.add(i, newSpringConfigLoc);
                break;
            }
        }
        if (!found) {
            arglist.add(SPRING_CONFIG_LOCATION + "classpath:activemq.basic.yaml");
        }
        return arglist;
    }

    private static AdvertiseUrlData rewriteAdvertiseUrlIfNeeded(String advertiseUrlString, boolean useSSL) throws InvalidConfigurationException {
        try {
            boolean mustBeRewritten = false;
            final URI advertiseUrl = new URI(advertiseUrlString);
            String scheme = advertiseUrl.getScheme();
            if (scheme == null) {
                advertiseUrlString = (useSSL ? "ssl://" : "tcp://") + advertiseUrlString;
                mustBeRewritten = true;
            }
            int port = advertiseUrl.getPort();
            return new AdvertiseUrlData(advertiseUrlString, port, mustBeRewritten);
        } catch (URISyntaxException e) {
            // we assume that we just have an ip address or host name
            try {
                advertiseUrlString = (useSSL ? "ssl://" : "tcp://") + advertiseUrlString;
                URI advertiseUrl = new URI(advertiseUrlString);
                int port = advertiseUrl.getPort();
                return new AdvertiseUrlData(advertiseUrlString, port, true);
            } catch (URISyntaxException ex) {
                throw new InvalidConfigurationException("Cannot create url from advertiseUrl, please check configuration!", ex);
            }
        }
    }

}
