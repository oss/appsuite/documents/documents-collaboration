--liquibase formatted sql
--changeset dcsschema:addPrimKeyToLiquibaseTable
ALTER TABLE `DATABASECHANGELOG`
	ADD COLUMN `DUMMY_ID` INT NOT NULL AUTO_INCREMENT,
	ADD PRIMARY KEY (`DUMMY_ID`);
	
--changeset dcsschema:createDcsTable
CREATE TABLE `DCS` (
	`ID` VARCHAR(190) NOT NULL,
	`Host` VARCHAR(190) NOT NULL,
	`Interface` VARCHAR(190) NOT NULL,
	`JMSPort` INT(11) NOT NULL,
	PRIMARY KEY (`ID`),
	UNIQUE INDEX `ID` (`ID`)
)
COLLATE='utf8mb4_unicode_ci'
ENGINE=InnoDB
;
--rollback drop table DCS;

--changeset dcsschema:addUseSSLColumn
ALTER TABLE `DCS`
    ADD COLUMN (
        `UseSSL` BOOLEAN DEFAULT FALSE NOT NULL
)
;

--changeset dcsschema:addTimestampColumn
ALTER TABLE `DCS`
    ADD COLUMN (
        `TimestampUTC` TIMESTAMP NOT NULL
)
;
