/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.dcs;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.rmi.registry.LocateRegistry;

import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@DirtiesContext
@ActiveProfiles("test")
@SpringBootTest(classes={ActiveMQTestConfig.class})
@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:dcsTest.properties")
public class DcsTest {

	@BeforeClass
    public static void startJmxMBeanServer() throws IOException {
    	LocateRegistry.createRegistry(9001);
    	MBeanServer beanServer = ManagementFactory.getPlatformMBeanServer();
    	String jmxServiceUrlAsString = "service:jmx:rmi:///jndi/rmi://localhost:9001/jmxrmi";
    	System.setProperty("jmx.url", jmxServiceUrlAsString);
    	JMXServiceURL jmxUrl = new JMXServiceURL(jmxServiceUrlAsString);
    	JMXConnectorServerFactory.newJMXConnectorServer(jmxUrl, null, beanServer).start();    	
    }

	@Test
	public void testStartup() throws Exception {
		ObjectName activeMQ = new ObjectName("org.apache.activemq:type=Broker,brokerName=localhost_61616,service=Health");
		String jmxUrlAsStr = System.getProperty("jmx.url");
		JMXServiceURL jmxUrl = new JMXServiceURL(jmxUrlAsStr);
		JMXConnector jmxConn = JMXConnectorFactory.connect(jmxUrl);
		MBeanServerConnection conn = jmxConn.getMBeanServerConnection();
		
 		Object currentStatusObj = conn.getAttribute(activeMQ, "CurrentStatus");
 		Assert.assertEquals("Good", currentStatusObj);
	}
}
