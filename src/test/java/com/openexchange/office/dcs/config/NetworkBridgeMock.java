package com.openexchange.office.dcs.config;

import javax.management.ObjectName;

import org.apache.activemq.network.NetworkBridge;
import org.apache.activemq.network.NetworkBridgeListener;
import org.apache.activemq.network.NetworkBridgeStatistics;

public class NetworkBridgeMock implements NetworkBridge {
	private final String remoteAddress;

	public NetworkBridgeMock(String remoteAddress) {
		this.remoteAddress = remoteAddress;
	}

	@Override
	public void start() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stop() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void serviceRemoteException(Throwable error) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void serviceLocalException(Throwable error) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNetworkBridgeListener(NetworkBridgeListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getRemoteAddress() {
		// TODO Auto-generated method stub
		return remoteAddress;
	}

	@Override
	public String getRemoteBrokerName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getRemoteBrokerId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLocalAddress() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLocalBrokerName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getEnqueueCounter() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long getDequeueCounter() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public NetworkBridgeStatistics getNetworkBridgeStatistics() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setMbeanObjectName(ObjectName objectName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ObjectName getMbeanObjectName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void resetStats() {
		// TODO Auto-generated method stub
		
	}

}
