/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.dcs.config;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;

import org.junit.Test;
import org.apache.activemq.network.NetworkBridge;
import org.junit.Assert;

import com.openxchange.office.dcs.config.DcsAddress;
import com.openxchange.office.dcs.config.DcsEntity;

public class DcsAddressTest {

	@Test
	public void testDcsAddress() {
		// DcsAddress ctor with String, String, int - TESTING ONLY!
		DcsAddress dcsAddrTest = new DcsAddress("tcp", "localhost", 61616);
		URI uriAddrTest = dcsAddrTest.toUri();
		Assert.assertEquals(61616, uriAddrTest.getPort());
		Assert.assertEquals("localhost", uriAddrTest.getHost());
		Assert.assertEquals("tcp", uriAddrTest.getScheme());

		// DcsAddress ctor with DcsEntity
		DcsEntity tmp1 = new DcsEntity(uriAddrTest.toString());
		DcsEntity tmp2 = new DcsEntity("tcp://localhost:61616");
		DcsEntity tmp3 = new DcsEntity("tcp://localhost:61617");
		try {
			DcsAddress dcsAddr1 = new DcsAddress(tmp1);
			Assert.assertEquals(61616, dcsAddr1.getJmsPort());
			Assert.assertEquals("127.0.0.1", dcsAddr1.getAddress());
			Assert.assertEquals("tcp", dcsAddr1.getScheme());
			DcsAddress dcsAddr2 = new DcsAddress(tmp2);
			DcsAddress dcsAddr3 = new DcsAddress(tmp3);

			// Beware: DcsAddress don't take the scheme into account, so
			// the next line are correct and expected
			Assert.assertEquals(dcsAddr1, dcsAddr2);
			Assert.assertEquals(dcsAddr1.hashCode(), dcsAddr2.hashCode());
			Assert.assertEquals(dcsAddr1.toString(), dcsAddr2.toString());
			Assert.assertEquals(dcsAddr1, dcsAddr1);
			Assert.assertNotEquals(dcsAddr1, dcsAddr3);

			DcsAddress nullAddr = new DcsAddress("tcp", null, 9999);
			Assert.assertEquals(nullAddr, nullAddr);
			Assert.assertEquals(nullAddr.hashCode(), nullAddr.hashCode());
			Assert.assertNotEquals(nullAddr, dcsAddr1);
			Assert.assertNotEquals(dcsAddr1, nullAddr);

			DcsEntity tmp4 = new DcsEntity("tcp://127.0.0.2:61616");
			DcsAddress notSameAddr = new DcsAddress(tmp4);
			Assert.assertNotEquals(dcsAddr1, null);
			Assert.assertNotEquals(dcsAddr1, new Object());
			Assert.assertNotEquals(dcsAddr1, notSameAddr);
			Assert.assertNotEquals(dcsAddr1, nullAddr);
		} catch (UnknownHostException e) {
			Assert.fail("localhost/127.0.0.n should never fail to be mapped to a ip address!");
		}

		try {
			DcsEntity tmpDcsEntity = new DcsEntity("tcp://abc.def.int:9999");
			DcsAddress fail = new DcsAddress(tmpDcsEntity);
			Assert.assertNotEquals(null, fail);
			Assert.fail("DcsAddress ctor with tmpDcsEntity should throw exception as host is unknown");
		} catch (UnknownHostException e) {
			// expected exception as we don't know the host and cannot
			// provide an ip address
		}

		// DcsAddress ctor with NetworkBridge
		try {
			NetworkBridge bridge = new NetworkBridgeMock("tcp://127.0.0.1:61616");
			DcsAddress dcsAddrFromBridge = new DcsAddress(bridge);
			Assert.assertEquals(61616, dcsAddrFromBridge.getJmsPort());
			Assert.assertEquals("127.0.0.1", dcsAddrFromBridge.getAddress());
			Assert.assertEquals("tcp", dcsAddrFromBridge.getScheme());
			Assert.assertEquals(bridge.getRemoteAddress(), dcsAddrFromBridge.toUri().toString());
		} catch (URISyntaxException | UnknownHostException e) {
			Assert.fail("Unexpected exception caught!" + e.toString());
		}

		try {
			NetworkBridge bridge = new NetworkBridgeMock("ssl://127.0.0.1:61617");
			DcsAddress dcsAddrFromBridge = new DcsAddress(bridge);
			Assert.assertEquals(61617, dcsAddrFromBridge.getJmsPort());
			Assert.assertEquals("127.0.0.1", dcsAddrFromBridge.getAddress());
			Assert.assertEquals("ssl", dcsAddrFromBridge.getScheme());
			Assert.assertEquals(bridge.getRemoteAddress(), dcsAddrFromBridge.toUri().toString());
		} catch (URISyntaxException | UnknownHostException e) {
			Assert.fail("Unexpected exception caught!" + e.toString());
		}

		try {
			NetworkBridge bridge = new NetworkBridgeMock("nio://127.0.0.1:61616");
			DcsAddress dcsAddrFromBridge = new DcsAddress(bridge);
			Assert.assertEquals(61616, dcsAddrFromBridge.getJmsPort());
			Assert.assertEquals("127.0.0.1", dcsAddrFromBridge.getAddress());
			Assert.assertEquals("nio", dcsAddrFromBridge.getScheme());
			Assert.assertEquals(bridge.getRemoteAddress(), dcsAddrFromBridge.toUri().toString());
		} catch (URISyntaxException | UnknownHostException e) {
			Assert.fail("Unexpected exception caught!" + e.toString());
		}

		try {
			NetworkBridge bridge = new NetworkBridgeMock("nio://abc.def.int:61616");
			DcsAddress dcsAddrFromBridge = new DcsAddress(bridge);
			Assert.assertNotEquals(null, dcsAddrFromBridge);
		} catch (URISyntaxException | UnknownHostException e) {
			// expected exception as we don't know the host and cannot
			// provide an ip address
		}

		// DcsAddress static factory method
        try {
            DcsAddress dcsAddr = DcsAddress.createDcsAddressFromUriString("tcp://127.0.0.1:61616");
            Assert.assertEquals(61616, dcsAddr.getJmsPort());
            Assert.assertEquals("127.0.0.1", dcsAddr.getAddress());
            Assert.assertEquals("tcp", dcsAddr.getScheme());
            Assert.assertEquals("tcp://127.0.0.1:61616", dcsAddr.toUri().toString());
        } catch (URISyntaxException | UnknownHostException e) {
            Assert.fail("Unexpected exception caught!" + e.toString());
        }

        try {
            DcsAddress dcsAddr = DcsAddress.createDcsAddressFromUriString("ssl://127.0.0.1:61617");
            Assert.assertEquals(61617, dcsAddr.getJmsPort());
            Assert.assertEquals("127.0.0.1", dcsAddr.getAddress());
            Assert.assertEquals("ssl", dcsAddr.getScheme());
            Assert.assertEquals("ssl://127.0.0.1:61617", dcsAddr.toUri().toString());
        } catch (URISyntaxException | UnknownHostException e) {
            Assert.fail("Unexpected exception caught!" + e.toString());
        }

        try {
            DcsAddress dcsAddr = DcsAddress.createDcsAddressFromUriString("nio://127.0.0.1:61616");
            Assert.assertEquals(61616, dcsAddr.getJmsPort());
            Assert.assertEquals("127.0.0.1", dcsAddr.getAddress());
            Assert.assertEquals("nio", dcsAddr.getScheme());
            Assert.assertEquals("nio://127.0.0.1:61616", dcsAddr.toUri().toString());
        } catch (URISyntaxException | UnknownHostException e) {
            Assert.fail("Unexpected exception caught!" + e.toString());
        }

        try {
            DcsAddress mustfail = DcsAddress.createDcsAddressFromUriString("nio://abc.def.int:61616");
            Assert.assertNotEquals(null, mustfail);
        } catch (URISyntaxException | UnknownHostException e) {
            // expected exception as we don't know the host and cannot
            // provide an ip address
        }
	}

}
