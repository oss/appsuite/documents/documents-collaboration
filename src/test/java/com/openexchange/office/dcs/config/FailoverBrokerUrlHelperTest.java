/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.dcs.config;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import com.openxchange.office.dcs.config.FailoverBrokerUrlHelper;

public class FailoverBrokerUrlHelperTest {

    private static final String ACTIVEMQ_CLIENT_TRANSPORT_UPDATE_URI_SUPPORTED = "updateURIsSupported";
    private static final String ACTIVEMQ_CLIENT_CONNECTION_VERIFY_HOSTNAME = "verifyHostName";

    //-------------------------------------------------------------------------
    @Test
    public void testEnsureFailoverBrokerUrlContainsQueryParamWithValue() throws Exception {
        final String failOverBrokerUrlTest1 = "failover:(tcp://10.93.1.202:61616,tcp://10.91.1.202:61616,tcp://10.93.1.201:61616,tcp://10.91.1.201:61616)?maxReconnectAttempts=3";
        String result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamWithValue(failOverBrokerUrlTest1, ACTIVEMQ_CLIENT_TRANSPORT_UPDATE_URI_SUPPORTED, "true");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlTest1 + "&" + ACTIVEMQ_CLIENT_TRANSPORT_UPDATE_URI_SUPPORTED + "=" + "true", result);

        final String failOverBrokerUrlTest2 = "failover:(tcp://10.93.1.202:61616,tcp://10.91.1.202:61616,tcp://10.93.1.201:61616,tcp://10.91.1.201:61616)?";
        result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamWithValue(failOverBrokerUrlTest2, ACTIVEMQ_CLIENT_TRANSPORT_UPDATE_URI_SUPPORTED, "true");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlTest2 + ACTIVEMQ_CLIENT_TRANSPORT_UPDATE_URI_SUPPORTED + "=" + "true", result);

        final String failOverBrokerUrlTest3 = "failover:(tcp://10.93.1.202:61616,tcp://10.91.1.202:61616,tcp://10.93.1.201:61616,tcp://10.91.1.201:61616)?maxReconnectAttempts=3&updateURIsSupported=true";
        result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamWithValue(failOverBrokerUrlTest3, ACTIVEMQ_CLIENT_TRANSPORT_UPDATE_URI_SUPPORTED, "true");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlTest3, result);

        final String failOverBrokerUrlTest4 = "failover:(tcp://10.93.1.202:61616,tcp://10.91.1.202:61616,tcp://10.93.1.201:61616,tcp://10.91.1.201:61616)?updateURIsSupported=true";
        result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamWithValue(failOverBrokerUrlTest4, ACTIVEMQ_CLIENT_TRANSPORT_UPDATE_URI_SUPPORTED, "true");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlTest4, result);

        final String failOverBrokerUrlTest5 = "failover:(ssl://10.93.1.202:61616?verifyHostName=false,ssl://10.91.1.202:61616?verifyHostName=false,ssl://10.93.1.201:61616?verifyHostName=false,tcp://10.91.1.201:61616?verifyHostName=false)?maxReconnectAttempts=3";
        result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamWithValue(failOverBrokerUrlTest5, ACTIVEMQ_CLIENT_TRANSPORT_UPDATE_URI_SUPPORTED, "true");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlTest5 + "&" + ACTIVEMQ_CLIENT_TRANSPORT_UPDATE_URI_SUPPORTED + "=" + "true", result);

        final String failOverBrokerUrlTest6 = "failover:(ssl://10.93.1.202:61616?verifyHostName=false,ssl://10.91.1.202:61616?verifyHostName=false,ssl://10.93.1.201:61616?verifyHostName=false,tcp://10.91.1.201:61616?verifyHostName=false)?";
        result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamWithValue(failOverBrokerUrlTest6, ACTIVEMQ_CLIENT_TRANSPORT_UPDATE_URI_SUPPORTED, "true");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlTest6 + ACTIVEMQ_CLIENT_TRANSPORT_UPDATE_URI_SUPPORTED + "=" + "true", result);

        final String failOverBrokerUrlTest7 = "failover:(ssl://10.93.1.202:61616?verifyHostName=false,ssl://10.91.1.202:61616?verifyHostName=false,ssl://10.93.1.201:61616?verifyHostName=false,tcp://10.91.1.201:61616?verifyHostName=false)";
        result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamWithValue(failOverBrokerUrlTest7, ACTIVEMQ_CLIENT_TRANSPORT_UPDATE_URI_SUPPORTED, "true");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlTest7 + "?" + ACTIVEMQ_CLIENT_TRANSPORT_UPDATE_URI_SUPPORTED + "=" + "true", result);

        // check behavior regarding failures
        try {
            result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamWithValue(failOverBrokerUrlTest5, "", "true");
            Assert.fail("Exception expected!");
        } catch (IllegalArgumentException e) {
            // this exception is expected
        }

        try {
            result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamWithValue(failOverBrokerUrlTest5, ACTIVEMQ_CLIENT_TRANSPORT_UPDATE_URI_SUPPORTED, "");
            Assert.fail("Exception expected!");
        } catch (IllegalArgumentException e) {
            // this exception is expected
        }

        // check behavior regarding wrong scheme
        try {
            result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamWithValue("nofailover:(tcp://10.20.30.40:61616)", "abc", "true");
            Assert.fail("Exception expected!");
        } catch (IllegalArgumentException e) {
            // this exception is expected
        }

        // check behavior regarding syntax error
        try {
            result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamWithValue("nofailover:(tcp://10.20.30.40:61616?abc=123", "abc", "true");
            Assert.fail("Exception expected!");
        } catch (IllegalArgumentException e) {
            // this exception is expected
        }

        // check behavior regarding syntax error
        try {
            result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamWithValue("failover:", "abc", "true");
            Assert.fail("Exception expected!");
        } catch (IllegalArgumentException e) {
            // this exception is expected
        }
    }

    //-------------------------------------------------------------------------
    @Test
    public void testEnsureFailoverBrokerUrlContainsQueryParamForEveryConnection() throws Exception {
        // test 1-3 connection urls within the failover url containing no query part
        final String failOverBrokerUrlTest11 = "failover:(tcp://10.93.1.202:61616)";
        final String failOverBrokerUrlExpected11 = "failover:(tcp://10.93.1.202:61616?verifyHostName=false)";
        String result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(failOverBrokerUrlTest11, ACTIVEMQ_CLIENT_CONNECTION_VERIFY_HOSTNAME, "false");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlExpected11, result);

        final String failOverBrokerUrlTest12 = "failover:(tcp://10.93.1.202:61616,tcp://10.91.1.202:61616)";
        final String failOverBrokerUrlExpected12 = "failover:(tcp://10.93.1.202:61616?verifyHostName=false,tcp://10.91.1.202:61616?verifyHostName=false)";
        result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(failOverBrokerUrlTest12, ACTIVEMQ_CLIENT_CONNECTION_VERIFY_HOSTNAME, "false");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlExpected12, result);

        final String failOverBrokerUrlTest13 = "failover:(tcp://10.93.1.202:61616,tcp://10.91.1.202:61616,tcp://10.93.1.201:61616)";
        final String failOverBrokerUrlExpected13 = "failover:(tcp://10.93.1.202:61616?verifyHostName=false,tcp://10.91.1.202:61616?verifyHostName=false,tcp://10.93.1.201:61616?verifyHostName=false)";
        result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(failOverBrokerUrlTest13, ACTIVEMQ_CLIENT_CONNECTION_VERIFY_HOSTNAME, "false");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlExpected13, result);

        // test 1-2 connection urls within the failover url containing with query part
        final String failOverBrokerUrlTest21 = "failover:(tcp://10.93.1.202:61616)?";
        final String failOverBrokerUrlExpected21 = "failover:(tcp://10.93.1.202:61616?verifyHostName=false)?";
        result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(failOverBrokerUrlTest21, ACTIVEMQ_CLIENT_CONNECTION_VERIFY_HOSTNAME, "false");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlExpected21, result);

        final String failOverBrokerUrlTest22 = "failover:(tcp://10.93.1.202:61616,tcp://10.91.1.202:61616)?";
        final String failOverBrokerUrlExpected22 = "failover:(tcp://10.93.1.202:61616?verifyHostName=false,tcp://10.91.1.202:61616?verifyHostName=false)?";
        result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(failOverBrokerUrlTest22, ACTIVEMQ_CLIENT_CONNECTION_VERIFY_HOSTNAME, "false");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlExpected22, result);

        //test 1-3 connection urls within the failover url with one connection url including query param 
        final String failOverBrokerUrlTest31 = "failover:(tcp://10.93.1.202:61616?verifyHostName=false)";
        final String failOverBrokerUrlExpected31 = "failover:(tcp://10.93.1.202:61616?verifyHostName=false)";
        result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(failOverBrokerUrlTest31, ACTIVEMQ_CLIENT_CONNECTION_VERIFY_HOSTNAME, "false");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlExpected31, result);

        final String failOverBrokerUrlTest32 = "failover:(tcp://10.93.1.202:61616,tcp://10.91.1.202:61616?verifyHostName=false)?";
        final String failOverBrokerUrlExpected32 = "failover:(tcp://10.93.1.202:61616?verifyHostName=false,tcp://10.91.1.202:61616?verifyHostName=false)?";
        result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(failOverBrokerUrlTest32, ACTIVEMQ_CLIENT_CONNECTION_VERIFY_HOSTNAME, "false");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlExpected32, result);

        final String failOverBrokerUrlTest33 = "failover:(tcp://10.93.1.202:61616?verifyHostName=false,tcp://10.91.1.202:61616)?";
        final String failOverBrokerUrlExpected33 = "failover:(tcp://10.93.1.202:61616?verifyHostName=false,tcp://10.91.1.202:61616?verifyHostName=false)?";
        result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(failOverBrokerUrlTest33, ACTIVEMQ_CLIENT_CONNECTION_VERIFY_HOSTNAME, "false");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlExpected33, result);

        final String failOverBrokerUrlTest34 = "failover:(tcp://10.93.1.202:61616,tcp://10.91.1.202:61616?verifyHostName=false,tcp://10.93.1.201:61616)";
        final String failOverBrokerUrlExpected34 = "failover:(tcp://10.93.1.202:61616?verifyHostName=false,tcp://10.91.1.202:61616?verifyHostName=false,tcp://10.93.1.201:61616?verifyHostName=false)";
        result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(failOverBrokerUrlTest34, ACTIVEMQ_CLIENT_CONNECTION_VERIFY_HOSTNAME, "false");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlExpected34, result);

        // test 1-3 connection urls within the failover url with one connection urls include query param with wrong value
        final String failOverBrokerUrlTest41 = "failover:(tcp://10.93.1.202:61616?verifyHostName=true)";
        final String failOverBrokerUrlExpected41 = "failover:(tcp://10.93.1.202:61616?verifyHostName=false)";
        result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(failOverBrokerUrlTest41, ACTIVEMQ_CLIENT_CONNECTION_VERIFY_HOSTNAME, "false");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlExpected41, result);

        final String failOverBrokerUrlTest42 = "failover:(tcp://10.93.1.202:61616,tcp://10.91.1.202:61616?verifyHostName=true)?";
        final String failOverBrokerUrlExpected42 = "failover:(tcp://10.93.1.202:61616?verifyHostName=false,tcp://10.91.1.202:61616?verifyHostName=false)?";
        result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(failOverBrokerUrlTest42, ACTIVEMQ_CLIENT_CONNECTION_VERIFY_HOSTNAME, "false");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlExpected42, result);

        // test 1-3 connection urls within the failover url with one connection urls include more query params
        final String failOverBrokerUrlTest51 = "failover:(tcp://10.93.1.202:61616?test=123&verifyHostName=true)";
        final String failOverBrokerUrlExpected51 = "failover:(tcp://10.93.1.202:61616?test=123&verifyHostName=false)";
        result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(failOverBrokerUrlTest51, ACTIVEMQ_CLIENT_CONNECTION_VERIFY_HOSTNAME, "false");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlExpected51, result);

        final String failOverBrokerUrlTest52 = "failover:(tcp://10.93.1.202:61616?test1=123&verifyHostName=true&test2=234)";
        final String failOverBrokerUrlExpected52 = "failover:(tcp://10.93.1.202:61616?test1=123&verifyHostName=false&test2=234)";
        result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(failOverBrokerUrlTest52, ACTIVEMQ_CLIENT_CONNECTION_VERIFY_HOSTNAME, "false");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlExpected52, result);

        final String failOverBrokerUrlTest53 = "failover:(tcp://10.93.1.202:61616?verifyHostName=true&test2=234&test1=123)";
        final String failOverBrokerUrlExpected53 = "failover:(tcp://10.93.1.202:61616?verifyHostName=false&test2=234&test1=123)";
        result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(failOverBrokerUrlTest53, ACTIVEMQ_CLIENT_CONNECTION_VERIFY_HOSTNAME, "false");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlExpected53, result);

        // test failover broker url with root query and connection url both with the query param
        final String failOverBrokerUrlTest61 = "failover:(tcp://10.93.1.202:61616?verifyHostName=true)?verifyHostName=true";
        final String failOverBrokerUrlExpected61 = "failover:(tcp://10.93.1.202:61616?verifyHostName=false)?verifyHostName=true";
        result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(failOverBrokerUrlTest61, ACTIVEMQ_CLIENT_CONNECTION_VERIFY_HOSTNAME, "false");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlExpected61, result);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testEnsureFailoverBrokerUrlContainsQueryParamForEveryConnectionSpecialCases() throws Exception {
        // special case: query parameter is included in longer query parameter
        final String failOverBrokerUrlTest1 = "failover:(tcp://10.93.1.202:61616?verifyHostNameLonger=true)";
        final String failOverBrokerUrlExpected1 = "failover:(tcp://10.93.1.202:61616?verifyHostNameLonger=true&verifyHostName=false)";
        String result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(failOverBrokerUrlTest1, ACTIVEMQ_CLIENT_CONNECTION_VERIFY_HOSTNAME, "false");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlExpected1, result);

        // special case: query parameter is included in longer query parameter
        final String failOverBrokerUrlTest2 = "failover:(tcp://10.93.1.202:61616?NotverifyHostName=true)";
        final String failOverBrokerUrlExpected2 = "failover:(tcp://10.93.1.202:61616?NotverifyHostName=true&verifyHostName=false)";
        result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(failOverBrokerUrlTest2, ACTIVEMQ_CLIENT_CONNECTION_VERIFY_HOSTNAME, "false");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlExpected2, result);

        // special case empty value - front
        final String failOverBrokerUrlTest31 = "failover:(tcp://10.93.1.202:61616?verifyHostName=&abc=123)";
        final String failOverBrokerUrlExpected31 = "failover:(tcp://10.93.1.202:61616?verifyHostName=false&abc=123)";
        result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(failOverBrokerUrlTest31, ACTIVEMQ_CLIENT_CONNECTION_VERIFY_HOSTNAME, "false");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlExpected31, result);

        // special case empty value - end
        final String failOverBrokerUrlTest32 = "failover:(tcp://10.93.1.202:61616?abc=123&verifyHostName=)";
        final String failOverBrokerUrlExpected32 = "failover:(tcp://10.93.1.202:61616?abc=123&verifyHostName=false)";
        result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(failOverBrokerUrlTest32, ACTIVEMQ_CLIENT_CONNECTION_VERIFY_HOSTNAME, "false");
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        Assert.assertEquals(failOverBrokerUrlExpected32, result);

        // special case wrong scheme - code just returns the input url string
        final String failOverBrokerUrlTest4 = "notfailover:(tcp://10.93.1.202:61616)";
        try {
            result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(failOverBrokerUrlTest4, ACTIVEMQ_CLIENT_CONNECTION_VERIFY_HOSTNAME, "false");
            Assert.fail("Exception expected!");
        } catch (IllegalArgumentException e) {
            // expected
        }

        // special case bad syntax for failover url - code just returns the input url string
        final String failOverBrokerUrlTest5 = "failover:(tcp://10.93.1.202:61616?abc=123";
        try {
            result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(failOverBrokerUrlTest5, ACTIVEMQ_CLIENT_CONNECTION_VERIFY_HOSTNAME, "false");
            Assert.fail("Exception expected!");
        } catch (IllegalArgumentException e) {
            // expected
        }

        // special case bad syntax for failover url - code just returns the input url string
        final String failOverBrokerUrlTest6 = "failover://(tcp://10.93.1.202:61616?abc=123";
        try {
            result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(failOverBrokerUrlTest6, ACTIVEMQ_CLIENT_CONNECTION_VERIFY_HOSTNAME, "false");
            Assert.fail("Exception expected!");
        } catch (IllegalArgumentException e) {
            // expected
        }

        // special case bad syntax for failover url - code just returns the input url string
        final String failOverBrokerUrlTest7 = "failover:()";
        try {
            result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(failOverBrokerUrlTest7, ACTIVEMQ_CLIENT_CONNECTION_VERIFY_HOSTNAME, "false");
            Assert.fail("Exception expected!");
        } catch (IllegalArgumentException e) {
            // expected
        }

        // special case query parameter is empty string or null
        final String failOverBrokerUrlTest8 = "failover:(tcp://10.93.1.202:61616)";
        try {
            result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(failOverBrokerUrlTest8, "", "false");
            Assert.fail("Exception expected!");
        } catch (IllegalArgumentException e) {
            // expected
        }
    }
}
