/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.dcs.config;

import org.junit.Test;
import org.junit.Assert;

import com.openxchange.office.dcs.config.DcsEntity;


public class DcsEntityTest {

	@Test
	public void testDcsEntity() {
		// DcsEntity created with correct url
		DcsEntity de61616 = new DcsEntity("tcp://localhost:61616");
		DcsEntity de61616_2 = new DcsEntity("tcp://localhost:61616");
		DcsEntity de61617 = new DcsEntity("tcp://localhost:61617");
		DcsEntity deDefPort = new DcsEntity("tcp://localhost");

		Assert.assertEquals("tcp:127.0.0.1:61616", de61616.toString());
		Assert.assertEquals("tcp:127.0.0.1:61616", de61616.getKey());
		Assert.assertEquals("tcp:127.0.0.1:61616", deDefPort.toString());
		Assert.assertEquals("tcp:127.0.0.1:61616", deDefPort.getKey());
		Assert.assertEquals("tcp:127.0.0.1:61617", de61617.toString());
		Assert.assertEquals("tcp:127.0.0.1:61617", de61617.getKey());
		Assert.assertFalse(de61616.isUseSSL());
		Assert.assertFalse(de61617.isUseSSL());
		Assert.assertEquals(de61616.hashCode(), de61616_2.hashCode());
		Assert.assertNotEquals(de61616.hashCode(), de61617.hashCode());

		Assert.assertEquals("tcp://127.0.0.1:61616", de61616.toUri().toString());
		Assert.assertEquals("tcp://127.0.0.1:61617", de61617.toUri().toString());

		Assert.assertEquals(61616, de61616.getJmsPort());
		Assert.assertEquals("127.0.0.1", de61616.getInterfce());
		Assert.assertEquals("127.0.0.1", de61616.getHost());

		Assert.assertEquals(de61616, de61616);
		Assert.assertEquals(de61616, de61616_2);
		Assert.assertNotEquals(de61616, de61617);
		Assert.assertNotEquals(null, de61616);
		Assert.assertNotEquals(de61616, null);
		Assert.assertNotEquals(de61616, "abc");

		// DcsEntity created with simplified string
		DcsEntity deFromSimpleStr = new DcsEntity("localhost:61616");
		Assert.assertEquals("tcp:127.0.0.1:61616", deFromSimpleStr.toString());
		Assert.assertEquals("tcp://127.0.0.1:61616", deFromSimpleStr.toUri().toString());

		DcsEntity deSSL61616 = new DcsEntity("ssl://localhost:61616");
		Assert.assertEquals("ssl:127.0.0.1:61616", deSSL61616.toString());
		Assert.assertTrue(deSSL61616.isUseSSL());

		// Special behavior of the DcsEntity class to force to use the default
		// port if ssl is used
		DcsEntity deSSL61617 = new DcsEntity("ssl://localhost:61617");
		Assert.assertEquals("ssl:127.0.0.1:61616", deSSL61617.toString());
		Assert.assertEquals("ssl://127.0.0.1:61616", deSSL61617.toUri().toString());
		Assert.assertTrue(deSSL61616.isUseSSL());

		// DcsEntity with an url using a host name instead of an ip address
		DcsEntity deHost = new DcsEntity("tcp://dev.docs.open-xchange.com:61617");
		Assert.assertEquals("tcp:dev.docs.open-xchange.com:61617", deHost.toString());
		Assert.assertEquals("tcp://dev.docs.open-xchange.com:61617", deHost.toUri().toString());
		Assert.assertEquals(61617, deHost.getJmsPort());
		Assert.assertEquals("dev.docs.open-xchange.com", deHost.getInterfce());
		Assert.assertEquals("dev.docs.open-xchange.com", deHost.getHost());
		Assert.assertEquals(deHost.getInterfce(), deHost.getHost());
		Assert.assertEquals("tcp:dev.docs.open-xchange.com:61617", deHost.getKey());

		// DcsEntity ctor with strings
		DcsEntity dcsEntityNonSSL = new DcsEntity("tcp:127.0.0.1:61616", "localhost", "127.0.0.1", 61616, false);
		Assert.assertEquals("tcp:127.0.0.1:61616", dcsEntityNonSSL.toString());
		Assert.assertEquals("tcp://127.0.0.1:61616", dcsEntityNonSSL.toUri().toString());
		Assert.assertEquals("127.0.0.1", dcsEntityNonSSL.getHost());
		Assert.assertEquals("127.0.0.1", dcsEntityNonSSL.getInterfce());
		Assert.assertEquals(61616, dcsEntityNonSSL.getJmsPort());

		DcsEntity dcsEntitySSL = new DcsEntity("ssl:127.0.0.1:61616", "localhost", "127.0.0.1", 61616, true);
		Assert.assertEquals("ssl:127.0.0.1:61616", dcsEntitySSL.toString());
		Assert.assertEquals("ssl://127.0.0.1:61616", dcsEntitySSL.toUri().toString());
		Assert.assertEquals("127.0.0.1", dcsEntitySSL.getHost());
		Assert.assertEquals("127.0.0.1", dcsEntitySSL.getInterfce());
		Assert.assertEquals(61616, dcsEntitySSL.getJmsPort());

		DcsEntity dcsEntityWithHost = new DcsEntity("tcp:127.0.0.1:61616", "127.0.0.1", "127.0.0.1", 61616, false);
		Assert.assertEquals("tcp:127.0.0.1:61616", dcsEntityWithHost.toString());
		Assert.assertEquals("tcp://127.0.0.1:61616", dcsEntityWithHost.toUri().toString());
		Assert.assertEquals("127.0.0.1", dcsEntityWithHost.getHost());
		Assert.assertEquals("127.0.0.1", dcsEntityWithHost.getInterfce());
		Assert.assertEquals(61616, dcsEntityWithHost.getJmsPort());

		DcsEntity dcsNull = new DcsEntity(null, "localhost", "127.0.0.1", 61616, false);
		DcsEntity dcsNull2 = new DcsEntity(null, "localhost", "127.0.0.1", 61616, false);
		Assert.assertNotEquals(dcsNull, dcsEntityNonSSL);
		Assert.assertNotEquals(dcsEntityNonSSL, dcsNull);
		Assert.assertEquals(dcsNull, dcsNull2);
		Assert.assertEquals(dcsNull.hashCode(), dcsNull2.hashCode());

		try {
			DcsEntity dcsBrokenUrl = new DcsEntity("tcp://abcdef.int:-61616");
			Assert.assertNotNull(dcsBrokenUrl);
			Assert.fail("Unexpected that no exception was thrown!");
		} catch (RuntimeException e) {
			// expected exception thrown due to broken url string
		}
	}

}
