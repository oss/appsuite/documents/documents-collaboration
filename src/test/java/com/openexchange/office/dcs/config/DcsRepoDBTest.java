/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.dcs.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import com.openxchange.office.dcs.config.DcsEntity;
import com.openxchange.office.dcs.config.DcsRepo;
import com.openxchange.office.dcs.config.DcsRepoDB;

public class DcsRepoDBTest {
    private static boolean created = false;

    private JdbcTemplate jdbcTemplate;

    @Before
    public void init() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource("jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE;MODE=MYSQL", "sa", "");
        dataSource.setDriverClassName("org.h2.Driver");
        jdbcTemplate = new JdbcTemplate(dataSource);
        if (!created) {
            try {
                jdbcTemplate.execute("CREATE TABLE `DCS` (" +
                    "`ID` VARCHAR(190) NOT NULL," +
                    "`Host` VARCHAR(190) NOT NULL," +
                    "`Interface` VARCHAR(190) NOT NULL," +
                    "`JMSPort` INT(11) NOT NULL," +
                    "`USESSL` INT(5) NOT NULL DEFAULT 0," +
                    "`TimestampUTC` TIMESTAMP NOT NULL," +
                    "PRIMARY KEY (`ID`))");
            } catch (DataAccessException e) {
                jdbcTemplate.execute("delete from DCS");
            }
            created = true;
        }
    }

    @Test
    public void testDcsRepoRegisterAndDelete() {
        DcsRepo dcsRepo = new DcsRepoDB(jdbcTemplate);

        dcsRepo.deleteAll();
        Assert.assertEquals(0, dcsRepo.getAll().size());

        DcsEntity de61616 = new DcsEntity("tcp://localhost:61616");
        DcsEntity de61617 = new DcsEntity("tcp://localhost:61617");
        DcsEntity de61618 = new DcsEntity("tcp://localhost:61618");

        dcsRepo.register(de61616);
        Collection<DcsEntity> dcsEntities = dcsRepo.getAll();
        Assert.assertEquals(1, dcsEntities.size());
        Assert.assertTrue(dcsEntities.contains(de61616));

        dcsRepo.register(de61617);
        dcsEntities = dcsRepo.getAll();
        Assert.assertEquals(2, dcsEntities.size());
        Assert.assertTrue(dcsEntities.contains(de61616));
        Assert.assertTrue(dcsEntities.contains(de61617));

        // we expect no exception on deleting a non-existing key
        dcsRepo.delete(de61618.getKey());
        dcsEntities = dcsRepo.getAll();
        Assert.assertEquals(2, dcsEntities.size());
        Assert.assertTrue(dcsEntities.contains(de61616));
        Assert.assertTrue(dcsEntities.contains(de61617));

        dcsRepo.delete(de61617.getKey());
        dcsEntities = dcsRepo.getAll();
        Assert.assertEquals(1, dcsEntities.size());
        Assert.assertTrue(dcsEntities.contains(de61616));

        dcsRepo.delete(de61616.getKey());
        dcsEntities = dcsRepo.getAll();
        Assert.assertEquals(0, dcsEntities.size());

        // we expect no exception on deleting a non-existing key
        dcsRepo.delete(de61616.getKey());
        dcsEntities = dcsRepo.getAll();
        Assert.assertEquals(0, dcsEntities.size());

        dcsRepo.register(de61616);
        dcsEntities = dcsRepo.getAll();
        Assert.assertEquals(1, dcsEntities.size());

        // check overwriting
        DcsEntity sameKeyDcsEntity = new DcsEntity(de61616.getKey(), "localhost", "localhost", 61616, true);
        dcsRepo.register(sameKeyDcsEntity);
        dcsEntities = dcsRepo.getAll();
        Assert.assertEquals(1, dcsEntities.size());
        List<DcsEntity> dcsEntries = this.convertToList(dcsEntities);
        DcsEntity dbEntry = dcsEntries.get(0);
        Assert.assertEquals(de61616.getKey(), dbEntry.getKey());
        Assert.assertEquals(false, de61616.isUseSSL());
        Assert.assertEquals(true, dbEntry.isUseSSL());

        // check simple get
        DcsEntity gotEntity = dcsRepo.get("localhost", 61616);
        Assert.assertEquals(dbEntry, gotEntity);
        gotEntity = dcsRepo.get("127.0.0.1", 61616);
        Assert.assertEquals(dbEntry, gotEntity);
    }

    @Test
    public void testDcsRepoTimestamps() {
        DcsRepo dcsRepo = new DcsRepoDB(jdbcTemplate);

        dcsRepo.deleteAll();
        Assert.assertEquals(0, dcsRepo.getAll().size());

        DcsEntity de61616 = new DcsEntity("tcp://localhost:61616");
        DcsEntity de61617 = new DcsEntity("tcp://localhost:61617");

        dcsRepo.register(de61616);
        Collection<DcsEntity> dcsEntities = dcsRepo.getAll();
        Assert.assertEquals(1, dcsEntities.size());
        Assert.assertTrue(dcsEntities.contains(de61616));
        Assert.assertEquals(0, dcsRepo.keysMonitored().size());

        dcsRepo.updateAndValidateTimestamps(de61616.getKey(), 1000);
        dcsEntities = dcsRepo.getAll();
        Assert.assertEquals(1, dcsEntities.size());
        Assert.assertTrue(dcsEntities.contains(de61616));
        Assert.assertEquals(1, dcsRepo.keysMonitored().size());

        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        // ensure that time stamp of the entry won't be changed
        // -> entry should be removed from database
        dcsRepo.updateAndValidateTimestamps(null, 1000);
        dcsEntities = dcsRepo.getAll();
        Assert.assertEquals(0, dcsEntities.size());
        Assert.assertEquals(0, dcsRepo.keysMonitored().size());

        dcsRepo.register(de61616);
        dcsEntities = dcsRepo.getAll();
        Assert.assertEquals(1, dcsEntities.size());
        Assert.assertTrue(dcsEntities.contains(de61616));
        Assert.assertEquals(0, dcsRepo.keysMonitored().size());

        dcsRepo.register(de61617);
        dcsEntities = dcsRepo.getAll();
        Assert.assertEquals(2, dcsEntities.size());
        Assert.assertTrue(dcsEntities.contains(de61617));
        Assert.assertEquals(0, dcsRepo.keysMonitored().size());

        // ensure entries are stored internally
        dcsRepo.updateAndValidateTimestamps(null, 10000);
        Assert.assertEquals(2, dcsRepo.keysMonitored().size());

        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        // now de61616 should get a new timestamp, but
        // de61617 should be removed
        dcsRepo.updateAndValidateTimestamps(de61616.getKey(), 1000);
        dcsEntities = dcsRepo.getAll();
        Assert.assertEquals(1, dcsEntities.size());
        Assert.assertTrue(dcsEntities.contains(de61616));
        Assert.assertEquals(1, dcsRepo.keysMonitored().size());

        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        // now de61616 should get a new timestamp and therefore
        // is not removed
        dcsRepo.updateAndValidateTimestamps(de61616.getKey(), 1000);
        dcsEntities = dcsRepo.getAll();
        Assert.assertEquals(1, dcsEntities.size());
        Assert.assertTrue(dcsEntities.contains(de61616));
        Assert.assertEquals(1, dcsRepo.keysMonitored().size());

        // adding same key just touches timestamp
        dcsRepo.deleteAll();
        dcsRepo.register(de61616);
        dcsRepo.updateAndValidateTimestamps(null, 1000);

        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        // register touches timestamp -> no removal of dcs entity
        dcsRepo.register(de61616);
        dcsRepo.updateAndValidateTimestamps(null, 1000);
        dcsEntities = dcsRepo.getAll();
        Assert.assertEquals(1, dcsEntities.size());
    }

    private List<DcsEntity> convertToList(Collection<DcsEntity> collEntries) {
        List<DcsEntity> dcsEntities = new ArrayList<>();
        dcsEntities.addAll(collEntries);
        return dcsEntities;
    }

}
