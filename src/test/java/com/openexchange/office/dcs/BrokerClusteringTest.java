/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.dcs;

import java.lang.management.ManagementFactory;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.advisory.AdvisorySupport;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.apache.activemq.command.ActiveMQTopic;
import org.apache.activemq.network.NetworkConnector;
import org.apache.activemq.xbean.BrokerFactoryBean;
import org.apache.activemq.xbean.XBeanBrokerService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import com.openxchange.office.dcs.activemq.BrokerStoppedNotifier;
import com.openxchange.office.dcs.config.DcsEntity;
import com.openxchange.office.dcs.config.DcsRepo;
import com.openxchange.office.dcs.config.DcsRepoDB;
import com.openxchange.office.dcs.service.BrokerClusteringService;
import com.openxchange.office.dcs.transport.discovery.DatabaseDiscoveryAgent;

public class BrokerClusteringTest {

	private static final Logger log = LoggerFactory.getLogger(BrokerClusteringTest.class);

	public static final int PORT_BROKER1 = 61616;
	public static final int PORT_BROKER2 = 61617;
	public static final int PORT_BROKER3 = 61618;

	private static final MessageFormat jmxQueryMsgFmt = new MessageFormat("org.apache.activemq:brokerName=localhost_{0},connector=networkConnectors,networkConnectorName=NC,networkBridge=tcp_//127.0.0.1_{1},type=Broker")  ;

	private ActiveMQQueue testQueue = new ActiveMQQueue("testQueue");
	private ActiveMQTopic brokerChangeTopic = new ActiveMQTopic("brokerChange-topic");

	private List<BrokerContext> brokerList = new ArrayList<>();

	private Set<DefaultMessageListenerContainer> msgListenerContSet = new HashSet<>();

	private Set<String> receivedMsgs = new HashSet<>();

	private boolean messageReceived = false;

	private DcsRepo dcsRepo;

	private static boolean created = false;

	private JdbcTemplate jdbcTemplate;

	@Before
	public void init() {
		brokerList.clear();
		receivedMsgs.clear();
		messageReceived = false;
		DriverManagerDataSource dataSource = new DriverManagerDataSource("jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE;MODE=MYSQL", "sa", "");
		dataSource.setDriverClassName("org.h2.Driver");
		jdbcTemplate = new JdbcTemplate(dataSource);
        if (!created) {
            try {
                jdbcTemplate.execute("CREATE TABLE `DCS` (" +
                    "`ID` VARCHAR(190) NOT NULL," +
                    "`Host` VARCHAR(190) NOT NULL," +
                    "`Interface` VARCHAR(190) NOT NULL," +
                    "`JMSPort` INT(11) NOT NULL," +
                    "`USESSL` INT(5) NOT NULL DEFAULT 0," +
                    "`TimestampUTC` TIMESTAMP NOT NULL," +
                    "PRIMARY KEY (`ID`))");
            } catch (DataAccessException e) {
                jdbcTemplate.execute("delete from DCS");
            }
            created = true;
        }
		dcsRepo = new DcsRepoDB(jdbcTemplate);
		dcsRepo.deleteAll();
		Assert.assertEquals(0, dcsRepo.getAll().size());
	}

	@After
	public void shutDownActiveMqs() throws Exception {
		msgListenerContSet.forEach(cont -> cont.stop());
		msgListenerContSet.clear();
		for (BrokerContext brokerCtx :  brokerList) {
			if (brokerCtx.isRunning()) {
				for (NetworkConnector nc : brokerCtx.getApplbroker().getBean(BrokerService.class).getNetworkConnectors()) {
					nc.stop();
				}
			}
		}
		for (BrokerContext brokerCtx :  brokerList) {
			AbstractApplicationContext abstrApplCtx = (AbstractApplicationContext) brokerCtx.getApplbroker();
			if (abstrApplCtx != null) {
				abstrApplCtx.close();
			}
		}
		Assert.assertEquals(0, dcsRepo.getAll().size());
		DatabaseDiscoveryAgent.resetDatabaseDiscoveryAgent();
	}

	@Test
	public void testAddBrokerToCluster() throws Exception {
		createBrokerContexts(3);
		startBrokers();
		brokerList.forEach(brokerCtx -> brokerCtx.getApplbroker().getBean(BrokerClusteringService.class).lookupForClusterChanged());
		Thread.sleep(3000);
		Assert.assertEquals(3, dcsRepo.getAll().size());
		Object [][] clusteringInfo = createClusteringInfo("61616", "61617", "61618");
		testJmxNetworkInfo(1, clusteringInfo);
	}

	@Test
	public void testRemoveBrokerFromCluster() throws Exception {
		testAddBrokerToCluster();
		shutdownBroker(2, true);
		Thread.sleep(1000);
		Assert.assertEquals(2, dcsRepo.getAll().size());
		testJmxNetworkInfo(1, createClusteringInfo("61616", "61617"));
	}

	@Test
	public void testRemoveAndAddBrokerToCluster() throws Exception {
		testRemoveBrokerFromCluster();
		Thread.sleep(1000);
		createAdditionalBrokerContext();
		startBroker(2);
		brokerList.forEach(brokerCtx -> brokerCtx.getApplbroker().getBean(BrokerClusteringService.class).lookupForClusterChanged());
		Thread.sleep(3000);
		Assert.assertEquals(3, dcsRepo.getAll().size());
		Object [][] clusteringInfo = createClusteringInfo("61616", "61617", "61618");
		testJmxNetworkInfo(1, clusteringInfo);
	}

	@Test
	public void testNoQueueConsumerLeft() throws Exception {
		createBrokerContexts(1);
		startBrokers();		
		Thread.sleep(1000);
		Assert.assertEquals(1, dcsRepo.getAll().size());
		ActiveMQConnectionFactory activeMQConnectionFactory = createJmsConnectionFactory();
		receiveMessages(activeMQConnectionFactory, new ActiveMQTopic(AdvisorySupport.QUEUE_CONSUMER_ADVISORY_TOPIC_PREFIX + ">"), true, new MessageReceivedListener());
		receiveMessages(activeMQConnectionFactory, new ActiveMQQueue("documents-queue.123"), false, new DummyReceivedListener());
		JmsTemplate jmsTemplate = new JmsTemplate(activeMQConnectionFactory);
		log.info("Sending testMsg1...");
		jmsTemplate.convertAndSend(testQueue, "documents-queue.123");
		Thread.sleep(3000);
		msgListenerContSet.forEach(cont -> cont.stop());
		Thread.sleep(3000);
		Assert.assertTrue(messageReceived);
	}

	@Test
	public void testNewQueueCreated() throws Exception {
		createBrokerContexts(1);
		startBrokers();
		Thread.sleep(1000);
		Assert.assertEquals(1, dcsRepo.getAll().size());
		ActiveMQConnectionFactory activeMQConnectionFactory = createJmsConnectionFactory();
		receiveMessages(activeMQConnectionFactory, new ActiveMQTopic(AdvisorySupport.ADVISORY_TOPIC_PREFIX + "Queue"), true, new MessageReceivedListener());
		JmsTemplate jmsTemplate = new JmsTemplate(activeMQConnectionFactory);
		log.info("Sending testMsg1...");
		jmsTemplate.convertAndSend(testQueue, "testMsg1");
		Thread.sleep(3000);
		Assert.assertTrue(messageReceived);
	}

	@Test
	public void testCleanupOfObsoleteDBEntries() throws Exception {
		final String activeDcsKey = "tcp:127.0.0.1:61616";
		final String obsoleteDcsKey = "tcp:127.0.0.1:61617";

		createBrokerContexts(1);
		startBrokers();

		Thread.sleep(1000);
		Assert.assertEquals(1, dcsRepo.getAll().size());

		DcsEntity obsoleteDcsEntity = new DcsEntity("http://localhost:61617");
		dcsRepo.register(obsoleteDcsEntity);
		Collection<DcsEntity> dcsDbEntries = dcsRepo.getAll();
		Assert.assertTrue(dcsDbEntries != null);
		Assert.assertEquals(1, dcsDbEntries.stream().filter(e -> e.getKey().equalsIgnoreCase(activeDcsKey)).count());
		Assert.assertEquals(1, dcsDbEntries.stream().filter(e -> e.getKey().equalsIgnoreCase(obsoleteDcsKey)).count());

		dcsRepo.updateAndValidateTimestamps(activeDcsKey, 2000);
		dcsDbEntries = dcsRepo.getAll();
		Assert.assertTrue(dcsDbEntries != null);
		Assert.assertEquals(1, dcsDbEntries.stream().filter(e -> e.getKey().equalsIgnoreCase(activeDcsKey)).count());
		Assert.assertEquals(1, dcsDbEntries.stream().filter(e -> e.getKey().equalsIgnoreCase(obsoleteDcsKey)).count());

		Thread.sleep(2100);

		dcsRepo.updateAndValidateTimestamps(activeDcsKey, 2000);
		dcsDbEntries = dcsRepo.getAll();
		Assert.assertEquals(1, dcsDbEntries.stream().filter(e -> e.getKey().equalsIgnoreCase(activeDcsKey)).count());
		Assert.assertEquals(0, dcsDbEntries.stream().filter(e -> e.getKey().equalsIgnoreCase(obsoleteDcsKey)).count());
	}

	private void receiveMessages(ConnectionFactory connFactory, Destination dest, boolean pubSubDomain, MessageListener msgListener) {
		DefaultMessageListenerContainer msgListCont = new DefaultMessageListenerContainer();
		msgListCont.setConnectionFactory(connFactory);
		msgListCont.setConcurrentConsumers(1);
		msgListCont.setDestination(dest);
		msgListCont.setMaxConcurrentConsumers(1);
		msgListCont.setPubSubDomain(pubSubDomain);
		msgListCont.setAutoStartup(true);
		msgListCont.setupMessageListener(msgListener);
		msgListCont.afterPropertiesSet();
		msgListCont.start();
		
	}

    private ActiveMQConnectionFactory createJmsConnectionFactory() {
        final ActiveMQConnectionFactory jmsConnectionFactory = new ActiveMQConnectionFactory();
        jmsConnectionFactory.setBrokerURL("failover:(tcp://localhost:61616)");
        jmsConnectionFactory.setUseAsyncSend(true);
        jmsConnectionFactory.setUseCompression(true);
        return jmsConnectionFactory;
    }

	private Object [][] createClusteringInfo(String port1, String port2, String...ports) {
		List<Object[]> clusteringInfo = new ArrayList<>();
		clusteringInfo.add(Arrays.asList(port1, port2).toArray());
		for (String port : ports) {
			clusteringInfo.add(Arrays.asList(port1, port).toArray());
		}
		Object [][] args = {};
		return clusteringInfo.toArray(args);
	}

	private Object [][] createNegativeClusteringInfo(String negativePort, String...ports) {
		List<Object[]> clusteringInfo = new ArrayList<>();
		for (String port : ports) {
			{
				Object [] args = {negativePort, port};
				clusteringInfo.add(args);
			}
			{
				Object [] args = {port, negativePort};
				clusteringInfo.add(args);
			}
		}		
		Object [][] args = {};
		return clusteringInfo.toArray(args);
	}

	private void testJmxNetworkInfo(int sollSize, Object [][] clusteringInfo) {
		MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
		for (Object [] args : clusteringInfo) {
			String jmxQuery = jmxQueryMsgFmt.format(args);
			try {
				Set<ObjectName> objectInstanceNames = mBeanServer.queryNames(new ObjectName(jmxQuery), null);
				Assert.assertEquals(jmxQuery, sollSize, objectInstanceNames.size());
			} catch (MalformedObjectNameException ex) {
				Assert.fail(ex.getMessage());
			}
		}

	}

	private void startBrokers() throws Exception {
		for (int idx = 0; idx < brokerList.size(); ++idx) {
			startBroker(idx);
		}
	}

	private void stopBrokers() throws Exception {
		for (int idx = 0; idx < brokerList.size(); ++idx) {
			shutdownBroker(idx, true);
		}
	}

	/**
	 * Starts a Broker
	 * @param idx Index of the broker starting with 0
	 * @throws Exception Start of Broker failed
	 */
	private void startBroker(int idx) throws Exception{
		ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("activemqTestNode" + (idx + 1) + ".xml");
		classPathXmlApplicationContext.start();
		XBeanBrokerService xBeanBrokerService = classPathXmlApplicationContext.getBean(XBeanBrokerService.class);
		brokerList.get(idx).setApplbroker(classPathXmlApplicationContext);
	}

	private void shutdownBroker(int brokerIdx, boolean remove) throws Exception {
		BrokerContext brokerContext = brokerList.get(brokerIdx);
		brokerContext.getApplbroker().getBean(BrokerStoppedNotifier.class).run();
		if (brokerContext.getApplbroker() != null) {
			((AbstractApplicationContext) brokerContext.getApplbroker()).close();
		}
		brokerContext.setRunning(false);
		if (remove) {
			brokerList.remove(brokerIdx);
		}
	}

	private void createBrokerContexts(int count) {
		Assert.assertEquals(0, brokerList.size());
		for (int i=0;i<count;++i) {
			brokerList.add(new BrokerContext());
		}
	}

	private void createAdditionalBrokerContext() {
		brokerList.add(new BrokerContext());
	}

	private class BrokerContext {
		private BrokerFactoryBean brokerFactoryBeanNode;
		private ApplicationContext applbroker;
		private boolean running;
		
		public BrokerContext() {
			this.brokerFactoryBeanNode = new BrokerFactoryBean();
			this.applbroker = null;
			this.running = false;
		}		

		public ApplicationContext getApplbroker() {
			return applbroker;
		}
		
		public void setApplbroker(ApplicationContext applbroker) {
			this.applbroker = applbroker;
		}
		
		public boolean isRunning() {
			return running;
		}
		
		public void setRunning(boolean running) {
			this.running = running;
		}
	}
	
	private static class UpdateClusterMessageListener implements MessageListener {

		private final ActiveMQConnectionFactory connFactory;

		public UpdateClusterMessageListener(ActiveMQConnectionFactory connFactory) {
			this.connFactory = connFactory;
		}

		@Override
		public void onMessage(Message message) {
			try {
				ActiveMQTextMessage txtMsg = (ActiveMQTextMessage) message;
				connFactory.setBrokerURL(txtMsg.getText());
			} catch (Exception ex) {
				Assert.fail(ex.getMessage());
			}
		}
	}

	private class MsgReceiver implements MessageListener {

		@Override
		public void onMessage(Message message) {
			try {
				ActiveMQTextMessage txtMsg = (ActiveMQTextMessage) message;
				receivedMsgs.add(txtMsg.getText());
			} catch (Exception ex) {
				Assert.fail(ex.getMessage());
			}
		}
	}
	
	private class MessageReceivedListener implements MessageListener {

		@Override
		public void onMessage(Message msg) {
			messageReceived = true;
		}
	}

	private class DummyReceivedListener implements MessageListener {

		@Override
		public void onMessage(Message msg) {
		}
	}
}
