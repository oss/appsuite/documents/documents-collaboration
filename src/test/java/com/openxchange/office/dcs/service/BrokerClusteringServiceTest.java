/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openxchange.office.dcs.service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import com.openxchange.office.dcs.config.DcsAddress;
import com.openxchange.office.dcs.service.BrokerClusteringService;

public class BrokerClusteringServiceTest {

	private DcsAddress dcsAddress1 = new DcsAddress("nio", "127.0.0.1", 1);
	//private DcsAddress dcsAddress2 = new DcsAddress("nio", "127.0.0.1", 2);
	private DcsAddress dcsAddress3 = new DcsAddress("nio", "127.0.0.1", 3);
	private DcsAddress dcsAddress4 = new DcsAddress("nio", "127.0.0.1", 4);
	private DcsAddress dcsAddress5 = new DcsAddress("nio", "127.0.0.1", 5);
	private DcsAddress dcsAddress6 = new DcsAddress("nio", "127.0.0.1", 6);
	private DcsAddress dcsAddress7 = new DcsAddress("nio", "127.0.0.1", 7);
	private DcsAddress dcsAddress8 = new DcsAddress("nio", "127.0.0.1", 8);
	private DcsAddress dcsAddress9 = new DcsAddress("nio", "127.0.0.1", 9);

	@Test
	public void testGetNewDcsEntries() {
		BrokerClusteringService objectToTest = new BrokerClusteringService(null, null, null, null, "tcp://127.0.0.1:61616");
		Set<DcsAddress> currentDcsList = new HashSet<>(Arrays.asList(dcsAddress1, dcsAddress3, dcsAddress5, dcsAddress7, dcsAddress9));
		Set<DcsAddress> sollDcsList = new HashSet<>(Arrays.asList(dcsAddress1, dcsAddress3, dcsAddress4, dcsAddress6, dcsAddress8, dcsAddress9));
		Set<DcsAddress> sollRes = new HashSet<>(Arrays.asList(dcsAddress4, dcsAddress6, dcsAddress8));
		Set<DcsAddress> res =  objectToTest.getNewDcsEntries(currentDcsList, sollDcsList);
		Assert.assertEquals(sollRes, res);
	}

	@Test
	public void testGetRemovedDcsEntries() {
		BrokerClusteringService objectToTest = new BrokerClusteringService(null, null, null, null, "tcp://127.0.0.1:61616");
		Set<DcsAddress> currentDcsList = new HashSet<>(Arrays.asList(dcsAddress1, dcsAddress3, dcsAddress5, dcsAddress7, dcsAddress9));
		Set<DcsAddress> sollDcsList = new HashSet<>(Arrays.asList(dcsAddress1, dcsAddress3, dcsAddress4, dcsAddress6, dcsAddress8, dcsAddress9));
		Set<DcsAddress> sollRes = new HashSet<>(Arrays.asList(dcsAddress5, dcsAddress7));
		Set<DcsAddress> res =  objectToTest.getRemovedDcsEntries(currentDcsList, sollDcsList);
		Assert.assertEquals(sollRes, res);
	}
}

//Set<String> getNewDcsEntries(Set<String> currentDcsList, Set<String> sollDcsList) {
//Set<String> res = new HashSet<>(sollDcsList);
//sollDcsList.removeAll(currentDcsList);
//return res;
//}

