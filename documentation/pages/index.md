---
title: DocumentsCollaboration server
classes: no-counting
---

# Documents Collaboration Service

The DCS needs to be installed, configured and run. It supports collaboration across Middleware nodes but is also mandatory for small (even single node) environments. The implementation is based on Spring Boot and uses the Java-based messaging server Apache ActiveMQ.

The service can be run as one additional server (JVM) or - for larger deployments - several instances can be clustered. One DCS cluster is always tied to one Hazelcast cluster of Middleware nodes. Following requirements described for node sizing (4GB RAM / 4 CPU core) one should provide 1 DCS node per 10 OX App Suite Middleware nodes.

As soon as more than 1 DCS runs ActiveMQ Failover is used to form a symmetric high availability cluster. The Middleware initially connects to a random DCS. Should a DCS disappear a new connection is established choosing one of the others randomly. 