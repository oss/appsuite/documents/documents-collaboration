###################################################################################################
# documents-collaboration build stage for project and project specific JRE
###################################################################################################
FROM registry-proxy.sre.cloud.oxoe.io/library/eclipse-temurin:21-jdk  as builder-stage

# Copy relevant project sources
COPY build.gradle .
COPY gradlew .
COPY gradle ./gradle
COPY settings.gradle .
COPY src src

# Build project
RUN ./gradlew --parallel --max-workers 4 runtime

###################################################################################################
# documents-collaboration service stage, using JRE and project builds from build stage
###################################################################################################
FROM registry-proxy.sre.cloud.oxoe.io/library/debian:bookworm-slim

ENV PROJECT_HOME /usr/share/open-xchange-documents-collaboration
ENV TINI_VERSION v0.19.0

ENV LANG C.UTF-8
ENV LANGUAGE C.UTF-8
ENV LC_ALL C.UTF-8

RUN         apt-get update && apt-get install -y --no-install-recommends \
                ca-certificates \
                fontconfig \
                locales \
                procps \
                tzdata && \
            echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen && \
            locale-gen en_US.UTF-8 && \
            rm -rf /var/lib/apt/lists/*

ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini

# Copy jlinked  project from builder stage
COPY --from=builder-stage build/image/documents-collaboration/ ${PROJECT_HOME}/

# Copy for remote debug
COPY --from=builder-stage /opt/java/openjdk/lib/libjdwp.so ${PROJECT_HOME}/lib
COPY --from=builder-stage /opt/java/openjdk/lib/libdt_socket.so ${PROJECT_HOME}/lib

COPY conf/dcs.properties /etc/documents-collaboration/
COPY conf/security /etc/documents-collaboration/security/
COPY conf/logback-spring.xml /etc/documents-collaboration/

# Create user/group and set permissions
RUN chmod +x /tini && \
    groupadd --system --gid 1000 open-xchange && \
    useradd --system --no-create-home --gid open-xchange --uid 987 open-xchange && \
    mkdir -p /var/log/open-xchange && \
    chown -R :open-xchange /var/log/open-xchange && \
    chmod -R g+rwx /var/log/open-xchange

WORKDIR /usr/share

USER open-xchange

ENTRYPOINT  [ "/tini", "-g", "--", "/bin/sh", "-c", \
              "JAVA_OPTS=\" \
                -Duser.timezone=UTC \
                -Xms${DCS_JVM_HEAP_SIZE_MB:-512}m \
                -Xmx${DCS_JVM_HEAP_SIZE_MB:-512}m \
                -XshowSettings:vm\" \
              ${PROJECT_HOME}/bin/documents-collaboration \
                --spring.config.location=file:/etc/documents-collaboration/dcs.properties \
                --logging.config=/etc/documents-collaboration/logback-spring.xml" \
            ]

EXPOSE 61616
EXPOSE 8004

