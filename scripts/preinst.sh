#!/bin/sh
# pre - install script

case "$1" in
	1|install)
		# This is an initial install
		groupadd -r open-xchange 2> /dev/null || :
		useradd -r -g open-xchange -r -s /bin/false -c "open-xchange system user" -d /usr/share/open-xchange-documents-collaboration open-xchange 2> /dev/null || :
	;;
	2|upgrade) 
		# This is an upgrade
	;;
	*)
		# called with unkown argument
        	echo "preinst called with unknown argument \`$1'" >&2
		exit 0

esac

exit 0
 
