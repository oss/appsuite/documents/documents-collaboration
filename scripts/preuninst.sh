#!/bin/sh
# preuninstall script

case "$1" in 
	0|remove)
		# uninstall
	systemctl stop open-xchange-documents-collaboration.service
	;;
	1|upgrade)
		# upgrade
	;;
	failed-upgrade)
	;;
	*)
	        echo "preuninst called with unknown argument \`$1'" >&2
		exit 0
esac

exit 0
