#!/bin/sh
# postuninstall

case "$1" in 
	0|remove|purge)
		#uninstall
		[ -f /usr/lib/systemd/system/open-xchange-documents-collaboration.service ] && rm /usr/lib/systemd/system/open-xchange-documents-collaboration.service
		[ -f /etc/systemd/system/multi-user.target.wants/open-xchange-documents-collaboration.service ] && rm /etc/systemd/system/multi-user.target.wants/open-xchange-documents-collaboration.service
	;;
	1|upgrade)
		#upgrade
	;;
	abort-remove|abort-install|abort-upgrade|failed-upgrade)
	;;
	*)
	        echo "postuninst called with unknown argument \`$1'" >&2
		exit 0
esac

exit 0
