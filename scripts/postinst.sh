#!/bin/bash
# post - install script

set -e

. /opt/open-xchange/lib/oxfunctions.sh

postFix() {
	# prevent bash from expanding, see bug 13316
	GLOBIGNORE='*'

	SCR=SCR-778
	if ox_scr_todo ${SCR}
	then
			pfile=/etc/documents-collaboration/dcs.properties
			pkey=dcs.name
			default_val=dcs
			pval=$(ox_read_property ${pkey} ${pfile})
			if [ "${default_val}" = "${pval}" ]
			then
				ox_set_property ${pkey} "" ${pfile}
			fi
			ox_scr_done ${SCR}
	fi
}

case "$1" in
	1|configure)
		# This is a initial install
		install --mode=700 --owner=open-xchange --group=open-xchange --directory  /var/log/open-xchange/documents-collaboration
		install -D -m 644 /usr/share/open-xchange-documents-collaboration/conf/open-xchange-documents-collaboration.service /usr/lib/systemd/system/open-xchange-documents-collaboration.service

		systemctl enable open-xchange-documents-collaboration.service
		systemctl start open-xchange-documents-collaboration.service
		test -n "$2" && {
			# we are in update mode, run postFix to apply fixes
			postFix "$2"
		}
	;;
	2)
		# rpm upgrade
		postFix
	;;
	abort-upgrade|abort-remove|abort-deconfigure)
		# do nothing
	;;
	*)
		echo "postinst called with unknown argument \`$1'" >&2
		exit 1
esac

exit 0


