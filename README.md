# Documents Collaboration Service

The DCS (Documents Collaboration Service) is based on Spring Boot using ActiveMQ as JMS (Java Message Service), Liquibase for database creation & updates and MariaDB java client for database access. There are no requirements regarding a OSGi environment. At least one DCS node is required to use OX Documents with Realtime II, which is the new "real-time" collaboration implementation.

The DCS provides a messaging infrastructure for middleware nodes running OX Appsuite with OX Documents. The DCS implementation uses the ActiveMQ clustering feature <http://activemq.apache.org/clustering.html> to support a dynamic set of DCS nodes. A DCS node at start-up adds itself to the database table DCS which resides on the new database (dcsdb), the node removes itself automatically from the database if the jvm is shutdown. The database table is used detect new DCS nodes, creating a dynamic cluster-setup and for the middleware node to connect to DCS nodes via a JMS connector using the ActiveMQ failover feature to compensate failing DCS nodes.

## Documents Collaboration Service discovery modes

The DCS now supports two distinct modes to find other nodes in its network to create a network of brokers and support fail saftey.

### Database

The DCS controls a database schema normally called dcsdb, which can be configured by environment variables, that stores the data of every DCS node for discovery. All entries in the database will be used to crerate a network of brokers to create a fail-safe cluster. This mode is normally used by classical node setups.

```shell
DCS_DISCOVERSTRATEGY=db
```
### Auto-discovery via DNS

The DCS uses the DNS to find all available nodes using a service name which must be provided via environment variables. Every DCS node scans the availability of the nodes and create connections to newly started and disconnects to not available nodes. It's the duty of the administrator or environment (e.g. Kubernetes) to ensure that all nodes registered with the provided service name. This mode is normally used in a Kubernetes setup and the default.

```shell
DCS_DISCOVERYSTRATEGY=dns
```

## Resource Requests and Limits

The DCS uses the following memory and cpu resources/limits which are sensible minimal values for running DCS instances.

The default resource limits are specified for a DCS which can handle about 300 concurrent OX Documents clients. If more clients have to be managed more replicas can be used or the requested memory and cpu resources can be increased. Please don't lower the memory requests/limits much more as the overhead of system resources lowers the possible Java heap space. A 4 Gi and 4 CPU DCS instance can handle about 2500 concurrent OX Documents clients.

```
  requests:
    cpu: 250m
    memory: 750Mi
  limits:
    cpu: 2000m
    memory: 1000Mi
```

It's possible to adopt the DCS Java heap space with the following DCS specific variable.

```shell
DCS_JVM_HEAP_SIZE_MB=512
```

Please ensure that the value is adopted to the values set in the memory resources/limits, otherwise it's possible that the DCS won't run correctly or does not use the available memory. The default value of 512 (MiB) is correct for the default memory requests/limits defined in the DCS Helm chart.

## Port Map

Port   | Description
-------|------------
 8004  | Jolokia, Prometheus
 9994  | JMX
61616  | JMS
</br>

## Environment Variables

### Kubernetes

The Kubernetes environment variable must be set to enable the service to work correctly in a Kubernetes environment.

Variable | Default | Description
---------|---------|---------------------------------------------------------------
POD_IP   |   -     | Specifies the external IP address of the service. MANDATORY. This is automatically done by the Helm chart which is part of this repository.
</br>

### DCS specific variables

Variable                           | Default   | Description
-----------------------------------|-----------|------------
DCS_JVM_HEAP_SIZE_MB               | 512       | The default Java VM heap size in MiB. This value must be adopted if the memory requests/limits are changed for Docker/Kubernetes.
DCS_DISCOVERYSTRATEGY              | dns       | The discovery stratgey to be used by the DCS instance. See description in Documents Collaboration Service discovery modes.
DCS_NAME                           | -         | The name of the current DCS server. This is normally created automatically by the DCS implementation using the advertise url.
DCS_ADMINGROUP                     | -         | If set to the anonymous group everyone is able to read/write to the queues and/or topics - only used by the unit tests
DCS_HOST                           | 0.0.0.0   | The host address, the current DCS (JMS) server is listening at. By default the DCS uses all available networks to provide its service. If only a specific address should be used it must be set here.
DCS_PORT                           | 61616     | The port, the current DCS (JMS) server is listening on. This DCS server port is stored within the DCS database for each started DCS server. If setting up a cluster of DCS servers, the explicit port for each started DCS server should be set here.
DCS_ADVERTISEURL                   | ${POD_IP} | The DCS itself registers to the DCS database. This property specifies the entry in the database and must provide the external IP address. By default the DCS uses the environment variable POD_IP. You can set the address here with syntax \<address\>:\<port\>.
DCS_SERVICENAME                    | documents-collaboration | The service name registered in the DNS to query for available DCS nodes. This value MUST be set in case the discovery strategy is set to **dns**.
SERVER_PORT                        | 8004      | The port to be used for monitoring purposes via Jolokia bridge or Prometheus.
DCS_USERNAME                       | dcsUser   | The connection to the DCS should be secured by username and password. This property sets the username for this purpose
DCS_PASSWORD                       | secret    | The connection to the DCS should be secured by username and password. This property sets the password for this purpose.
DCS_USESSL                         | false     | If set to true, the current DCS (JMS) server is listening to SSL requests only.
DCS_SSL_VERIFYHOSTNAME             | false     | Determines, if the connection hostname is to be verified in case a secured connection is requested. If customer provides own certificates, this value should be set to true for security reasons.
DCS_SSL_SERVER_KEYSTORE_PATH       | (DCS_USESSL == true) ? /etc/documents-collaboration/security/dcs-broker.ks : "" | The path to the custom server key store to be used for SSL connections. If dcs.usessl is set to true and this entry is commented, a default self signed server key store file will be used.
DCS_SSL_SERVER_KEYSTORE_PASSWORD   | password  | The password for the custom server key store to be used for SSL connections. If dcs.usessl is set to true and this entry is commented, a default password will be used. 
DCS_SSL_SERVER_TRUSTSTORE_PATH     | (DCS_USESELL == true) ? /etc/documents-collaboration/security/dcs-broker.ts : "" | The path to the custom server trust store to be used for SSL connections. If dcs.usessl is set to true and this entry is commented, a default self signed server trust store file will be used.
DCS_SSL_SERVER_TRUSTSTORE_PASSWORD | password  | The password for the custom server trust store to be used for SSL connections. If dcs.usessl is set to true and this entry is commented, a default password will be used. 
</br>

### MySQL database access

These variables specify the MySQL/MariaDB database access. The variables have to be set in case DCS_DISCOVERYSTRATEGY is set to **db**. They will be ignored otherwise.

Variable                                  | Default               | Description
------------------------------------------|-----------------------|------------
DB_HOST                                   | localhost             | The host address of the DCS database server, each started DCS server stores its own connection data at.
DB_PORT                                   | 3306                  | The port of the DCS database server, each started DCS server stores its own connection data at.
DB_SCHEMA                                 | dcsdb                 | The schema of the DCS database server, each started DCS server stores its own connection data within.
DB_USERNAME                               | -                     | The userName to be used when connecting to the DCS database server, each started DCS server stores its own connection data at. MANDATORY.
DB_PASSWORD                               | -                     | The password to be used when connecting to the DCS database server, each started DCS server stores its own connection data at. MANDATORY.
DB_MAXCONNECTIONLIFETIME                  | 600000                | This property controls the maximum lifetime of a database connection in the used pool of the DCS. Provide values in ms - the minimum value cannot be lower than 30000 (30s)
DB_JDBC_USESSL                            | false                 | Specifies if the database connection should be use ssl or not.
DB_JDBC_REQUIRESSL                        | false                 | Require server support of SSL database connection if db.jdbc.useSSL is set to true.
DB_JDBC_VERIFYSERVERCERTIFICATE           | false                 | If db.jdbc.useSSL is set to true, specify if the database driver should verify the server's certificate. When using this feature, the keystore parameters should be specified by the db.jdbc.clientCertificateKeyStore properties, rather than system properties.
DB_JDBC_ENABLEDTLSPROTOCOLS               | TLSv1,TLSv1.1,TLSv1.2 | If db.jdbc.useSSL is set to true, overrides the TLS protocols enabled for use on the underlying database SSL sockets. This may be used to restrict connections to specific TLS versions. 
DB_JDBC_CLIENTCERTIFICATEKEYSTOREURL      | -                     | URL to the database client certificate KeyStore. If not specified, defaults are used.
DB_JDBC_CLIENTCERTIFICATEKEYSTORETYPE     | -                     | KeyStore type for database client certificates. If NULL or empty is set, the default "JKS" is used. Standard keystore types supported by the JVM are "JKS" and "PKCS12", although your environment may have more available depending on what security products are installed and available to the JVM.
DB_JDBC_CLIENTCERTIFICATEKEYSTOREPASSWORD | -                     | Password for the database client certificates KeyStore.

</br>

## JMX specific variables

Variable        | Default | Description
----------------|---------|------------
JMX_USERNAME    | -       | The username to be used for JMX requests.
JMX_PASSWORD    | -       | The password to be used for JMX requests
JMX_PORT        | 9994    | The port, the current DCS server starts the JMX registry on.
JMX_SERVERPORT  | 0       | The port, the current DCS server is listening on for JMX requests.
JMX_HOST        | -       | The host address, the current DCS server is listening on for JMX requests. If not set, this value is determined automatically.
JMX_HOSTNAME    | -       | x
JMX_ENABLED     | false   | Specifies if the JMX service is enabled or not.
</br>

### Examples

Run a DCS instance within docker using the DB mode
```shell
docker run -it -p 61616:61616 -p 8004:8004 -e DB_HOST=<DB-IP> -e DB_USERNAME=<USERNAME> -e DB_PASSWORD=<PASSWORD> -e DCS_ADVERTISEURL=127.0.0.1 -e DCS_DISCOVERYSTRATEGY=db documents-collaboration:latest
```

Run a DCS instance within docker using the DNS mode and memory/cpu limits
```shell
docker run -it -p 61616:61616 -p 8004:8004 -p 9994:9994 --memory="1g" --cpus=2 -e DCS_ADVERTISEURL=192.168.178.106 -e DCS_DISCOVERYSTRATEGY=dns -e POD_IP=192.168.178.106 -e DCS_SERVICENAME=192.168.178.106 -e DCS_SECURED_PROMETHEUS_ACCESS=false documents-collaboration:latest
```

