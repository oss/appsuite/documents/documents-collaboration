#!/bin/bash

DCS_START_SCRIPT=/usr/share/open-xchange-documents-collaboration/bin/com.openexchange.office.dcs 
DCS_CONFIG=/etc/documents-collaboration/dcs.properties 
DCS_LOGBACK=/etc/documents-collaboration/logback-spring.xml
DCS_CONSOLE_LOG=/var/log/open-xchange/dcs-console.log

# help/usage text
usage() {

    echo
    echo "$0 starts the Documents Collaboration Server"
    echo

    printf '%-30s | %s\n' "Parameter" "Default value"
    echo "------------------------------------------------------------"

    echo
    echo 'NOTE: use "-h" to show this help text.'
    echo
    echo
}


exec ${DCS_START_SCRIPT} -Duser.timezone=UTC --spring.config.location=file:${DCS_CONFIG} --logging.config=${DCS_LOGBACK} >> ${DCS_CONSOLE_LOG}

DCS_ERROR=$?

    if [[ ${DCS_ERROR} -ne 0 ]]; then
        exit ${DCS_ERROR}
    fi

exit 0
